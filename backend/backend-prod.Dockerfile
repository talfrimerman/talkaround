FROM openjdk:11

COPY backend/target/*.jar app.jar 

# set the startup command to execute the jar as container, with memory limitations, and connecting to port env port variable 
CMD [ "sh", "-c", "java -Dserver.port=$PORT -Xmx300m -Xss512k -XX:CICompilerCount=2 -Dfile.encoding=UTF-8 -XX:+UseContainerSupport -jar app.jar" ]
