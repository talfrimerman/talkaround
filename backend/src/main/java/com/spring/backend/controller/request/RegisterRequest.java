package com.spring.backend.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.spring.backend.utils.IsoLanguageUtil;

import javax.validation.constraints.NotEmpty;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequest {

        @NotEmpty(message = "{constraints.NotEmpty.message}")
        private String username;

        @NotEmpty(message = "{constraints.NotEmpty.message}")
        private String password;

        @NotEmpty(message = "{constraints.NotEmpty.message}")
        private String language;

        @NotEmpty(message = "{constraints.NotEmpty.message}")
        private String country;

        private String status;

        private List<String> passions;

        public RegisterRequest() {
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public String getLanguage() {
                return language;
        }

        public void setLanguage(String language) {
                this.language = language;
        }

        public String getCountry() {
                return country;
        }

        public void setCountry(String country) {
                this.country = country;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public List<String> getPassions() {
                return passions;
        }

        public void setPassions(List<String> passions) {
                this.passions = passions;
        }

        /**
         * @return true if valid parameters, else false
         */
        public boolean isValid(){
                int MIN_PASSWORD_LENGTH = 8;
                int MAX_PASSWORD_LENGTH = 20;
                int MIN_USERNAME_LENGTH = 3;
                int MAX_USERNAME_LENGTH = 15;
                int MIN_COUNTRY_LENGTH = 3;
                int MAX_COUNTRY_LENGTH = 15;

                if (this.password.length()<MIN_PASSWORD_LENGTH || this.password.length()>MAX_PASSWORD_LENGTH) return false;
                if (this.username.length()<MIN_USERNAME_LENGTH || this.username.length()>MAX_USERNAME_LENGTH) return false;
                if (!IsoLanguageUtil.isValidISOLanguage(language)) return false;
                if (this.country.length()<MIN_COUNTRY_LENGTH || this.country.length()>MAX_COUNTRY_LENGTH) return false;
                return true;
        }
}
