package com.spring.backend.controller.request;

import javax.validation.constraints.NotEmpty;

public class CreateChatRequest {

    @NotEmpty(message = "{constraints.NotEmpty.message}")
    private String initiatorUsername;

    @NotEmpty(message = "{constraints.NotEmpty.message}")
    private String targetUsername;

    @NotEmpty(message = "{constraints.NotEmpty.message}")
    private String chatName;

    public CreateChatRequest() {
    }

    public String getInitiatorUsername() {
        return initiatorUsername;
    }

    public void setInitiatorUsername(String initiatorUsername) {
        this.initiatorUsername = initiatorUsername;
    }

    public String getTargetUsername() {
        return targetUsername;
    }

    public void setTargetUsername(String targetUsername) {
        this.targetUsername = targetUsername;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }
}
