package com.spring.backend.controller.api;

import com.spring.backend.dto.response.Response;
import com.spring.backend.dto.response.admin.base_admin_dashboard.BaseDashboardDto;
import com.spring.backend.dto.response.admin.chat_analytics.ChatAnalyticsDto;
import com.spring.backend.service.admin.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller responsible for admins' requests
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    @GetMapping("/test")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Response alive() {
        Response response = Response.ok();
        response.setPayload("admin");
        return response;
    }

    @GetMapping("/baseDashboard")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Response getBaseDashboard() {
        BaseDashboardDto baseDashboardDto = adminService.getBaseDashboardData();
        Response response = Response.ok();
        response.setPayload(baseDashboardDto);
        return response;
    }

    @GetMapping("/chatAnalytics")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Response getChatAnalytics() {
        ChatAnalyticsDto ChatAnalyticsDto = adminService.getChatAnalyticsData();
        Response response = Response.ok();
        response.setPayload(ChatAnalyticsDto);
        return response;
    }
}
