package com.spring.backend.controller.api;

import com.spring.backend.WebSecurityConfig;
import com.spring.backend.controller.request.LoginRequest;
import com.spring.backend.controller.request.RegisterRequest;
import com.spring.backend.dto.model.AdminDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.dto.response.Response;
import com.spring.backend.dto.response.login.AdminJwtResponsePayload;
import com.spring.backend.dto.response.login.UserJwtResponsePayload;
import com.spring.backend.exception.DuplicateUsernameException;
import com.spring.backend.exception.WrongCredentialsException;
import com.spring.backend.security.JwtHelper;
import com.spring.backend.service.admin.AdminService;
import com.spring.backend.service.user.UserService;
import com.spring.backend.utils.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * A controller responsible for user management related requests
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class UserController {

    private final JwtHelper jwtHelper;
    //private final UserDetailsService userDetailsService;
    //private final PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private AdminService adminService;


    public UserController(JwtHelper jwtHelper) {
        this.jwtHelper = jwtHelper;
        //this.userDetailsService = userDetailsService;
        //this.passwordEncoder = passwordEncoder;
    }

    // register
    @PostMapping("/register")
    public Response register(@RequestBody RegisterRequest registerRequest) {
        // check valid params
        if (!registerRequest.isValid()) {
            Response response = Response.validationException();
            return response;
        }
        // from register request to user dto
        UserDto userDtoInput = toUserDto(registerRequest);

        // activating service layer
        UserDto userDtoOutput = null;
        try {
            userDtoOutput = userService.registerUser(userDtoInput);
        } catch (DuplicateUsernameException e) {
            Response response = Response.duplicateEntity();
            return response;
        }

        // return newly added user
        Response response = Response.ok();
        response.setPayload(userDtoOutput);
        return  response;
    }

    @PostMapping("/addAdmin")
    public Response addAdmin(@RequestBody LoginRequest loginRequest) {
        adminService.addAdmin(loginRequest.getUsername(),loginRequest.getPassword());
        return Response.ok();
    }

    // login
    @PostMapping("/login")
    public Response login(@RequestBody LoginRequest loginRequest) {
        String username = loginRequest.getUsername();
        String password = loginRequest.getPassword();

        // first, check if user is admin
        AdminDto adminDto = adminService.getAdminDtoByUsername(username);
        if (adminDto != null) { // this is an admin
            if (PasswordUtils.verifyUserPassword(password, adminDto.getPassword(), PasswordUtils.getSalt())) {
                String authorities = WebSecurityConfig.ADMIN_ROLE;
                String jwt = buildJwt(username, authorities, adminDto.getId());

                AdminJwtResponsePayload responsePayload = new AdminJwtResponsePayload();
                responsePayload.setJwt(jwt);
                responsePayload.setAdmin(true);
                Response response = Response.ok();
                response.setPayload(responsePayload);
                return response;
            }
        }

        // try to log in as regular user
        UserDto userDtoOutput = null;
        try {
            userDtoOutput = userService.loginRegularUser(username,password);
            String jwt = buildJwt(username, "", userDtoOutput.getId());

            UserJwtResponsePayload responsePayload = new UserJwtResponsePayload();
            responsePayload.setJwt(jwt);
            responsePayload.setUsername(userDtoOutput.getUsername());
            responsePayload.setLanguage(userDtoOutput.getLanguage());
            Response response = Response.ok();
            response.setPayload(responsePayload);
            return response;
        }
        catch (WrongCredentialsException e) { //login failed (as admin and user)
            Response response = Response.wrongCredentials();
            return response;
        }
    }

    private String buildJwt(String username, String rolesStr, Long userId) {
        Map<String, String> claims = new HashMap<>();
        claims.put("username", username);
        claims.put(WebSecurityConfig.AUTHORITIES_CLAIM_NAME, rolesStr);
        claims.put("userId", String.valueOf(userId));
        String jwt = jwtHelper.createJwtForClaims(username, claims);
        return jwt;
    }

    private UserDto toUserDto(RegisterRequest registerRequest) {
        UserDto userDtoInput = new UserDto();
        userDtoInput.setUsername(registerRequest.getUsername());
        userDtoInput.setPassword(registerRequest.getPassword());
        userDtoInput.setLanguage(registerRequest.getLanguage());
        userDtoInput.setCountry(registerRequest.getCountry());
        userDtoInput.setStatus(registerRequest.getStatus());
        userDtoInput.setPassions(registerRequest.getPassions());

        return userDtoInput;
    }

    // alive check
    @GetMapping("/alive")
    public Response alive() {
        Response response = Response.ok();
        response.setPayload("alive");

        return response;
    }

}
