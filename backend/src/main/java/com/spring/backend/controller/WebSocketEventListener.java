package com.spring.backend.controller;

import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.ArrayList;
import java.util.Map;

import static java.lang.String.format;

@Component
public class WebSocketEventListener {

    private static final String SIMP_SESSION_ID_ATTRIBUTE = "simpSessionId";
    private static final String SHALLOW_SUBSCRIBING_FLAG = "shallow";

    @Autowired
    private UserService userService;

    /**
     * listener for new socket connection event
     * @param event
     */
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        System.out.println(("new web socket connection."));
    }

    //TODO are they still subscribed correctly after this handler
    /**
     * listener for subscription events
     */
    @EventListener
    public void subscriptionEventListener(SessionSubscribeEvent event) {
        Message<byte[]> message = event.getMessage();
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
        StompCommand command = accessor.getCommand();
        if (command.equals(StompCommand.SUBSCRIBE)) {
            //makeSubscribeStoreLinks(accessor);
            //System.out.println(accessor.getDestination());
        }
    }


//    /**
//     * handling a subscribe event, by:
//     * (1) add the user as a subscriber to the chat in ChatSubscribersStore
//     * (2) linking user to his session id in ChatSubscribersStore
//     * @param accessor
//     */
//    private void makeSubscribeStoreLinks(StompHeaderAccessor accessor) {
//        String destination = accessor.getDestination();
//        // check if the user subscribed to the chat as a main chat (and not shallow subscription)
//        if (!destination.contains(SHALLOW_SUBSCRIBING_FLAG)) {
//            try {
//                String subscriberUsername = getSubscriberUsername(accessor);
//                Long subscriberId = userService.getIdByUsername(subscriberUsername);
//                Long destinationChatId = extractChatIdFromDestination(destination);
//                // set the chat as user's main chat
//                ChatSubscribersStore.changUserMainSubscription(destinationChatId, subscriberId);
//                // connect session id to username
//                String simpSessionId = (String) accessor.getMessageHeaders().get(SIMP_SESSION_ID_ATTRIBUTE);
//                ChatSubscribersStore.linkUserToSession(subscriberId, simpSessionId);
//            } catch (NoSuchUserException e ) {
//                // don't save to store if no such user
//            }
//        }
//    }

    /**
     * handle web socket disconnect by removing user's subscription to chat from store
     * @param event
     */
    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        System.out.println("User Disconnected");
//        Message<byte[]> message = event.getMessage();
//        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
//        String simpSessionId = (String) accessor.getMessageHeaders().get(SIMP_SESSION_ID_ATTRIBUTE);
//        ChatSubscribersStore.removeUserMainSubscriptionBySessionId(simpSessionId);
    }

//    /**
//     * handling an unsubscribe event, by removing the user from the subscribers set
//     *  of the chat
//     * @param accessor
//     */
//    private void handleUnsubscribeEvent(StompHeaderAccessor accessor) {
//        String destination = accessor.getDestination();
//        // If the user subscribed to the chat as a main chat (and not shallow subscription)
//        if (!destination.contains(SHALLOW_SUBSCRIBING_FLAG)) {
//            String subscriberUsername = getSubscriberUsername(accessor);
//            try {
//                Long subscriberId = userService.getIdByUsername(subscriberUsername);
//                Long destinationChatId = extractChatIdFromDestination(destination);
//                ChatSubscribersStore.removeUserMainSubscription(subscriberId);
//                System.out.println(subscriberUsername + " mainly unsubscribed from " + destinationChatId);
//
//            } catch (NoSuchUserException e ) {
//                // don't save to the manager if no such user
//            }
//        }
//    }

    /**
     * extract the chat id from destination path and convert it to long
     * (e.g: "channel/3" will be parsed to (long) 3)
     * @param destination
     * @return extracted chat id
     */
    private Long extractChatIdFromDestination(String destination) {
        String[] segments = destination.split("/");
        String chatIdString = segments[segments.length-1];
        Long chatId = Long.parseLong(chatIdString);
        return chatId;
    }

    /**
     * helper function to extract the subscriber's username from the header accessor
     */
    private String getSubscriberUsername(StompHeaderAccessor accessor) {
        Map<String,Object> nativeHeaders = (Map<String,Object>) accessor.getMessageHeaders()
                .get("nativeHeaders");
        String subscriberUsername = ((ArrayList<String>) nativeHeaders
                .get("username")).get(0);
        return subscriberUsername;
    }


}
