package com.spring.backend.controller.request;

import javax.validation.constraints.NotEmpty;

public class MarkReadChatRequest {

    private String username;

    private String chatId;

    public MarkReadChatRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
