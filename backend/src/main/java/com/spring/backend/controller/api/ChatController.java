package com.spring.backend.controller.api;

import com.spring.backend.controller.request.CreateChatRequest;
import com.spring.backend.controller.request.LinkUserToChatRequest;
import com.spring.backend.controller.request.MarkReadChatRequest;
import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.dto.response.Response;
import com.spring.backend.exception.DuplicateChatNameException;
import com.spring.backend.exception.NoSuchChatException;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.service.chat.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static java.lang.String.format;

/**
 * a controller responsible for chat related requests (including socket handling)
 */
@Controller
@CrossOrigin
public class ChatController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private ChatService chatService;




    @GetMapping("/api/v1/chat/chatsMap/{username}")
    @ResponseBody
    public Response getChatNameIdMap(@PathVariable String username) {
        try {
            Map<String, ChatDto> chatNameIdMap = chatService.getChatNameToChatMap(username);
            Response response = Response.ok();
            response.setPayload(chatNameIdMap);
            return response;
        } catch (NoSuchUserException e) {
            return Response.wrongCredentials();
        }
    }

    @PostMapping("/api/v1/chat/create")
    @ResponseBody
    public Response createChat(@RequestBody CreateChatRequest createChatRequest) {
        try {
            ChatDto createdChat = chatService.createChat(createChatRequest.getChatName(),
                    createChatRequest.getInitiatorUsername(), createChatRequest.getTargetUsername());
            Response response = Response.ok();
            response.setPayload(createdChat);
            return response;
        } catch (NoSuchUserException e) {
            return Response.badRequest();
        } catch (DuplicateChatNameException e) {
            return Response.duplicateEntity();
        }
    }

    @GetMapping("/api/v1/chat/{chatId}/{username}")
    @ResponseBody
    public Response getChat(@PathVariable String chatId, @PathVariable String username) {
        try {
            ChatDto responseChatDto = chatService.getChat(chatId, username);
            Response response = Response.ok();
            response.setPayload(responseChatDto);
            return response;
        } catch (NoSuchChatException e) {
            Response response = Response.badRequest();
            return response;
        }
    }

    @MessageMapping("/{chatId}/sendMessage")
    public Response sendMessage(@DestinationVariable String chatId, @Payload ChatMessageDto inputChatMessageDto) {
        try {
            // persist message to db
            ChatMessageDto savedMessageDto = chatService.saveMessageToChat(chatId, inputChatMessageDto);
            // send full message to users subscribed to the chat
            messagingTemplate.convertAndSend(format("/channel/%s", chatId), savedMessageDto);
            return Response.ok();
        } catch (NoSuchUserException e) {
            return Response.wrongCredentials();
        } catch (NoSuchChatException e) {
            return Response.badRequest();
        }
    }

    @PostMapping("/api/v1/chat/userRead")
    @ResponseBody
    public Response markChatAsRead(@RequestBody MarkReadChatRequest markReadChatRequest) {
        String chatIdStr = markReadChatRequest.getChatId();
        String username = markReadChatRequest.getUsername();
        try {
            List<ChatMessageDto> newReadMessages = chatService.markUserReadChat(chatIdStr, username);
            // if there are messages who are now read by all, send it via socket
            for (ChatMessageDto readMessage : newReadMessages) {
                messagingTemplate.convertAndSend(format("/channel/%s", chatIdStr), readMessage);
            }
            Response response = Response.ok();
            return response;
        } catch (NoSuchUserException | NoSuchChatException e) {
            return Response.badRequest();
        }
    }

    @PostMapping("/api/v1/chat/linkUserChat")
    @ResponseBody
    public Response linkUserToChat(@RequestBody LinkUserToChatRequest linkUserToChatRequest) {
        String chatIdStr = linkUserToChatRequest.getChatId();
        String username = linkUserToChatRequest.getUsername();
        try {
            ChatDto chatDtoOutput = chatService.linkUserToChat(chatIdStr, username);
            Response response = Response.ok();
            response.setPayload(chatDtoOutput);
            return response;
        } catch (NoSuchChatException e) {
            return Response.badRequest();
        }
    }



}
