package com.spring.backend.controller.request;

public class LinkUserToChatRequest {
    private String username;

    private String chatId;

    public LinkUserToChatRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
