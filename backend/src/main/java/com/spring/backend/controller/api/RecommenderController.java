package com.spring.backend.controller.api;

import com.spring.backend.controller.request.RegisterRequest;
import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.dto.response.Response;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.service.recommenders.RecommenderService;
import com.spring.backend.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * A controller responsible for recommendations for the user
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/recommend")
public class RecommenderController {

    @Autowired
    private RecommenderService recommenderService;


    // recommend users
    @GetMapping("/users/{username}")
    public Response recommendUsers(@PathVariable String username) {
        try {
            List<UserDto> recommendedUsers = recommenderService.recommendUsers(username);
            Response response = Response.ok();
            response.setPayload(recommendedUsers);
            return response;
        } catch (NoSuchUserException e) {
            Response response = Response.badRequest();
            return response;
        }
    }

    // recommend chats
    @GetMapping("/chats/{username}")
    public Response recommendChats(@PathVariable String username) {
        try {
            List<ChatPreviewDto> recommendedUsers = recommenderService.recommendChats(username);
            Response response = Response.ok();
            response.setPayload(recommendedUsers);
            return response;
        } catch (NoSuchUserException e) {
            Response response = Response.badRequest();
            return response;
        }
    }
}
