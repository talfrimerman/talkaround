package com.spring.backend.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * a model representing regular chat message in the system
 */
@Entity
@Table(name = "chat_messages")
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="content")
    private String content;

    @Column(name="formattedDateTime")
    private String formattedDateTime;

    @ManyToOne(optional = false)
    @JoinColumn(name= "sender_id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Chat chat;

    /**
     * For memory saving reasons, we save only ids of users who read the message,
     * as we don't need the entire user object.
     */
    @ElementCollection
    @CollectionTable(name="message_read_by_users", joinColumns=@JoinColumn(name="user_id"))
    @Column(name="message_id")
    private Set<Long> readByIds = new HashSet<Long>();


    public ChatMessage() { }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFormattedDateTime() {
        return formattedDateTime;
    }

    public void setFormattedDateTime(String formattedDateTime) {
        this.formattedDateTime = formattedDateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public Set<Long> getReadByIds() {
        return readByIds;
    }

    public void setReadByIds(Set<Long> readByIds) {
        this.readByIds = readByIds;
    }

    public ChatMessage(long id, String content, String formattedDateTime, User sender, Chat chat) {
        this.id = id;
        this.content = content;
        this.formattedDateTime = formattedDateTime;
        this.sender = sender;
        this.chat = chat;
    }
}
