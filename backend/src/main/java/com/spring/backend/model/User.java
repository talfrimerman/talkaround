package com.spring.backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * a model representing user in the system
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    @Column(name="language")
    private String language;

    @Column(name="country")
    private String country;

    @Column(name="status")
    private String status;

    @ElementCollection
    @CollectionTable(name = "user_passions", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "passion")
    private List<String> passions;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "usersInChat")
    private List<Chat> chats = new ArrayList<>();


    public User() {
    }

    public User(String username, String password, String language, String country, String status, List<String> passions) {
        this.username = username;
        this.password = password;
        this.language = language;
        this.country = country;
        this.status = status;
        this.passions = passions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getPassions() {
        return passions;
    }

    public void setPassions(List<String> passions) {
        this.passions = passions;
    }

    public List<Chat> getChats() {
        return chats;
    }

    public void setChats(List<Chat> chats) {
        this.chats = chats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
