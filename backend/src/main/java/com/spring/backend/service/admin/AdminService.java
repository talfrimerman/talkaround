package com.spring.backend.service.admin;

import com.spring.backend.dto.mapper.AdminMapper;
import com.spring.backend.dto.mapper.ChatMapper;
import com.spring.backend.dto.mapper.UserMapper;
import com.spring.backend.dto.model.AdminDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.dto.response.admin.base_admin_dashboard.BaseDashboardDto;
import com.spring.backend.dto.response.admin.base_admin_dashboard.inner_dtos.PassionCountPair;
import com.spring.backend.dto.response.admin.chat_analytics.ChatAnalyticsDto;
import com.spring.backend.dto.response.admin.chat_analytics.inner_dtos.ChatMetaDataDto;
import com.spring.backend.model.Admin;
import com.spring.backend.model.Chat;
import com.spring.backend.model.User;
import com.spring.backend.repository.AdminRepository;
import com.spring.backend.repository.ChatMessageRepository;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.utils.PassionUtils;
import com.spring.backend.utils.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class AdminService {

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ChatMessageRepository messageRepository;

    @Autowired
    ChatRepository chatRepository;

    public AdminService() {
    }

    public AdminService(AdminRepository adminRepository, UserRepository userRepository, ChatMessageRepository messageRepository, ChatRepository chatRepository) {
        this.adminRepository = adminRepository;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.chatRepository = chatRepository;
    }

    public ChatAnalyticsDto getChatAnalyticsData() {
        ChatAnalyticsDto data = new ChatAnalyticsDto();
        data.setMostMessagesChats(getMostMessagesChats());
        data.setMostUsersChats(getMostUsersChats());
        return data;
    }

    public BaseDashboardDto getBaseDashboardData() {
        BaseDashboardDto data = new BaseDashboardDto();
        data.setUsersCount(getUsersCount());
        data.setMessagesCount(getMessagesCount());
        data.setChatsCount(getChatsCount());
        data.setNewestUsers(getNewestUsersDtos());
        data.setTopPassionsCounts(getTopPassionsCount());
        return data;
    }


    private List<ChatMetaDataDto> getMostMessagesChats() {
        List<Chat> chats = chatRepository.getWithMostMessages(PageRequest.of(0,5));
        List<ChatMetaDataDto> chatsMetaData = chats.stream().map(chat ->
                ChatMapper.toChatMetaData(chat))
                .collect(Collectors.toList());
        return chatsMetaData;
    }

    private List<ChatMetaDataDto> getMostUsersChats() {
        List<Chat> chats = chatRepository.getWithMostUsers(PageRequest.of(0,5));
        List<ChatMetaDataDto> chatsMetaData = chats.stream().map(chat ->
                        ChatMapper.toChatMetaData(chat))
                .collect(Collectors.toList());
        return chatsMetaData;
    }

    private List<PassionCountPair> getTopPassionsCount() {
        // count passions to map
        Map<String, Long> passionsCountsMap = new HashMap<>();
        List<String> passions = PassionUtils.passions;
        for (String p : passions) {
            long currentCount = userRepository.countByPassion(p);
            passionsCountsMap.put(p, currentCount);
        }
        // create list of passion-count pairs
        List<PassionCountPair> passionCountPairs = passionsCountsMap.entrySet()
                .stream().map(entry -> new PassionCountPair(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
        return passionCountPairs;
    }

    private List<UserDto> getNewestUsersDtos() {
        List<User> newestUsers = userRepository.findTop3ByOrderByIdDesc();
        return newestUsers.stream()
                .map(UserMapper::toUserDtoHidePassword)
                .collect(Collectors.toList());
    }

    private long getMessagesCount() {
        return messageRepository.count();
    }

    private long getUsersCount() {
        return userRepository.count();
    }

    private long getChatsCount() {
        return chatRepository.count();
    }

    public AdminDto getAdminDtoByUsername(String username)  {
        Admin admin = adminRepository.findByUsername(username);
        if (admin == null) {
            return null;
        }
        else {
            return AdminMapper.toAdminDto(admin);
        }
    }

    public void addAdmin(String username, String password) {
        Admin admin = new Admin();
        admin.setUsername(username);
        admin.setPassword(PasswordUtils.generateSecurePassword(password,PasswordUtils.getSalt()));
        adminRepository.save(admin);
    }
}
