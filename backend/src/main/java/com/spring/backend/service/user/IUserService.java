package com.spring.backend.service.user;

import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.DuplicateUsernameException;
import com.spring.backend.exception.WrongCredentialsException;

import java.util.List;

/**
 * interface of all user service functions.
 */
public interface IUserService {

    //public List<UserDto> getAllUsers();

    /**
     * @param userDtoInput
     * @return user dto of the newly added user (with empty password field)
     * @throws DuplicateUsernameException
     */
    UserDto registerUser(UserDto userDtoInput) throws DuplicateUsernameException;

    /**
     *
     * @param username
     * @param password
     * @return corresponding user dto (with empty password field)
     * @throws WrongCredentialsException if wrong credantials supplied
     */
    UserDto loginRegularUser(String username, String password) throws WrongCredentialsException;
}
