package com.spring.backend.service.recommenders.chats.strategies;

import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.model.User;

import java.util.List;

public abstract class ChatsRecommendationStrategy {

    /**
     * @param user dto of the user
     * @param count maximum recommendations to return
     * @return list of recommended chats previews by strategy, in the size of maximum count
     */
    public abstract List<ChatPreviewDto> recommendChatsPreviews(UserDto user, int count);


}
