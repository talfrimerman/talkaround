package com.spring.backend.service.recommenders.users.strategies;

import com.spring.backend.dto.mapper.UserMapper;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.model.User;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.utils.PassionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * in this strategy, we recommend users to the target user based on mutual passions,
 * by calculating hamming distance between passions vectors
 * and choosing the nearest users to the target user
 */
@Component
public class PassionsDistanceUsersRecommendationStrategy extends UsersRecommendationStrategy{


    private UserRepository userRepository;

    public PassionsDistanceUsersRecommendationStrategy(UserRepository userRepository) {this.userRepository = userRepository;}

    @Override
    public List<UserDto> recommendUsers(UserDto targetUser, int count) {

        List<String> targetUserPassions = targetUser.getPassions();
        ArrayList<Double> targetUserVector = makePassionsVector(targetUserPassions);

        // creating our reccomended users map
        HashMap<User, Double> usersScoresMap = new HashMap<>();

        // building the map
        for (String p : targetUserPassions) {
            // the candidates will be users who share a passion with the user
            List<User> usersByPassion = userRepository.getAllByPassion(p);

            for (User candidate : usersByPassion) {
                ArrayList<Double> currentCandidateVector = makePassionsVector(candidate.getPassions());
                double currentCandidateDistance = hammingDistance(targetUserVector, currentCandidateVector);
                // if the candidate is different user then the target user, is not yet added and has smaller
                // distance then one of the users in the list (or list is not yet full), we add it
                if (candidate.getUsername() != targetUser.getUsername() &&
                        !usersScoresMap.containsKey(candidate) &&
                        (usersScoresMap.size() < count || currentCandidateDistance < Collections.max(usersScoresMap.values())))
                {
                    usersScoresMap.put(candidate, currentCandidateDistance); // adding user to recommendations
                }
            }
        }

        List<User> recommendedUsers = new ArrayList<>(usersScoresMap.keySet());
        // mapping Users to UserDtos and returning
        List<UserDto> recommendedUsersDtos = new ArrayList<>();
        for (User u : recommendedUsers) {
            recommendedUsersDtos.add(UserMapper.toUserDtoHidePassword(u));
        }
        return recommendedUsersDtos;
    }

    /**
     * calculate hamming distance between two vectors
     * @param targetUserVector
     * @param currentUVector
     * @return hamming distance
     */
    private double hammingDistance(ArrayList<Double> targetUserVector, ArrayList<Double> currentUVector) {
        double distance = 0;
        for (int indexInVector = 0; indexInVector < targetUserVector.size(); indexInVector++) {
            if (targetUserVector.get(indexInVector) != currentUVector.get(indexInVector)) {
                distance++;
            }
        }
        return distance;
    }

    /**
     * create a vector representing user's connection to each passion
     * currently, we simply put 1 if the passion is in user's passions list, else 0
     * this vector is used to calculate similarity between users
     * @param userPassions
     * @return passion vector
     */
    private ArrayList<Double> makePassionsVector(List<String> userPassions) {
        List<String> allPassions = PassionUtils.passions;
        ArrayList<Double> vector = new ArrayList<>();

        for (String passion : allPassions) {
            Double coordinate = userPassions.contains(passion) ? 1.0 : 0;
            vector.add(coordinate);
        }

        return vector;
    }



}
