package com.spring.backend.service.recommenders.chats;

import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.model.User;
import com.spring.backend.service.recommenders.chats.strategies.ChatsRecommendationStrategy;

import java.util.List;

public class ChatsRecommender {

    ChatsRecommendationStrategy chatsRecommendationStrategy;

    public ChatsRecommender(ChatsRecommendationStrategy chatsRecommendationStrategy) {
        this.chatsRecommendationStrategy = chatsRecommendationStrategy;
    }

    public List<ChatPreviewDto> recommendChatsPreviews(UserDto user, int count) {
        return chatsRecommendationStrategy.recommendChatsPreviews(user, count);
    }
}
