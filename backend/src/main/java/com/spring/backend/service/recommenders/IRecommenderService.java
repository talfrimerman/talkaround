package com.spring.backend.service.recommenders;

import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.NoSuchUserException;

import java.util.List;

/**
 * interface for recommender service functions
 */
public interface IRecommenderService {

    /**
     * recommend users for target user
     * @param targetUser
     * @return list at the size of "count" of recommended users
     */
    public List<UserDto> recommendUsers(String username) throws NoSuchUserException;

    /**
     * recommend chats for target user
     * @param username
     * @return list at the size of "count" of recommended chat previews
     */
    public List<ChatPreviewDto> recommendChats(String username) throws NoSuchUserException;
}
