package com.spring.backend.service.recommenders.users.strategies;

import com.spring.backend.dto.model.UserDto;
import com.spring.backend.model.User;

import java.util.List;

public abstract class UsersRecommendationStrategy {

    /**
     * @param user dto of the user
     * @param count maximum recommendations to return
     * @return list of reccommended users by strategy, in the size of up to count
     */
    public abstract List<UserDto> recommendUsers(UserDto user, int count);
}
