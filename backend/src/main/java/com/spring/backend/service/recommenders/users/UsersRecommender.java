package com.spring.backend.service.recommenders.users;

import com.spring.backend.dto.model.UserDto;
import com.spring.backend.service.recommenders.users.strategies.UsersRecommendationStrategy;

import java.util.List;

public class UsersRecommender {

    private UsersRecommendationStrategy usersRecommendationStrategy;

    public UsersRecommender(UsersRecommendationStrategy strategy) {
        this.usersRecommendationStrategy = strategy;
    }

    /**
     * recommend users to user by the recommendation strategy chosen
     * @param userDto
     * @param count
     * @return recommended users dtos list
     */
    public List<UserDto> recommendUsers(UserDto userDto, int count) {
        return this.usersRecommendationStrategy.recommendUsers(userDto, count);
    }
}
