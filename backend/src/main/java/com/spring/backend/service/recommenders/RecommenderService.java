package com.spring.backend.service.recommenders;

import com.spring.backend.dto.mapper.UserMapper;
import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.model.Chat;
import com.spring.backend.model.User;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.service.recommenders.chats.ChatsRecommender;
import com.spring.backend.service.recommenders.chats.strategies.ChatToPassionsScoreChatsRecommendationStrategy;
import com.spring.backend.service.recommenders.users.UsersRecommender;
import com.spring.backend.service.recommenders.users.strategies.PassionsDistanceUsersRecommendationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Component
public class RecommenderService implements IRecommenderService{
    private static int NUMBER_OF_USERS_TO_RECOMMEND = 5;
    private static int NUMBER_OF_CHATS_TO_RECOMMEND = 10;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    public RecommenderService() {}

    public RecommenderService(UserRepository userRepository) {this.userRepository = userRepository;}

    public RecommenderService(UserRepository userRepository, ChatRepository chatRepository) {
        this.userRepository = userRepository;
        this.chatRepository = chatRepository;
    }

    @Override
    public List<UserDto> recommendUsers(String username) throws NoSuchUserException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }
        UserDto userDto = UserMapper.toUserDtoHidePassword(user);
        UsersRecommender usersRecommender = new UsersRecommender(new PassionsDistanceUsersRecommendationStrategy(userRepository));
        List<UserDto> recommendedUsers = usersRecommender.recommendUsers(userDto, NUMBER_OF_USERS_TO_RECOMMEND);
        return recommendedUsers;
    }

    @Override
    public List<ChatPreviewDto> recommendChats(String username) throws NoSuchUserException {
        ChatsRecommender chatsRecommender = new ChatsRecommender(new ChatToPassionsScoreChatsRecommendationStrategy(userRepository, chatRepository));
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }
        UserDto userDto = UserMapper.toUserDtoHidePassword(user);
        List<ChatPreviewDto> recommendedChats = chatsRecommender.recommendChatsPreviews(userDto, NUMBER_OF_CHATS_TO_RECOMMEND);
        // set each chat's top passions
        for (ChatPreviewDto chatPreviewDto : recommendedChats) {
            chatPreviewDto.setTopPassions(findTop3PassionsForChat(chatPreviewDto.getId()));
        }
        return recommendedChats;
    }

    private List<String> findTop3PassionsForChat(Long chatId) {
        HashMap<String, Integer> passionsCountMap = new HashMap<>();
        HashMap<String, Integer> top3Passions = new HashMap<>();
        Chat chat = chatRepository.findById(chatId).orElse(null);
        for (User userInChat : chat.getUsersInChat()) {
            for (String passion : userInChat.getPassions()) {
                if (!passionsCountMap.containsKey(passion)) {
                    passionsCountMap.put(passion, 0);
                }
                passionsCountMap.put(passion, passionsCountMap.get(passion) + 1);
                if (top3Passions.size() < 3) {
                    // put new passion as one of the top 3 and remove previous one
                    top3Passions.put(passion, passionsCountMap.get(passion) + 1);
                }
                else if (passionsCountMap.get(passion) > Collections.min(top3Passions.values())) {
                    top3Passions.values().remove(Collections.min(top3Passions.values()));
                    top3Passions.put(passion, passionsCountMap.get(passion) + 1);
                }
            }
        }
        return new ArrayList<>(top3Passions.keySet());
    }
}
