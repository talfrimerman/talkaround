package com.spring.backend.service.chat;

import com.spring.backend.dto.mapper.ChatMapper;
import com.spring.backend.dto.mapper.ChatMessageMapper;
import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.exception.DuplicateChatNameException;
import com.spring.backend.exception.NoSuchChatException;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.model.Chat;
import com.spring.backend.model.ChatMessage;
import com.spring.backend.model.User;
import com.spring.backend.repository.ChatMessageRepository;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ChatService implements IChatService {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Map<String, ChatDto> getChatNameToChatMap(String username) throws NoSuchUserException {
        // getting relevant user
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }

        Map<String, ChatDto> chatNameIdMap = new HashMap<String, ChatDto>();

        List<Chat> chats = user.getChats();
        for (Chat chat : chats) {
            ChatDto currentChatDto = ChatMapper.toChatDtoShallow(chat);
            currentChatDto.setNewMessagesForUser(checkHasUnreadMessages(chat,user));
            chatNameIdMap.put(chat.getChatName(), currentChatDto);
        }

        return chatNameIdMap;
    }

    @Override
    public ChatDto getChat(String id, String username) throws NoSuchChatException {
        try {
            Long longChatId = Long.parseLong(id);
            Chat chat = chatRepository.findById(longChatId).orElse(null);
            User requestingUser = userRepository.findByUsername(username);
            if (chat == null || requestingUser == null) {
                throw new NoSuchChatException();
            }
            ChatDto chatDtoOutput = ChatMapper.toChatDto(chat);
            chatDtoOutput.setNewMessagesForUser(checkHasUnreadMessages(chat, requestingUser));
            return chatDtoOutput;
        } catch (Exception e) {
            throw new NoSuchChatException();
        }
    }

    @Override
    public ChatDto linkUserToChat(String id, String username) throws NoSuchChatException {
        try {
            Long longChatId = Long.parseLong(id);
            Chat chat = chatRepository.findById(longChatId).orElse(null);
            User requestingUser = userRepository.findByUsername(username);
            if (chat == null || requestingUser == null) {
                throw new NoSuchChatException();
            }
            // link user to chat and save to db
            if (!chat.getUsersInChat().contains(requestingUser)) {
                chat.getUsersInChat().add(requestingUser);
                chatRepository.save(chat);
            }
            ChatDto chatDtoOutput = ChatMapper.toChatDtoShallow(chat);
            chatDtoOutput.setNewMessagesForUser(true);
            return chatDtoOutput;
        } catch (Exception e) {
            throw new NoSuchChatException();
        }
    }

    private boolean checkHasUnreadMessages(Chat chat, User user) {
        for (ChatMessage message : chat.getMessages()) {
            if (!message.getReadByIds().contains(user.getId())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public ChatDto createChat(String chatName, String initiatorUsername, String targetUsername) throws NoSuchUserException, DuplicateChatNameException {
        // getting relevant users
        User initiatorUser = userRepository.findByUsername(initiatorUsername);
        User targetUser = userRepository.findByUsername(targetUsername);
        if (targetUser == null || initiatorUser == null) {
            throw new NoSuchUserException();
        }

        // checking if chat with this name already exists
        Chat existingChat = chatRepository.findByChatName(chatName);
        if (existingChat != null) {
            System.out.println(chatName + "already exists");
            throw new DuplicateChatNameException();
        }

        // creating new chat and adding to db
        Chat newChat = new Chat();
        newChat.setChatName(chatName);
        newChat.setUsersInChat(new ArrayList<>(Arrays.asList(initiatorUser, targetUser)));
        newChat.setMessages(new ArrayList<>());
        chatRepository.save(newChat);

        // return corresponding chat dto
        ChatDto outputChatDto = ChatMapper.toChatDto(newChat);
        return outputChatDto;
    }

    @Override
    public ChatMessageDto saveMessageToChat(String chatId, ChatMessageDto inputChatMessageDto) throws NoSuchChatException, NoSuchUserException {
        // find user
        String username = inputChatMessageDto.getUsername();
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }

        // finding chat
        Long longChatId = Long.parseLong(chatId); // the chat's primary key is of type long
        Chat chat = chatRepository.findById(longChatId).orElse(null);
        // if no such chat, throw corresponding exception
        if (chat==null) {
            throw new NoSuchChatException();
        }

        // link user to chat if not their yet
        if (!chat.getUsersInChat().contains(user)) {
            chat.getUsersInChat().add(user);
            chatRepository.save(chat);
        }

        // creating message model and saving it to repository
        ChatMessage newMessage = new ChatMessage();
        newMessage.setContent(inputChatMessageDto.getContent());
        newMessage.setChat(chat);
        newMessage.setFormattedDateTime(inputChatMessageDto.getFormattedDateTime());
        newMessage.setSender(user);
        chatMessageRepository.save(newMessage);

        // returning corresponding chat message dto
        ChatMessageDto outputChatMessageDto = ChatMessageMapper.toChatMessageDto(newMessage);
        outputChatMessageDto.setChatName(chat.getChatName());
        return outputChatMessageDto;
    }

    public ChatService(ChatRepository chatRepository, ChatMessageRepository chatMessageRepository, UserRepository userRepository) {
        this.chatRepository = chatRepository;
        this.chatMessageRepository = chatMessageRepository;
        this.userRepository = userRepository;
    }


    @Override
    public List<ChatMessageDto> markUserReadChat(String chatIdStr, String username) throws NoSuchChatException, NoSuchUserException {
        Long chatId = Long.parseLong(chatIdStr);
        Chat chat = chatRepository.findById(chatId).orElse(null);
        if (chat == null) {
            throw new NoSuchChatException();
        }
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }
        Long userId = user.getId();

        List<ChatMessageDto> newReadMessages = new ArrayList<>();
        for (ChatMessage message : chat.getMessages()) {
            // if user not already read the message
            if (!message.getReadByIds().contains(userId)) {
                message.getReadByIds().add(userId);
                chatMessageRepository.save(message);
                // if the message is now read by all users, add to output list
                ChatMessageDto currentMessageDto = ChatMessageMapper.toShallowChatMessageDto(message);
                if (currentMessageDto.isRead()) {
                    newReadMessages.add(currentMessageDto);
                }
            }
        }
        return newReadMessages;
    }

    public ChatService() {}

}
