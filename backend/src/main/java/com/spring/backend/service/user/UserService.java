package com.spring.backend.service.user;

import com.spring.backend.dto.mapper.UserMapper;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.DuplicateUsernameException;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.exception.WrongCredentialsException;
import com.spring.backend.model.User;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.service.user.IUserService;
import com.spring.backend.utils.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    public UserService() {}

    // for injecting mock in unit tests
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto registerUser(UserDto userDtoInput) throws DuplicateUsernameException {
        // check if user with username already exists
        User user = userRepository.findByUsername(userDtoInput.getUsername());
        if (user != null) {
            throw new DuplicateUsernameException();
        }
        else { // no such user, create it and save to repository
            user = new User();
            user.setUsername(userDtoInput.getUsername());
            user.setCountry(userDtoInput.getCountry());
            user.setLanguage(userDtoInput.getLanguage());
            user.setStatus(userDtoInput.getStatus());
            user.setPassions(userDtoInput.getPassions());
            // encrypting password
            String salt = PasswordUtils.getSalt();
            user.setPassword(PasswordUtils.generateSecurePassword(userDtoInput.getPassword(), salt));

            userRepository.save(user); // save new user

            // return newly added user dto
            UserDto userDtoOutput = UserMapper.toUserDtoHidePassword(user);
            return userDtoOutput;
        }
    }

    public UserDto loginRegularUser(String username, String password) throws WrongCredentialsException {
        User user = userRepository.findByUsername(username);
        // check user exists
        if (user == null) {
            throw new WrongCredentialsException();
        }
        // check passwords match
        boolean passwordsMatch = PasswordUtils.verifyUserPassword(password, user.getPassword(), PasswordUtils.getSalt());
        if (!passwordsMatch) {
            throw new WrongCredentialsException();
        }

        // success, give user it's details
        UserDto OutputUserDto = UserMapper.toUserDtoHidePassword(user);
        return OutputUserDto;
    }

    public Long getIdByUsername(String username) throws NoSuchUserException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NoSuchUserException();
        }
        return  user.getId();
    }


//    public List<UserDto> getAllUsers() {
//        List<User> users = this.userRepository.findAll();
//        List<UserDto> userDtos = users.stream().map
//                (u -> UserMapper.toUserDto(u)).collect(Collectors.toList());
//
//        return userDtos;
//    }

}
