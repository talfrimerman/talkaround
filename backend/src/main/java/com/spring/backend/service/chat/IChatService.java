package com.spring.backend.service.chat;

import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.exception.DuplicateChatNameException;
import com.spring.backend.exception.NoSuchChatException;
import com.spring.backend.exception.NoSuchUserException;

import java.util.List;
import java.util.Map;

/**
 * interface for all chat related services
 */
public interface IChatService {

    /**
     * get a map of user's chat name as key and corresponding chat dto as value
     * @param username
     * @return
     * @throws NoSuchUserException
     */
    public Map<String, ChatDto> getChatNameToChatMap(String username) throws NoSuchUserException;

    /**
     * get chat dto by id and mark all messages as read by user
     * @param id
     * @return chat dto by id
     * @throws NoSuchChatException
     */
    ChatDto getChat(String id, String username) throws NoSuchChatException;

    /**
     * creating a chat in the db with the initiator and the invited user
     * @param chatName
     * @param initiatorUsername
     * @param targetUsername
     * @return corresponding chatdto
     * @throws NoSuchUserException
     * @throws DuplicateChatNameException
     */
    public ChatDto createChat(String chatName, String initiatorUsername, String targetUsername) throws NoSuchUserException, DuplicateChatNameException;


    /**
     * saves a message to room id
     * @param inputChatMessageDto
     * @param rommId the chat id
     * @return a ChatMessageDto of the actually saved message
     */
    ChatMessageDto saveMessageToChat(String roomId, ChatMessageDto inputChatMessageDto) throws NoSuchChatException, NoSuchUserException;

    /**
     * Saves the user as a reader of every message in the chat, if wasn't saved already
     * @param chatIdStr
     * @param username
     * @return List of all the messages in the chat that are now read by all chat users, but weren't before
     */
    public List<ChatMessageDto> markUserReadChat(String chatIdStr, String username) throws NoSuchChatException, NoSuchUserException;

    /**
     * Links user to chat in database
     * @param id chat id
     * @param username
     * @return ChatDto with id and name of the chat the user was added to
     * @throws NoSuchChatException
     */
    public ChatDto linkUserToChat(String id, String username) throws NoSuchChatException;

}
