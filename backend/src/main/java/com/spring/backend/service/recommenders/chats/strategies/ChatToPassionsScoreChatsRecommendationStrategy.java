package com.spring.backend.service.recommenders.chats.strategies;

import com.spring.backend.dto.mapper.ChatMapper;
import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.model.Chat;
import com.spring.backend.model.User;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;

import java.util.*;

public class ChatToPassionsScoreChatsRecommendationStrategy extends ChatsRecommendationStrategy{


    private UserRepository userRepository;

    private ChatRepository chatRepository;

    private static int MAX_CANDIDATES_PER_PASSION = 40;


    public ChatToPassionsScoreChatsRecommendationStrategy(UserRepository userRepository, ChatRepository chatRepository) {
        this.userRepository = userRepository;
        this.chatRepository = chatRepository;
    }

    @Override
    public List<ChatPreviewDto> recommendChatsPreviews(UserDto user, int count) {
        List<String> targetUserPassions = user.getPassions();

        // build candidates list by looking at chats of users with same passions
        HashMap<ChatPreviewDto, Double> recommendedChatsMap = new HashMap<>();
        for (String passion : targetUserPassions) {
            int lookedAt = 0;
            List<User> usersByPassion = userRepository.getAllByPassion(passion);
            for (User u : usersByPassion) {
                for (Chat c : u.getChats()) {
                    if (!alreadyInChat(user.getId(), c)) {
                        lookedAt ++;
                        ChatPreviewDto currentChatPreviewDto = ChatMapper.toChatPreviewDto(c);
                        if (!recommendedChatsMap.containsKey(currentChatPreviewDto)) {
                            if (lookedAt <= MAX_CANDIDATES_PER_PASSION) {
                                Double currentChatScore = score(c, targetUserPassions);
                                if (recommendedChatsMap.size() < count) {
                                    recommendedChatsMap.put(currentChatPreviewDto, currentChatScore);
                                }
                                else if (currentChatScore > Collections.min(recommendedChatsMap.values())) {
                                    recommendedChatsMap.values().remove(Collections.min(recommendedChatsMap.values()));
                                    recommendedChatsMap.put(currentChatPreviewDto, currentChatScore);
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (lookedAt > MAX_CANDIDATES_PER_PASSION) {
                continue; // continue to next passion
            }
        }
        // return recommended chat previews with top scores
        return new ArrayList<>(recommendedChatsMap.keySet());
    }

    private boolean alreadyInChat(long id, Chat c) {
        for (User u : c.getUsersInChat()) {
            if (u.getId() == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Calculate score of a chat in respect to user's passions.
     * score is defined as:
     * log of total count of mutual passions between target user and users in the chat,
     * divided by number of users in chat for normalization
     */
    private Double score(Chat c, List<String> targetUserPassions) {
        double mutualPassionsCount = 0;
        List<User> usersInChat = c.getUsersInChat();
        for (User userInChat: usersInChat) {
            for (String targetPassion : targetUserPassions) {
                if (userInChat.getPassions().contains(targetPassion)) {
                    mutualPassionsCount++;
                }
            }
        }
        double score = Math.log(mutualPassionsCount) / usersInChat.size(); // normalize score
        return score;
    }
}
