package com.spring.backend.dto.mapper;

import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.model.ChatMessage;
import org.springframework.stereotype.Component;

@Component
public class ChatMessageMapper {

    public static ChatMessageDto toChatMessageDto (ChatMessage chatMessage) {
        ChatMessageDto chatMessageDto = new ChatMessageDto();
        chatMessageDto.setUsername(chatMessage.getSender().getUsername());
        chatMessageDto.setContent(chatMessage.getContent());
        chatMessageDto.setFormattedDateTime(chatMessage.getFormattedDateTime());
        chatMessageDto.setId(chatMessage.getId());
        boolean readByAll = chatMessage.getReadByIds().size() >=
                chatMessage.getChat().getUsersInChat().size();
        chatMessageDto.setRead(readByAll);
        chatMessageDto.setChatName(chatMessage.getChat().getChatName());
        return chatMessageDto;
    }

    /**
     * Map chat to  chat dto with without content field
     * @param chatMessage
     * @return Corresponding chat dto without content field (null)
     */
    public static ChatMessageDto toShallowChatMessageDto (ChatMessage chatMessage) {
        ChatMessageDto chatMessageDto = new ChatMessageDto();
        chatMessageDto.setUsername(chatMessage.getSender().getUsername());
        chatMessageDto.setFormattedDateTime(chatMessage.getFormattedDateTime());
        chatMessageDto.setId(chatMessage.getId());
        chatMessageDto.setChatName(chatMessage.getChat().getChatName());
        boolean readByAll = chatMessage.getReadByIds().size() >=
                chatMessage.getChat().getUsersInChat().size();
        chatMessageDto.setRead(readByAll);
        return chatMessageDto;
    }
}
