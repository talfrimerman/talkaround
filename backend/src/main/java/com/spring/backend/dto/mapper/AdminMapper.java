package com.spring.backend.dto.mapper;


import com.spring.backend.dto.model.AdminDto;
import com.spring.backend.model.Admin;
import org.springframework.stereotype.Component;

@Component
public class AdminMapper {

    public static AdminDto toAdminDto(Admin admin) {
        AdminDto adminDto = new AdminDto();
        adminDto.setUsername(admin.getUsername());
        adminDto.setPassword(admin.getPassword());
        return adminDto;
    }
}
