package com.spring.backend.dto.response.login;

public class UserJwtResponsePayload {

    private String username;
    private String jwt;
    private String language;


    public UserJwtResponsePayload() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
