package com.spring.backend.dto.response.admin.base_admin_dashboard.inner_dtos;

public class PassionCountPair {
    private String passion;
    private Long count;

    public PassionCountPair() {
    }

    public PassionCountPair(String passion, Long count) {
        this.passion = passion;
        this.count = count;
    }

    public String getPassion() {
        return passion;
    }

    public void setPassion(String passion) {
        this.passion = passion;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
