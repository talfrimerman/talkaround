package com.spring.backend.dto.response.login;

public class AdminJwtResponsePayload {

    private String jwt;
    private boolean isAdmin;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public AdminJwtResponsePayload() {
    }
}
