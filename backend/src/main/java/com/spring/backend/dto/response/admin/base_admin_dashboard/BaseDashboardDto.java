package com.spring.backend.dto.response.admin.base_admin_dashboard;

import com.spring.backend.dto.model.UserDto;
import com.spring.backend.dto.response.admin.base_admin_dashboard.inner_dtos.PassionCountPair;

import java.util.List;

/**
 * This class contains the basic data to display in the admin dashboard
 */
public class BaseDashboardDto {

    private long usersCount;
    private long messagesCount;
    private long chatsCount;
    private List<UserDto> newestUsers;
    private List<PassionCountPair> topPassionsCounts;

    public BaseDashboardDto() {
    }

    public long getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(long usersCount) {
        this.usersCount = usersCount;
    }

    public long getMessagesCount() {
        return messagesCount;
    }

    public void setMessagesCount(long messagesCount) {
        this.messagesCount = messagesCount;
    }

    public List<UserDto> getNewestUsers() {
        return newestUsers;
    }

    public void setNewestUsers(List<UserDto> newestUsers) {
        this.newestUsers = newestUsers;
    }

    public List<PassionCountPair> getTopPassionsCounts() {
        return topPassionsCounts;
    }

    public void setTopPassionsCounts(List<PassionCountPair> topPassionsCounts) {
        this.topPassionsCounts = topPassionsCounts;
    }

    public long getChatsCount() {
        return chatsCount;
    }

    public void setChatsCount(long chatsCount) {
        this.chatsCount = chatsCount;
    }
}
