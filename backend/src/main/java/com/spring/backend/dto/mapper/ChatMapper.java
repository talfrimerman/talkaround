package com.spring.backend.dto.mapper;

import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.dto.response.admin.chat_analytics.inner_dtos.ChatMetaDataDto;
import com.spring.backend.model.Chat;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ChatMapper {

    public static ChatDto toChatDto(Chat chat) {

        // build message dtos list
        List<ChatMessageDto> mappedMessagesList = new ArrayList<>();
        chat.getMessages().forEach(message ->
            mappedMessagesList.add(ChatMessageMapper.toChatMessageDto(message)));

        // build user dtos list
        List<UserDto> mappedUsersList = new ArrayList<>();
        chat.getUsersInChat().forEach(user ->
                mappedUsersList.add(UserMapper.toUserDtoHidePassword(user)));

        // build the dto
        ChatDto chatDto = new ChatDto();
        chatDto.setId(chat.getId());
        chatDto.setChatName(chat.getChatName());
        chatDto.setMessages(mappedMessagesList);
        chatDto.setUsers(mappedUsersList);

        return chatDto;
    }

    /**
     * returns a shallow version of corresponding dto-
     * just id and name
     * @param chat
     * @return
     */
    public static ChatDto toChatDtoShallow(Chat chat) {
        // build the dto
        ChatDto chatDto = new ChatDto();
        chatDto.setId(chat.getId());
        chatDto.setChatName(chat.getChatName());
        return chatDto;
    }

    public static ChatPreviewDto toChatPreviewDto(Chat chat) {
        ChatPreviewDto chatPreviewDto = new ChatPreviewDto();
        chatPreviewDto.setId(chat.getId());
        chatPreviewDto.setChatName(chat.getChatName());
        chatPreviewDto.setTopPassions(new ArrayList<>());

        List<String> usernames = new ArrayList<>();
        chat.getUsersInChat().forEach(user ->
                usernames.add(user.getUsername()));
        chatPreviewDto.setUsernames(usernames);

        return chatPreviewDto;
    }

    public static ChatMetaDataDto toChatMetaData(Chat chat) {
        ChatMetaDataDto chatMetaDataDto = new ChatMetaDataDto();
        chatMetaDataDto.setId(chat.getId());
        chatMetaDataDto.setChatName(chat.getChatName());
        chatMetaDataDto.setMessagesCount((long)chat.getMessages().size());
        chatMetaDataDto.setUsersCount((long)chat.getUsersInChat().size());
        chatMetaDataDto.setLastMessageTimestamp(getLastMessageTimestamp(chat));
        return chatMetaDataDto;
    }

    private static String getLastMessageTimestamp(Chat chat) {
        try {
            return chat.getMessages()
                    .get(chat.getMessages().size()-1)
                    .getFormattedDateTime();
        }
        catch (Exception e) {
            return null;
        }

    }
}
