package com.spring.backend.dto.mapper;

import com.spring.backend.dto.model.UserDto;
import com.spring.backend.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    /**
     * map user model to user dto
     * @param user user to map
     * @return corresponding userDto based on input user model with the same field values
     */
    public static UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setLanguage(user.getLanguage());
        userDto.setCountry(user.getCountry());
        userDto.setStatus(user.getStatus());
        userDto.setPassions(user.getPassions());
        return userDto;
    }

    /**
     * map user model to user dto for response,
     * i.e put empty string instead of password
     * @param user user to map
     * @return corresponding userDto based on input user model with the same field values, but empty string password
     */
    public static UserDto toUserDtoHidePassword(User user) {
        UserDto userDto = toUserDto(user);
        userDto.setPassword("");
        return userDto;
    }
}
