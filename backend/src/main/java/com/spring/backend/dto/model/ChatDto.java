package com.spring.backend.dto.model;

import java.util.List;

public class ChatDto {

    private Long id;
    private String chatName;
    private List<ChatMessageDto> messages;
    private List<UserDto> users;
    private boolean hasUnreadMessagesForUser;

    public ChatDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public List<ChatMessageDto> getMessages() {
        return messages;
    }

    public void setMessages(List<ChatMessageDto> messages) {
        this.messages = messages;
    }

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

    public boolean isNewMessagesForUser() {
        return hasUnreadMessagesForUser;
    }

    public void setNewMessagesForUser(boolean newMessagesForUser) {
        this.hasUnreadMessagesForUser = newMessagesForUser;
    }
}
