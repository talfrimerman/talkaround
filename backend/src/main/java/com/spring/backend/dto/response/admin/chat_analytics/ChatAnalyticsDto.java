package com.spring.backend.dto.response.admin.chat_analytics;

import com.spring.backend.dto.response.admin.chat_analytics.inner_dtos.ChatMetaDataDto;
import com.spring.backend.model.Chat;

import java.util.List;

public class ChatAnalyticsDto {

    private List<ChatMetaDataDto> mostUsersChats;
    private List<ChatMetaDataDto> mostMessagesChats;

    public ChatAnalyticsDto() {
    }

    public List<ChatMetaDataDto> getMostUsersChats() {
        return mostUsersChats;
    }

    public void setMostUsersChats(List<ChatMetaDataDto> mostUsersChats) {
        this.mostUsersChats = mostUsersChats;
    }

    public List<ChatMetaDataDto> getMostMessagesChats() {
        return mostMessagesChats;
    }

    public void setMostMessagesChats(List<ChatMetaDataDto> mostMessagesChats) {
        this.mostMessagesChats = mostMessagesChats;
    }
}
