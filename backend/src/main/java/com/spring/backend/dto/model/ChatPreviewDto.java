package com.spring.backend.dto.model;

import java.util.List;
import java.util.Objects;

public class ChatPreviewDto {

    private Long id;
    private String chatName;
    private List<String> usernames;
    private List<String> topPassions;

    public ChatPreviewDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public List<String> getTopPassions() {
        return topPassions;
    }

    public void setTopPassions(List<String> topPassions) {
        this.topPassions = topPassions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatPreviewDto that = (ChatPreviewDto) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
