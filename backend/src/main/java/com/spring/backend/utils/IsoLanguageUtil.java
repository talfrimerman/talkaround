package com.spring.backend.utils;

import java.util.Locale;
import java.util.Set;

/**
 * Util class to validate valid languages, based on ISO 639
 */
public final class IsoLanguageUtil {
    private static final Set<String> ISO_LANGUAGES = Set.of(Locale.getISOLanguages());

    private IsoLanguageUtil() {
    }

    public static boolean isValidISOLanguage(String s) {
        return ISO_LANGUAGES.contains(s);
    }

}

