package com.spring.backend.utils;

import java.util.Arrays;
import java.util.List;

public class PassionUtils {

    public static List<String> passions = Arrays.asList(
            "Coffee", "Running", "Art", "Baking", "Fashion",
            "Surfing", "Disney", "Golf", "Writer", "Comedy",
            "Tea", "Wine", "Netflix", "Museum", "Walking",
            "Kareoke", "Yoga", "Cooking", "Gamer", "Politics",
            "Blogging", "Astrology", "Trivia", "Athlete", "Cycling"
            );
}
