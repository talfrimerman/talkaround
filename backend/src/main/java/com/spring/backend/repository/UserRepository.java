package com.spring.backend.repository;

import com.spring.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    List<User> findTop3ByOrderByIdDesc();

    @Query("SELECT COUNT(u) FROM User u WHERE ?1 member of u.passions")
    Long countByPassion(String passion);

    @Query("SELECT u FROM User u WHERE ?1 member of u.passions")
    List<User> getAllByPassion(String passion);
}


