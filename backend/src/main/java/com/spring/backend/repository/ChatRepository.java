package com.spring.backend.repository;

import com.spring.backend.model.Chat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {

    Chat findByChatName(String chatName);

    @Query("SELECT c FROM Chat c ORDER BY c.messages.size DESC")
    List<Chat> getWithMostMessages(Pageable pageable);

    @Query("SELECT c FROM Chat c ORDER BY c.usersInChat.size DESC")
    List<Chat> getWithMostUsers(Pageable pageable);

}
