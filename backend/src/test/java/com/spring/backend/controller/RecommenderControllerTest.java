package com.spring.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.backend.controller.api.ChatController;
import com.spring.backend.controller.api.RecommenderController;
import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.service.recommenders.RecommenderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(RecommenderController.class)
@WebMvcTest(value= RecommenderController.class, useDefaultFilters = false, includeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RecommenderController.class)})
public class RecommenderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecommenderService recommenderService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void successRecommendUsersTest() throws Exception {
        String username = "SomeUserName";

        when(recommenderService.recommendUsers(any())).thenReturn(new ArrayList<UserDto>());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/recommend/users/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @Test
    public void badUsernameRecommendUsersTest() throws Exception {
        String username = "BadUserName";

        when(recommenderService.recommendUsers(any())).thenThrow(new NoSuchUserException());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/recommend/users/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

    @Test
    public void successRecommendChatsTest() throws Exception {
        String username = "SomeUserName";

        when(recommenderService.recommendChats(any())).thenReturn(new ArrayList<ChatPreviewDto>());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/recommend/chats/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.payload").isEmpty());

    }

    @Test
    public void badUsernameRecommendChatsTest() throws Exception {
        String username = "BadUserName";

        when(recommenderService.recommendChats(any())).thenThrow(new NoSuchUserException());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/recommend/chats/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

}
