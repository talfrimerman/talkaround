package com.spring.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.backend.BackendApplication;
import com.spring.backend.WebSecurityConfig;
import com.spring.backend.controller.api.ChatController;
import com.spring.backend.controller.request.CreateChatRequest;
import com.spring.backend.controller.request.LinkUserToChatRequest;
import com.spring.backend.controller.request.MarkReadChatRequest;
import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.dto.response.Response;
import com.spring.backend.exception.DuplicateChatNameException;
import com.spring.backend.exception.NoSuchChatException;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.service.chat.ChatService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value=ChatController.class, useDefaultFilters = false, includeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = ChatController.class)})
////@ExtendWith(SpringExtension.class)
//@WebMvcTest(ChatController.class)
public class ChatControllerTest {

    @Autowired
    private ChatController chatController;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ChatService chatService;

    @MockBean
    private SimpMessageSendingOperations messagingTemplate;

    @BeforeEach()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * test valid chat map request
     *
     * @throws Exception
     */
    @Test
    public void successGetChatNameIdMapTest() throws Exception {
        String username = "user1";
        Long id = 1L;
        ChatDto expectedChatDto = new ChatDto();
        expectedChatDto.setId(id);
        Map<String, ChatDto> mockedMap = new HashMap<>();
        mockedMap.put(username, expectedChatDto);
        when(chatService.getChatNameToChatMap(any())).thenReturn(mockedMap);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/chat/chatsMap/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.user1.id").value(id));
    }

    /**
     * test chat map request with non-existing username
     *
     * @throws Exception
     */
    @Test
    public void invalidUserGetChatNameIdMapTest() throws Exception {
        String username = "user1";
        Long id = 1L;
        Map<String, Long> mockedMap = new HashMap<>();
        mockedMap.put(username, id);
        when(chatService.getChatNameToChatMap(any())).thenThrow(NoSuchUserException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/chat/chatsMap/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("WRONG_CREDENTIALS"));
    }

    /**
     * test valid chat creation
     *
     * @throws Exception
     */
    @Test
    public void successCreateChatTest() throws Exception {
        String initiatorUsername = "user1";
        String targetUsername = "user2";
        String chatName = "new chat";
        CreateChatRequest mockedRequest = new CreateChatRequest();
        mockedRequest.setChatName(chatName);
        mockedRequest.setInitiatorUsername(initiatorUsername);
        mockedRequest.setTargetUsername(targetUsername);

        ChatDto expectedResponse = new ChatDto();
        expectedResponse.setChatName(chatName);

        when(chatService.createChat(chatName, initiatorUsername, targetUsername)).thenReturn(expectedResponse);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.chatName").value(chatName));
    }

    /**
     * test case for trying to create chat with name that already exists
     *
     * @throws Exception
     */
    @Test
    public void existingChatCreateChatTest() throws Exception {
        String initiatorUsername = "user1";
        String targetUsername = "user2";
        String chatName = "new chat";
        CreateChatRequest mockedRequest = new CreateChatRequest();
        mockedRequest.setChatName(chatName);
        mockedRequest.setInitiatorUsername(initiatorUsername);
        mockedRequest.setTargetUsername(targetUsername);

        ChatDto expectedResponse = new ChatDto();
        expectedResponse.setChatName(chatName);

        when(chatService.createChat(chatName, initiatorUsername, targetUsername)).thenThrow(DuplicateChatNameException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("DUPLICATE_ENTITY"));
    }

    /**
     * test case for trying to create chat where one of the users doesn't exist
     *
     * @throws Exception
     */
    @Test
    public void invalidUserCreateChatTest() throws Exception {
        String initiatorUsername = "user1";
        String targetUsername = "user2";
        String chatName = "new chat";
        CreateChatRequest mockedRequest = new CreateChatRequest();
        mockedRequest.setChatName(chatName);
        mockedRequest.setInitiatorUsername(initiatorUsername);
        mockedRequest.setTargetUsername(targetUsername);

        ChatDto expectedResponse = new ChatDto();
        expectedResponse.setChatName(chatName);

        when(chatService.createChat(chatName, initiatorUsername, targetUsername)).thenThrow(NoSuchUserException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

    /**
     * test valid get chat
     *
     * @throws Exception
     */
    @Test
    public void successGetChatTest() throws Exception {
        String inputId = "5";
        String expectedChatName = "new chat";
        String username = "user1";

        ChatDto expectedResponse = new ChatDto();
        expectedResponse.setChatName(expectedChatName);

        when(chatService.getChat(inputId, username)).thenReturn(expectedResponse);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/chat/" + inputId + "/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.chatName").value(expectedChatName));
    }

    /**
     * test case of trying to get a non-existing chat
     *
     * @throws Exception
     */
    @Test
    public void invalidChatGetChatTest() throws Exception {
        String inputId = "5";
        String username = "user1";
        when(chatService.getChat(inputId, username)).thenThrow(NoSuchChatException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/chat/" + inputId +"/" + username))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

    /**
     * test case for valid send message request
     */
    @Test
    public void successSendMessageTest() throws NoSuchUserException, NoSuchChatException {
        String chatId = "500";
        ChatMessageDto inputMessage = new ChatMessageDto();
        inputMessage.setContent("test message");

        when(chatService.saveMessageToChat(chatId, inputMessage)).thenReturn(inputMessage);
        Response output = chatController.sendMessage(chatId, inputMessage);
        assertEquals("OK", output.getStatus().toString());

    }

    /**
     * test case for sending a message by a non existing user
     */
    @Test
    public void invalidUserSendMessageTest() throws NoSuchUserException, NoSuchChatException {
        String chatId = "500";
        ChatMessageDto inputMessage = new ChatMessageDto();
        inputMessage.setContent("test message");

        when(chatService.saveMessageToChat(any(), any())).thenThrow(NoSuchUserException.class);
        Response output = chatController.sendMessage(chatId, inputMessage);
        assertEquals("WRONG_CREDENTIALS", output.getStatus().toString());
    }

    /**
     * test case for attempting to send a message for non-existing chat
     */
    @Test
    public void invalidChatSendMessageTest() throws NoSuchUserException, NoSuchChatException {
        String chatId = "500";
        ChatMessageDto inputMessage = new ChatMessageDto();
        inputMessage.setContent("test message");

        when(chatService.saveMessageToChat(any(), any())).thenThrow(NoSuchChatException.class);
        Response output = chatController.sendMessage(chatId, inputMessage);
        assertEquals("BAD_REQUEST", output.getStatus().toString());
    }

    /**
     * test case for when chat id is not a string representing a Long
     */
    @Test
    public void invalidChatIdSendMessageTest() throws NoSuchUserException, NoSuchChatException {
        String chatId = "500ABC";
        ChatMessageDto inputMessage = new ChatMessageDto();
        inputMessage.setContent("test message");

        when(chatService.saveMessageToChat(any(), any())).thenThrow(NoSuchChatException.class);
        Response output = chatController.sendMessage(chatId, inputMessage);
        assertEquals("BAD_REQUEST", output.getStatus().toString());
    }

    /**
     * test valid mark chat as read by user
     *
     * @throws Exception
     */
    @Test
    public void successMarkChatAsReadTest() throws Exception {
        MarkReadChatRequest mockedRequest = new MarkReadChatRequest();
        mockedRequest.setChatId("1");
        mockedRequest.setUsername("user1");

        when(chatService.markUserReadChat(mockedRequest.getChatId(), mockedRequest.getUsername()))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/userRead")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    /**
     * test attempt to mark chat as read by non-existing user
     * @throws Exception
     */
    @Test
    public void invalidUserMarkChatAsReadTest() throws Exception {
        MarkReadChatRequest mockedRequest = new MarkReadChatRequest();
        mockedRequest.setChatId("1");
        mockedRequest.setUsername("user1");

        when(chatService.markUserReadChat(mockedRequest.getChatId(), mockedRequest.getUsername()))
                .thenThrow(NoSuchUserException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/userRead")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

    /**
     * test attempt to mark invalid chat as read by a user
     * @throws Exception
     */
    @Test
    public void invalidChatMarkChatAsReadTest() throws Exception {
        MarkReadChatRequest mockedRequest = new MarkReadChatRequest();
        mockedRequest.setChatId("1");
        mockedRequest.setUsername("user1");

        when(chatService.markUserReadChat(mockedRequest.getChatId(), mockedRequest.getUsername()))
                .thenThrow(NoSuchChatException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/userRead")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }

    /**
     * test valid link user to chat
     * @throws Exception
     */
    @Test
    public void successLinkUserToChatTest() throws Exception {
        LinkUserToChatRequest mockedRequest = new LinkUserToChatRequest();
        mockedRequest.setChatId("1");
        mockedRequest.setUsername("user1");

        ChatDto mockedOutput = new ChatDto();
        mockedOutput.setId(1L);

        when(chatService.linkUserToChat(mockedRequest.getChatId(), mockedRequest.getUsername()))
                .thenReturn(mockedOutput);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/linkUserChat")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.payload.id").value(1L));
    }

    /**
     * test attempt to link user to invalid chat
     * @throws Exception
     */
    @Test
    public void invalidChatLinkUserToChatTest() throws Exception {
        LinkUserToChatRequest mockedRequest = new LinkUserToChatRequest();
        mockedRequest.setChatId("1");
        mockedRequest.setUsername("user1");

        ChatDto mockedOutput = new ChatDto();
        mockedOutput.setId(1L);

        when(chatService.linkUserToChat(mockedRequest.getChatId(), mockedRequest.getUsername()))
                .thenThrow(NoSuchChatException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/chat/linkUserChat")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(mockedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));
    }
}
