package com.spring.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.backend.controller.api.AdminController;
import com.spring.backend.controller.api.ChatController;
import com.spring.backend.dto.response.admin.base_admin_dashboard.BaseDashboardDto;
import com.spring.backend.dto.response.admin.chat_analytics.ChatAnalyticsDto;
import com.spring.backend.service.admin.AdminService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value= AdminController.class, useDefaultFilters = false, includeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = AdminController.class)})
public class AdminControllerTest {

    @Autowired
    private AdminController adminController;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    AdminService adminService;

    @BeforeEach()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getBaseDashboardSuccessTest() throws Exception {
        BaseDashboardDto data = new BaseDashboardDto();
        when(adminService.getBaseDashboardData()).thenReturn(data);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/admin/baseDashboard"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @Test
    public void getChatAnalyticsSuccessTest() throws Exception {
        ChatAnalyticsDto data = new ChatAnalyticsDto();
        when(adminService.getChatAnalyticsData()).thenReturn(data);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/admin/chatAnalytics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }


}
