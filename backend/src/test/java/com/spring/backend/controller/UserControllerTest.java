package com.spring.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.backend.controller.api.UserController;
import com.spring.backend.controller.request.LoginRequest;
import com.spring.backend.controller.request.RegisterRequest;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.DuplicateUsernameException;
import com.spring.backend.exception.WrongCredentialsException;
import com.spring.backend.security.JwtHelper;
import com.spring.backend.service.admin.AdminService;
import com.spring.backend.service.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value= UserController.class, useDefaultFilters = false, includeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = UserController.class)})
////@ExtendWith(SpringExtension.class)
//@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @MockBean
    private AdminService adminService;

    @MockBean
    private JwtHelper jwtHelper;

    @BeforeEach()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void successRegisterTest() throws Exception {
        //creating register request object
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user1");
        registerRequest.setPassword("aA123456");
        registerRequest.setCountry("Israel");
        registerRequest.setLanguage("en");

        // test normal register
        when(userService.registerUser(any())).thenReturn(new UserDto());    // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerRequest))
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @Test
    void duplicateUsernameRegisterTest() throws Exception {
        //creating register request object
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user1");
        registerRequest.setPassword("aA123456");
        registerRequest.setCountry("Israel");
        registerRequest.setLanguage("en");

        // test duplicate user register
        when(userService.registerUser(any())).thenThrow(new DuplicateUsernameException());  // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerRequest))
        ).andExpect(jsonPath("$.status").value("DUPLICATE_ENTITY"));
    }

    @Test
    void invalidPasswordLengthRegisterTest() throws Exception {
        //creating register request object
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user1");
        registerRequest.setPassword("aaaaaaA");
        registerRequest.setCountry("Israel");
        registerRequest.setLanguage("en");

        // test duplicate user register
        when(userService.registerUser(any())).thenReturn(new UserDto());  // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerRequest))
        ).andExpect(jsonPath("$.status").value("VALIDATION_EXCEPTION"));
    }

    @Test
    void tooLongUsernameRegisterTest() throws Exception {
        //creating register request object
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("userrrrrrrrrrrrr");
        registerRequest.setPassword("aA123456");
        registerRequest.setCountry("Israel");
        registerRequest.setLanguage("en");

        when(userService.registerUser(any())).thenReturn(new UserDto());  // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerRequest))
        ).andExpect(jsonPath("$.status").value("VALIDATION_EXCEPTION"));
    }

    @Test
    void invalidLanguageRegisterTest() throws Exception {
        //creating register request object
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user1");
        registerRequest.setPassword("a");
        registerRequest.setCountry("Israel");
        registerRequest.setLanguage("fakeLang!&^");

        when(userService.registerUser(any())).thenReturn(new UserDto());  // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerRequest))
        ).andExpect(jsonPath("$.status").value("VALIDATION_EXCEPTION"));
    }

    @Test
    void successLoginTest() throws Exception {
        //creating register request object
        String username = "user1";
        String password = "Aa123456";
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setPassword(password);

        UserDto shouldReturn = new UserDto();
        shouldReturn.setUsername(username);

        // test normal register
        when(userService.loginRegularUser(anyString(),anyString())).thenReturn(shouldReturn);    // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequest))
                ).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.payload.username").value(username));

    }

    @Test
    void wrongCredentialsLoginTest() throws Exception {
        //creating register request object
        String username = "user1";
        String password = "Aa123456";
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setPassword(password);

        // test normal register
        when(userService.loginRegularUser(anyString(),anyString())).thenThrow(new WrongCredentialsException());    // mock service layer
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequest))
                ).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("WRONG_CREDENTIALS"));
    }



}
