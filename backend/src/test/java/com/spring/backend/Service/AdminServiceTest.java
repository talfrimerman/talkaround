package com.spring.backend.Service;

import com.spring.backend.dto.response.admin.base_admin_dashboard.BaseDashboardDto;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.model.Chat;
import com.spring.backend.repository.AdminRepository;
import com.spring.backend.repository.ChatMessageRepository;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.service.admin.AdminService;
import com.spring.backend.service.recommenders.RecommenderService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig
public class AdminServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ChatRepository chatRepository;

    @MockBean
    private AdminRepository adminRepository;

    @MockBean
    private ChatMessageRepository chatMessageRepository;

    @Test
    public void getChatAnalyticsDataTest() {
        List<Chat> mockedMostUsersChats = new ArrayList<>();
        Chat mostUsersChat = new Chat();
        mostUsersChat.setId(1);
        mockedMostUsersChats.add(mostUsersChat);
        when(chatRepository.getWithMostUsers(any())).thenReturn(mockedMostUsersChats);

        List<Chat> mockedMostMessagesChats = new ArrayList<>();
        Chat mostMessagesChat = new Chat();
        mostMessagesChat.setId(2);
        mockedMostMessagesChats.add(mostMessagesChat);
        when(chatRepository.getWithMostMessages(any())).thenReturn(mockedMostMessagesChats);

        AdminService adminService = new AdminService(adminRepository, userRepository, chatMessageRepository, chatRepository);
        assertEquals(mostUsersChat.getId(), adminService.getChatAnalyticsData().getMostUsersChats().get(0).getId());
        assertEquals(mostMessagesChat.getId(), adminService.getChatAnalyticsData().getMostMessagesChats().get(0).getId());
    }

    @Test
    public void getBaseDashboardDataTest() {
        Long mockedUsersCount = 7L;
        Long mockedMessagesCount = 2L;
        Long mockedChatsCount = 1L;
        when(userRepository.count()).thenReturn(mockedUsersCount);
        when(chatMessageRepository.count()).thenReturn(mockedMessagesCount);
        when(chatRepository.count()).thenReturn(mockedChatsCount);


        AdminService adminService = new AdminService(adminRepository, userRepository, chatMessageRepository, chatRepository);
        BaseDashboardDto output = adminService.getBaseDashboardData();
        assertEquals(mockedUsersCount, output.getUsersCount());
        assertEquals(mockedMessagesCount, output.getMessagesCount());
        assertEquals(mockedChatsCount, output.getChatsCount());
    }

}
