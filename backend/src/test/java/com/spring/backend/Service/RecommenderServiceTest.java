package com.spring.backend.Service;


import com.spring.backend.dto.model.ChatPreviewDto;
import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.model.Chat;
import com.spring.backend.model.User;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.service.recommenders.RecommenderService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig
public class RecommenderServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ChatRepository chatRepository;

    @Test
    public void badUsernameRecommendUsersTest() {
        String username = "BadUserName";
        when(userRepository.findByUsername(username)).thenReturn(null); // mock bad username

        RecommenderService recommenderService = new RecommenderService(userRepository);
        assertThrows(NoSuchUserException.class, () ->
                recommenderService.recommendUsers(username));

    }

    // test case of no users who share passions with user (should be empty list)
    @Test
    public void noOtherUsersRecommendUsersTest() throws NoSuchUserException {
        String username = "username";
        List<String> passionsList = List.of("Coffee", "Running", "Art");

        when(userRepository.findByUsername(username)).thenReturn(new User(username,"pass","en","Canada","status",passionsList));
        when(userRepository.getAllByPassion(any())).thenReturn(new ArrayList<User>());

        RecommenderService recommenderService = new RecommenderService(userRepository);
        List<UserDto> output = recommenderService.recommendUsers(username);

        assertEquals(0, output.size());
    }

    // test case of one suitable user
    @Test
    public void singleRecommendedUserRecommendUsersTest() throws NoSuchUserException {
        String targetUsername = "username";
        String candidateUsername = "candidate";

        List<String> passionsListTarget = List.of("Coffee", "Running", "Art");
        List<String> passionsListCandidate = List.of("Coffee", "Running", "Netflix");

        when(userRepository.findByUsername(targetUsername)).thenReturn(new User(targetUsername,"pass","en","Canada","status",passionsListTarget));
        when(userRepository.getAllByPassion(any())).thenReturn
                (new ArrayList<User>(Arrays.asList(new User(candidateUsername,"pass","en","Canada","status",passionsListCandidate))));

        RecommenderService recommenderService = new RecommenderService(userRepository);
        List<UserDto> output = recommenderService.recommendUsers(targetUsername);

        assertEquals(1, output.size());
        assertEquals(candidateUsername, output.get(0).getUsername()); // check we got the candidate user as a recommended user
    }


    /**
     * test case of 6 candidate users, should choose 5
     * and have the best match user among them
     */
    @Test
    public void choose5OutOf6RecommendUsersTest() throws NoSuchUserException {
        List<String> passionsListTarget = List.of("Coffee", "Running", "Art");
        List<String> passionsListCandidates = List.of("Coffee", "Running", "Netflix");

        User targetUser = new User("target","pass","en","Canada","status",passionsListTarget);
        User bestMatchCandidate = new User("importantCandidate","pass","en","Canada","status",passionsListTarget);


        when(userRepository.findByUsername(targetUser.getUsername())).thenReturn(targetUser);
        when(userRepository.getAllByPassion(any())).thenReturn
                (new ArrayList<User>(Arrays.asList(
                        bestMatchCandidate,
                        new User("candidate2","pass","en","Canada","status",passionsListCandidates),
                        new User("candidate3","pass","en","Canada","status",passionsListCandidates),
                        new User("candidate4","pass","en","Canada","status",passionsListCandidates),
                        new User("candidate5","pass","en","Canada","status",passionsListCandidates),
                        new User("candidate6","pass","en","Canada","status",passionsListCandidates)
                )));

        RecommenderService recommenderService = new RecommenderService(userRepository);
        List<UserDto> output = recommenderService.recommendUsers(targetUser.getUsername());

        assertEquals(5, output.size());
        assertThat(output, hasItem(allOf(
            hasProperty("username", is(bestMatchCandidate.getUsername()))
        ))); // check we got the best candidate user as a recommended user
    }

    @Test
    public void badUsernameRecommendChatsTest() {
        String username = "BadUserName";
        when(userRepository.findByUsername(username)).thenReturn(null); // mock bad username

        RecommenderService recommenderService = new RecommenderService(userRepository, chatRepository);
        assertThrows(NoSuchUserException.class, () ->
                recommenderService.recommendChats(username));
    }

    // test case of no users who share passions with user (should be empty list)
    @Test
    public void noOtherUsersRecommendChatsTest() throws NoSuchUserException {
        String username = "username";
        List<String> passionsList = List.of("Coffee", "Running", "Art");

        when(userRepository.findByUsername(username)).thenReturn(new User(username,"pass","en","Canada","status",passionsList));
        when(userRepository.getAllByPassion(any())).thenReturn(new ArrayList<User>());

        RecommenderService recommenderService = new RecommenderService(userRepository, chatRepository);
        List<UserDto> output = recommenderService.recommendUsers(username);

        assertEquals(0, output.size());
    }

}
