package com.spring.backend.Service;

import com.spring.backend.dto.model.ChatDto;
import com.spring.backend.dto.model.ChatMessageDto;
import com.spring.backend.exception.DuplicateChatNameException;
import com.spring.backend.exception.NoSuchChatException;
import com.spring.backend.exception.NoSuchUserException;
import com.spring.backend.model.Chat;
import com.spring.backend.model.ChatMessage;
import com.spring.backend.model.User;
import com.spring.backend.repository.ChatMessageRepository;
import com.spring.backend.repository.ChatRepository;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.service.chat.ChatService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.mockito.Mockito.*;


@SpringJUnitConfig
public class ChatServiceTest {

    @MockBean
    private ChatRepository chatRepository;

    @MockBean
    private ChatMessageRepository chatMessageRepository;

    @MockBean
    private UserRepository userRepository;

    /**
     * test expected chats map for user is returned
     * @throws NoSuchUserException
     */
    @Test
    void successfulGetChatNameIdMapTest() throws NoSuchUserException {
        // creating mocked user
        User user = new User();
        user.setUsername("user1");

        // creating mocked chat list
        List<Chat> chatList = new ArrayList<>();
        Chat chat1 = new Chat();
        chat1.setId(1);
        chat1.setChatName("chat1");
        Chat chat2 = new Chat();
        chat2.setId(2);
        chat2.setChatName("chat2");
        chatList.add(chat1);
        chatList.add(chat2);

        // assigning chats to user
        user.setChats(chatList);

        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        Map<String, ChatDto> output = chatService.getChatNameToChatMap(user.getUsername());

        assertEquals(2, output.size());
        assertTrue(output.containsKey(chat1.getChatName()) && output.get(chat1.getChatName()).getId() == chat1.getId());
        assertTrue(output.containsKey(chat2.getChatName()) && output.get(chat2.getChatName()).getId() == chat2.getId());
    }

    /**
     * test empty chat map is returned for user that isn't  linked to chats
     * @throws NoSuchUserException
     */
    @Test
    void noChatsGetChatNameIdMapTest() throws NoSuchUserException {
        // creating mocked user
        User user = new User();
        user.setUsername("user1");

        // creating mocked chat list
        List<Chat> chatList = new ArrayList<>();

        // assigning chats to user
        user.setChats(chatList);

        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        Map<String, ChatDto> output = chatService.getChatNameToChatMap(user.getUsername());

        assertEquals(0, output.size());
    }

    /**
     * test correct exception is thrown when trying to get chats map for a non-existing user
     * @throws NoSuchUserException
     */
    @Test
    void nonExistingUserGetChatNameIdMapTest() {
        when(userRepository.findByUsername(any())).thenReturn(null);
        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);

        assertThrows(NoSuchUserException.class, () ->
                chatService.getChatNameToChatMap("nonExistingUser"));
    }

    /**
     * should successfully create new chat with two users
     * @throws NoSuchUserException
     * @throws DuplicateChatNameException
     */
    @Test
    void successfulCreateChatTest() throws NoSuchUserException, DuplicateChatNameException {
        // creating mocked users
        User initiator = new User();
        initiator.setUsername("initiator");
        User invited = new User();
        invited.setUsername("invited");

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findByChatName(any())).thenReturn(null);
        when(userRepository.findByUsername(initiator.getUsername())).thenReturn(initiator);
        when(userRepository.findByUsername(invited.getUsername())).thenReturn(invited);


        ChatDto output = chatService.createChat("new chat",initiator.getUsername(),invited.getUsername());
        assertEquals("new chat", output.getChatName());
        assertEquals(2, output.getUsers().size());
        assertEquals(0, output.getMessages().size());
        assertThat(output.getUsers(), hasItem(hasProperty("username", is(initiator.getUsername()))));
        assertThat(output.getUsers(), hasItem(hasProperty("username", is(invited.getUsername()))));
    }

    /**
     * try to create chat when initiator not exists
     * @throws NoSuchUserException
     * @throws DuplicateChatNameException
     */
    @Test
    void nonExistingInitiatorCreateChatTest() throws NoSuchUserException, DuplicateChatNameException {
        // creating mocked users
        String username1 = "initiator";
        String username2 = "invited";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findByChatName(any())).thenReturn(null);
        when(userRepository.findByUsername(username1)).thenReturn(null);
        when(userRepository.findByUsername(username2)).thenReturn(new User());

        assertThrows(NoSuchUserException.class,
                () -> chatService.createChat("new chat", username1, username2));
        verify(userRepository, times(2)).findByUsername(any());

    }


    /**
     * try to create chat where the invited user is not existing
     * @throws NoSuchUserException
     * @throws DuplicateChatNameException
     */
    @Test
    void nonExistingInvitedCreateChatTest() throws NoSuchUserException, DuplicateChatNameException {
        // creating mocked users
        String username1 = "initiator";
        String username2 = "invited";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findByChatName(any())).thenReturn(null);
        when(userRepository.findByUsername(username1)).thenReturn(new User());
        when(userRepository.findByUsername(username2)).thenReturn(null);

        assertThrows(NoSuchUserException.class,
                () -> chatService.createChat("new chat", username1, username2));
        verify(userRepository, times(2)).findByUsername(any());
        verify(chatRepository, times(0)).findByChatName(any());
    }

    /**
     * try to create chat with both non-existing users
     * @throws NoSuchUserException
     * @throws DuplicateChatNameException
     */
    @Test
    void nonExistingBothCreateChatTest() throws NoSuchUserException, DuplicateChatNameException {
        // creating mocked users
        String username1 = "initiator";
        String username2 = "invited";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findByChatName(any())).thenReturn(null);
        when(userRepository.findByUsername(username1)).thenReturn(null);
        when(userRepository.findByUsername(username2)).thenReturn(null);

        assertThrows(NoSuchUserException.class,
                () -> chatService.createChat("new chat", username1, username2));
        verify(userRepository, times(2)).findByUsername(any());
        verify(chatRepository, times(0)).findByChatName(any());

    }

    /**
     * try to create a chat with already existing name
     * @throws NoSuchUserException
     * @throws DuplicateChatNameException
     */
    @Test
    void duplicateChatNameCreateChatTest() throws NoSuchUserException, DuplicateChatNameException {
        // creating mocked users
        String duplicateChatName = "chat1";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findByChatName(any())).thenReturn(new Chat());
        when(userRepository.findByUsername(any())).thenReturn(new User());

        assertThrows(DuplicateChatNameException.class,
                () -> chatService.createChat(duplicateChatName, "user1", "user2"));
        verify(userRepository, times(2)).findByUsername(any());
        verify(chatRepository, times(1)).findByChatName(any());
    }

    /**
     * add message to chat by user already assigned to chat
     * @throws NoSuchChatException
     * @throws NoSuchUserException
     */
    @Test
    void successOldUserSaveMessageToChatTest() throws NoSuchChatException, NoSuchUserException {
        // building mocked input
        String roomId = "1";
        String username = "user1";
        String content = "message content";
        String formattedDateTime = "mm/dd 00:00";
        ChatMessageDto messageDtoToSave = new ChatMessageDto();
        messageDtoToSave.setUsername(username);
        messageDtoToSave.setFormattedDateTime(formattedDateTime);
        messageDtoToSave.setContent(content);

        // creating mocked sender
        User mockedUser = new User();
        mockedUser.setUsername(username);
        mockedUser.setChats(new ArrayList<>());

        // creating mocked chat to add the message to
        Chat mockedChat = new Chat();
        mockedChat.setChatName("chat1");
        mockedChat.setMessages(new ArrayList<>());
        mockedChat.setUsersInChat(new ArrayList<>());
        mockedChat.getUsersInChat().add(mockedUser);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(userRepository.findByUsername(username)).thenReturn(mockedUser);
        when(chatRepository.findById(Long.parseLong(roomId))).thenReturn(Optional.of(mockedChat));
        ChatMessageDto output = chatService.saveMessageToChat(roomId, messageDtoToSave);

        assertEquals(username, output.getUsername());
        assertEquals(content, output.getContent());
        assertEquals(formattedDateTime, output.getFormattedDateTime());
        verify(chatMessageRepository, times(1)).save(any(ChatMessage.class));
        verify(userRepository, times(0)).save(any());
    }

    /**
     * add message to chat by user not yet assigned to chat
     * @throws NoSuchChatException
     * @throws NoSuchUserException
     */
    @Test
    void successNewUserSaveMessageToChatTest() throws NoSuchChatException, NoSuchUserException {
        // building mocked input
        String roomId = "1";
        String username = "user1";
        String content = "message content";
        String formattedDateTime = "mm/dd 00:00";
        ChatMessageDto messageDtoToSave = new ChatMessageDto();
        messageDtoToSave.setUsername(username);
        messageDtoToSave.setFormattedDateTime(formattedDateTime);
        messageDtoToSave.setContent(content);

        // creating mocked sender
        User mockedUser = new User();
        mockedUser.setUsername(username);
        mockedUser.setChats(new ArrayList<>());

        // creating mocked chat to add the message to
        Chat mockedChat = new Chat();
        mockedChat.setChatName("chat1");
        mockedChat.setMessages(new ArrayList<>());
        mockedChat.setUsersInChat(new ArrayList<>());

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(userRepository.findByUsername(username)).thenReturn(mockedUser);
        when(chatRepository.findById(Long.parseLong(roomId))).thenReturn(Optional.of(mockedChat));
        ChatMessageDto output = chatService.saveMessageToChat(roomId, messageDtoToSave);

        assertEquals(username, output.getUsername());
        assertEquals(content, output.getContent());
        assertEquals(formattedDateTime, output.getFormattedDateTime());
        verify(chatMessageRepository, times(1)).save(any(ChatMessage.class));
        verify(chatRepository, times(1)).save(any());
    }

    /**
     * should successfully return chat dto
     */
    @Test
    void successfulGetChatTest() throws NoSuchChatException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        ChatDto output = chatService.getChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(mockedChat.getId(), output.getId());
        verify(chatRepository, times(1)).findById(any());
    }

    /**
     * test attempt to get non-existing chat. should throw corresponding exception
     */
    @Test
    void nonExistingChatGetChatTest() throws NoSuchChatException {
        String usernameInput = "user1";
        Chat mockedChat = new Chat();
        mockedChat.setId(1);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.empty());

       assertThrows(NoSuchChatException.class, ()->
                       chatService.getChat(Long.toString(mockedChat.getId()), usernameInput), usernameInput);
        verify(chatRepository, times(1)).findById(any());
    }

    /**
     * test attemp to get a chat with invalid id
     * (non-long id). should throw exception
     */
    @Test
    void invalidIdChatGetChatTest() throws NoSuchChatException {
        String invalidId = "1a";
        String usernameInput = "user1";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);

        assertThrows(NoSuchChatException.class, ()->
                chatService.getChat(invalidId, usernameInput));
        verify(chatRepository, times(0)).findById(any());
    }

    /**
     * should successfully mark user as read chat
     */
    @Test
    void successNoMessagesMarkUserReadChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        List<ChatMessageDto> output = chatService.markUserReadChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(0, output.size());
    }

    /**
     * Should successfully mark user as read chat, and return 0 messages as newly read
     */
    @Test
    void successAllSeenMarkUserReadChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);
        mockedChat.setMessages(new ArrayList<ChatMessage>());
        mockedChat.setUsersInChat(new ArrayList<>());

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(mockedUser);
        mockedUser.getChats().add(mockedChat);

        ChatMessage seenMessage = new ChatMessage();
        seenMessage.setReadByIds(new HashSet<>());
        seenMessage.getReadByIds().add(mockedUser.getId());

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        List<ChatMessageDto> output = chatService.markUserReadChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(0, output.size());
    }

    /**
     * Should successfully mark user as read chat, and return 1 (out of 1) message as newly read
     */
    @Test
    void successAllNotSeenMarkUserReadChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);
        mockedChat.setMessages(new ArrayList<ChatMessage>());
        mockedChat.setUsersInChat(new ArrayList<>());

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(mockedUser);
        mockedUser.getChats().add(mockedChat);

        ChatMessage unseenMessage = new ChatMessage();
        unseenMessage.setSender(new User());
        unseenMessage.setReadByIds(new HashSet<>());
        unseenMessage.setChat(mockedChat);
        mockedChat.getMessages().add(unseenMessage);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        List<ChatMessageDto> output = chatService.markUserReadChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(1, output.size());
    }

    /**
     * Should successfully mark user as read chat, and return 1 (out of 2) message as newly read
     */
    @Test
    void successSomeSeenMarkUserReadChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);
        mockedChat.setMessages(new ArrayList<ChatMessage>());
        mockedChat.setUsersInChat(new ArrayList<>());

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(mockedUser);
        mockedUser.getChats().add(mockedChat);

        ChatMessage unseenMessage = new ChatMessage();
        unseenMessage.setSender(new User());
        unseenMessage.setReadByIds(new HashSet<>());
        unseenMessage.setChat(mockedChat);
        mockedChat.getMessages().add(unseenMessage);

        ChatMessage seenMessage = new ChatMessage();
        seenMessage.setSender(new User());
        seenMessage.setReadByIds(new HashSet<>());
        seenMessage.setChat(mockedChat);
        mockedChat.getMessages().add(seenMessage);
        seenMessage.getReadByIds().add(mockedUser.getId());

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        List<ChatMessageDto> output = chatService.markUserReadChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(1, output.size());
    }

    /**
     * Should successfully mark user as read chat, and return 1 (out of 2) message as newly read,
     * + there is another user in the chat
     */
    @Test
    void successMultipleUsersMarkUserReadChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);
        mockedChat.setMessages(new ArrayList<ChatMessage>());
        mockedChat.setUsersInChat(new ArrayList<>());

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(mockedUser);
        mockedUser.getChats().add(mockedChat);

        User otherChatUser = new User();
        mockedUser.setUsername("user2");
        mockedUser.setId(2);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(otherChatUser);
        mockedUser.getChats().add(mockedChat);

        ChatMessage unseenMessage = new ChatMessage();
        unseenMessage.setSender(new User());
        unseenMessage.setReadByIds(new HashSet<>());
        unseenMessage.setChat(mockedChat);
        unseenMessage.getReadByIds().add(otherChatUser.getId());
        mockedChat.getMessages().add(unseenMessage);

        ChatMessage seenMessage = new ChatMessage();
        seenMessage.setSender(new User());
        seenMessage.setReadByIds(new HashSet<>());
        seenMessage.setChat(mockedChat);
        mockedChat.getMessages().add(seenMessage);
        seenMessage.getReadByIds().add(mockedUser.getId());
        seenMessage.getReadByIds().add(otherChatUser.getId());


        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        List<ChatMessageDto> output = chatService.markUserReadChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(1, output.size());
    }

    /**
     * Should successfully mark user as read chat, and return 2 (out of 2) message as newly read,
     * + there is another user in the chat
     */
    @Test
    void successSeenAllMultipleUsersMarkUserReadChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);
        mockedChat.setMessages(new ArrayList<ChatMessage>());
        mockedChat.setUsersInChat(new ArrayList<>());

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(mockedUser);
        mockedUser.getChats().add(mockedChat);

        User otherChatUser = new User();
        mockedUser.setUsername("user2");
        mockedUser.setId(2);
        mockedUser.setChats(new ArrayList<Chat>());
        mockedChat.getUsersInChat().add(otherChatUser);
        mockedUser.getChats().add(mockedChat);

        ChatMessage seenMessage = new ChatMessage();
        seenMessage.setSender(new User());
        seenMessage.setReadByIds(new HashSet<>());
        seenMessage.setChat(mockedChat);
        mockedChat.getMessages().add(seenMessage);
        seenMessage.getReadByIds().add(mockedUser.getId());
        seenMessage.getReadByIds().add(otherChatUser.getId());


        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        List<ChatMessageDto> output = chatService.markUserReadChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(0, output.size());
    }

    /**
     * test markUserReadChat with non-existing user
     */
    @Test
    void invalidUserMarkReadChatTest() throws NoSuchChatException, NoSuchUserException {
        String usernameInput = "user1";
        String chatId = "1";
        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(userRepository.findByUsername(usernameInput)).thenReturn(null);
        when(chatRepository.findById(Long.parseLong(chatId))).thenReturn(Optional.of(new Chat()));

        assertThrows(NoSuchUserException.class, ()->
                chatService.markUserReadChat("1", usernameInput));
    }

    /**
     * test markUserReadChat with non-existing chat
     */
    @Test
    void invalidChatMarkReadChatTest() throws NoSuchChatException, NoSuchUserException {
        String chatIdStr = "1";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(Long.parseLong(chatIdStr))).thenReturn(Optional.empty());

        assertThrows(NoSuchChatException.class, ()->
                chatService.markUserReadChat(chatIdStr, "user1"));
    }


    /**
     * should successfully add user to chat
     */
    @Test
    void successNewUserAddUserToChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        ChatDto output = chatService.linkUserToChat(Long.toString(mockedChat.getId()), usernameInput);
        assertEquals(mockedChat.getId(), output.getId());
    }

    /**
     * trying to add user which is already linked to chat, should not call "save"
     */
    @Test
    void successOldUserAddUserToChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);

        String usernameInput = "user1";
        User mockedUser = new User();
        mockedUser.setId(1);
        mockedChat.getUsersInChat().add(mockedUser);

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(mockedUser);

        ChatDto output = chatService.linkUserToChat(Long.toString(mockedChat.getId()), usernameInput);
        verify(userRepository, times(0)).save(any());
        verify(chatRepository, times(0)).save(any());
    }

    /**
     * trying to add non-existing user to existing chat
     */
    @Test
    void invalidUserAddUserToChatTest() throws NoSuchChatException, NoSuchUserException {
        Chat mockedChat = new Chat();
        mockedChat.setId(1);
        String usernameInput = "user1";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(mockedChat.getId())).thenReturn(Optional.of(mockedChat));
        when(userRepository.findByUsername(usernameInput)).thenReturn(null);

        assertThrows(Exception.class, () ->
                chatService.linkUserToChat(Long.toString(mockedChat.getId()), usernameInput));
    }

    /**
     * Trying to add existing user to a non-existing chat
     */
    @Test
    void invalidChatAddUserToChatTest() throws NoSuchChatException, NoSuchUserException {
        String usernameInput = "user1";

        ChatService chatService = new ChatService(chatRepository, chatMessageRepository, userRepository);
        when(chatRepository.findById(any())).thenReturn(Optional.empty());
        when(userRepository.findByUsername(usernameInput)).thenReturn(new User());

        assertThrows(NoSuchChatException.class, () ->
                chatService.linkUserToChat("1", usernameInput));
    }
}
