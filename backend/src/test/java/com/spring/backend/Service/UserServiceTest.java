package com.spring.backend.Service;

import com.spring.backend.dto.model.UserDto;
import com.spring.backend.exception.DuplicateUsernameException;
import com.spring.backend.exception.WrongCredentialsException;
import com.spring.backend.model.User;
import com.spring.backend.repository.UserRepository;
import com.spring.backend.service.user.UserService;
import com.spring.backend.utils.PasswordUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


//@SpringBootTest(classes = {UserService.class})
@SpringJUnitConfig
public class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @Test
    void successRegisterTest() throws DuplicateUsernameException {
        // creating a user dto to register
        UserDto inputUserDto = new UserDto();
        inputUserDto.setUsername("username");
        inputUserDto.setPassword("Aa123456");
        inputUserDto.setCountry("Germany");
        inputUserDto.setLanguage("en");

        // mock userRepository methods
        when(userRepository.findByUsername(any())).thenReturn(null);
        when(userRepository.save(any(User.class))).thenReturn(null);

        // register user
        UserService userService = new UserService(userRepository);
        UserDto registerOutputUserDto = userService.registerUser(inputUserDto);

        assertEquals(inputUserDto.getUsername(), registerOutputUserDto.getUsername());
        assertEquals("", registerOutputUserDto.getPassword()); // make sure we don't send password field

    }

    @Test
    void duplicateUserRegisterTest() {
        // creating a user dto to register
        UserDto userDto = new UserDto();
        userDto.setUsername("username");
        userDto.setPassword("Aa123456");
        userDto.setCountry("Germany");
        userDto.setLanguage("en");


        // mock userRepository methods
        when(userRepository.findByUsername(anyString())).thenReturn(new User());
        when(userRepository.save(any(User.class))).thenReturn(null);

        // check exception is thrown
        UserService userService = new UserService(userRepository);
        assertThrows(DuplicateUsernameException.class, () ->
                userService.registerUser(userDto));
    }

    @Test
    void successLoginTest() throws WrongCredentialsException {
        // credentials we will test
        String username = "user1";
        String password = "Aa123456";

        // creating a corresponding user object to simulate user in db
        String hashedPassword = PasswordUtils.generateSecurePassword(password, PasswordUtils.getSalt());
        User user = new User();
        user.setUsername(username);
        user.setPassword(hashedPassword);
        user.setLanguage("en");
        user.setCountry("Canada");

        // mock userRepository methods
        when(userRepository.findByUsername(username)).thenReturn(user);

        // login
        UserService userService = new UserService(userRepository);
        UserDto outputUserDto = userService.loginRegularUser(username, password);

        assertEquals(username, outputUserDto.getUsername());
        assertEquals("", outputUserDto.getPassword()); // make sure we don't send password field
    }

    @Test
    void wrongUsernameLoginTest() {
        // credentials we will test
        String username = "user1";
        String password = "Aa123456";

        // mock userRepository methods
        when(userRepository.findByUsername(username)).thenReturn(null);

        // check exception is thrown
        UserService userService = new UserService(userRepository);
        assertThrows(WrongCredentialsException.class, () ->
                userService.loginRegularUser(username, password));
    }

    @Test
    void wrongPasswordLoginTest() {
        // credentials we will test
        String username = "user1";
        String password = "Aa123456";

        // creating a corresponding user object to simulate user in db with different password
        String hashedPassword = PasswordUtils.generateSecurePassword("anotherPassword", PasswordUtils.getSalt());
        User user = new User();
        user.setUsername(username);
        user.setPassword(hashedPassword);
        user.setLanguage("en");
        user.setCountry("Canada");

        // mock userRepository methods
        when(userRepository.findByUsername(username)).thenReturn(user);

        // check exception is thrown
        UserService userService = new UserService(userRepository);
        assertThrows(WrongCredentialsException.class, () ->
                userService.loginRegularUser(username, password));

    }
}
