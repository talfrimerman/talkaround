FROM node:14.1-alpine AS builder

WORKDIR /opt/web
COPY frontend/package.json frontend/package-lock.json ./
RUN npm ci --only=production

ENV PATH="./node_modules/.bin:$PATH"
ENV REACT_APP_API_BASE_URL="https://talkaround-env-production.herokuapp.com/api/v1"
ENV REACT_APP_API_WS_BASE_URL="https://talkaround-env-production.herokuapp.com/ws"

COPY frontend/. ./
RUN npm run build

FROM nginx:1.17-alpine
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin
COPY frontend/nginx.config /etc/nginx/nginx.template
CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
COPY --from=builder /opt/web/build /usr/share/nginx/html