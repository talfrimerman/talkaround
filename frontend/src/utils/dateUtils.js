/**
 * @returns current date-time in the format: mm/dd hh:mm
 */
export const getFormattedNow = () => {
    const currentDate = new Date();
    const month = (currentDate.getMonth() < 10 ? 0 : "") + (currentDate.getMonth() + 1);
    const dayInMonth = currentDate.getDate();
    const time = (currentDate.getHours() < 10 ? 0 : "") + currentDate.getHours() + ":" 
                    + (currentDate.getMinutes() < 10 ? 0 : "") + currentDate.getMinutes();
    const fullDate = month + "/" + dayInMonth + " " + time;
    return fullDate;
}

