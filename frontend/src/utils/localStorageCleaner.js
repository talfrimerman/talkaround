export const cleanLocalStorage = () => {
    localStorage.removeItem('username')
    localStorage.removeItem('language')
    localStorage.removeItem('jwt')
    localStorage.removeItem('admin')
}

export default cleanLocalStorage