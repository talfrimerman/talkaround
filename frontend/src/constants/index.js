export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:8080/api/v1';
export const API_WS_CONNECTION_URL = process.env.REACT_APP_API_WS_BASE_URL || 'http://localhost:8080/ws'
export const CONNECTION_FAILED_MESSAGE = "Unable to connect to server. Please retry.";