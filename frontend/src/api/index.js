import { API_BASE_URL,  } from '../constants';
const axios = require('axios');

/**
 * set to true for fake (mocked) reponses from server
 */
const mockResponse = false; 

// ### request urls ###
//users requests
const LOGIN_URL = API_BASE_URL + "/login";
const REGISTER_URL = API_BASE_URL + "/register";
const DISCOVER_USERS_URL = API_BASE_URL + "/recommend/users"
const DISCOVER_CHATS_URL = API_BASE_URL + "/recommend/chats"
const GET_CHATS_NAMES_MAP_URL = API_BASE_URL + "/chat/chatsMap"
const CREATE_CHAT_URL = API_BASE_URL + "/chat/create"
const GET_CHAT_URL = API_BASE_URL + "/chat"
const MARK_READ_CHAT_URL = API_BASE_URL + "/chat/userRead"
const LINK_USER_TO_CHAT_URL = API_BASE_URL + "/chat/linkUserChat"
// admins requests
const ADMIN_CHECK_URL = API_BASE_URL + "/admin/test"
const BASE_ADMIN_DASHBOARD_URL = API_BASE_URL + "/admin/baseDashboard"
const ADMIN_CHAT_ANALYTICS_URL = API_BASE_URL + "/admin/chatAnalytics"
export const responseStatuses = { "DUPLICATE_ENTITY": "DUPLICATE_ENTITY", "WRONG_CREDENTIALS": "WRONG_CREDENTIALS", "UNAUTHORIZED": "UNAUTHORIZED", "BAD_REQUEST": "BAD_REQUEST" } 


// helper function to simulate waiting when mocking responses
const timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}
// login
export const loginRequest = async (username, password) => {
    if (mockResponse) {
        return {status: "OK", payload: {
            username: username,
            language: 'en'
        }}
    }

    const requestObject = {username: username, password: password}
    let response = await axios.post(LOGIN_URL,requestObject);
    response = response.data
    return response;
}

// register
export const registerRequest = async (username, password, country, language, status, passions) => {
    if (mockResponse) {
        return {status: "OK", payload: {
            username: username,
            language: 'en'
        }}
    }

    const requestObject = { 
        username: username,
        password: password,
        language: language,
        country: country,
        status: status,
        passions: passions
    };
    let response = await axios.post(REGISTER_URL, requestObject);
    return response.data;
}

// discover users
export const discoverUsersRequest = async (username) => {
    if (mockResponse) {
        await timeout(700)
        return {status: "OK", payload:
            [
                {username: "JustAUser", status: "I like dogs and guns!", country: "Germany", passions: ["netflix", "climbing", "cooking"]},
                {username: "NotMe", status: "What is this website? can someone explain?", country: "Canada", passions: ["netflix", "climbing", "coffee"]},
                {username: "John", status: "Talk to me about Noa Kirel!!", country: "China", passions: ["football", "netflix", "music"]},
                {username: "MarkZuckerberg", status: "Facebook is better.", country: "China", passions: ["football", "netflix", "music"]},

            ]
        }
    }

    let response = await axios.get(DISCOVER_USERS_URL + "/" + username);
    return response.data;
}

// discover chats
export const discoverChatsRequest = async (username) => {
    if (mockResponse) {
        await timeout(2000)
        return {status: "OK", payload:
            [
                {chatName: "chat1", usernames: ["user1", "user2", "user3", "user4"], topPassions: ["netflix", "climbing", "cooking"]},
                {chatName: "chat2!!!", usernames: ["user1", "user2", "user3", "user4"], topPassions: ["netflix", "climbing", "cooking"]},
                {chatName: "chat3", usernames: ["user1", "user2", "user3", "user4"], topPassions: ["netflix", "climbing", "cooking"]},
                {chatName: "chat4!!!", usernames: ["user1", "user2", "user3", "user4"], topPassions: ["netflix", "climbing", "cooking"]},

            ]
        }
    }
    let response = await axios.get(DISCOVER_CHATS_URL + "/" + username);
    return response.data;
}

// create new chat with 2 users
export const createChat = async (chatName, initiatorUsername, targetUsername) => {
    if (mockResponse) {
        await timeout(800)
        return {status: "OK", payload: {chatName: "free britney!!"}}
    }
    const requestObject = {
        initiatorUsername: initiatorUsername,
        targetUsername: targetUsername,
        chatName: chatName,
    }
    let response = await axios.post(CREATE_CHAT_URL, requestObject);
    return response.data;
}

// get chat messages 
export const getChat = async (chatId, username) => {
    if (mockResponse) {
        await timeout(800)
        const fakeMessages = [
            {content: "Free her 💯	💯	", username: "user1", timestamp:"MM/DD 00:00"},
            {content: "Yea man we should totally do it", username: "user2", timestamp:"MM/DD 00:00"},
            {content: "who is britney lol 😨	", username: localStorage.getItem('username'), timestamp:"MM/DD 00:00"},
            {content: "where is the bathroom mannnn", username: "user3", timestamp:"MM/DD 00:00"},
            {content: "Let's go back to whatsapp⏪", username: "user1", timestamp:"MM/DD 00:00"},
        ]
        return {status: "OK", payload: {messages: fakeMessages}};
    }
    else {
        let response = await axios.get(GET_CHAT_URL + "/" + chatId + "/" + username);
        return response.data;
    }
}

// get all user's chatnames map (pairs of name:id)
export const getChatNamesMap = async (username) => {
    if (mockResponse) {
        await timeout(800)
        const mockedChatNames = {"free britney!!" : 1,
                                "rak bibi ♥ 💛 ♥" : 2,
                                "Just a group" : 3,
                                "What's in the חדר אוכל today" : 4}
        return {status:"OK", payload: mockedChatNames}
    }

    let response = await axios.get(GET_CHATS_NAMES_MAP_URL + "/" + username);
    return response.data;
}


// mark chat as read by user
export const markChatAsRead = async (username, chatId) => {
    const requestObject = {
        username: username,
        chatId: chatId,
    }
    let response = await axios.post(MARK_READ_CHAT_URL, requestObject);
    return response.data;
}

// link user to chat
export const linkUserToChatRequest = async (username, chatId) => {
    const requestObject = {
        username: username,
        chatId: chatId,
    }
    let response = await axios.post(LINK_USER_TO_CHAT_URL, requestObject);
    return response.data;
}

export const checkAdminRequest = async () => {
    let response = await axios.get(ADMIN_CHECK_URL);
    return response.data;
}

export const baseAdminDashboardRequest = async () => {
    let response = await axios.get(BASE_ADMIN_DASHBOARD_URL);
    return response.data;
}

export const adminChatAnalyticsRequest = async () => {
    let response = await axios.get(ADMIN_CHAT_ANALYTICS_URL);
    return response.data;
}