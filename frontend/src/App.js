import './App.css';
import WelcomePage from './pages/welcome-page'
import DiscoverPage from './pages/discover-page';
import RegisterPage from "./pages/register-page"
import LoadingPage from "./pages/loading-page"
import {useEffect} from 'react'
import {
  Route,
  Routes,
} from "react-router-dom";
import axios from 'axios';
import ChatsPage from './pages/chats-page';
import AdminDashboard from './pages/admin/admin-dashboard'
import AdminChatAnalytics from './pages/admin/admin-chat-analytics'
import cleanLocalStorage from './utils/localStorageCleaner'

Notification.requestPermission(function(status) {
  console.log('Notification permission status:', status);
});

// function displayNotification() {
//   if (Notification.permission === 'granted') {
//     navigator.serviceWorker.getRegistration().then(function(reg) {
//       let pathArray = window.location.href.split( '/' );
//       let protocol = pathArray[0];
//       let host = pathArray[2];
//       let baseUrl = protocol + '//' + host;
//       let redirectUrl = baseUrl + "/welcome/redirect"
//       let options = {
//         body: 'You have a new message! redirect to: ' + redirectUrl,
//         icon: 'assets/o_from_logo.png',
//         vibrate: [100, 50, 100],
//         data: {
//           dateOfArrival: Date.now(),
//           primaryKey: 1,
//           redirectUrl: redirectUrl
//         }
//       };
//       reg.showNotification('New Message', options);
//     });
//   }
// }
// displayNotification();

axios.interceptors.request.use(
  config => {
    const jwt = localStorage.getItem('jwt');
    if (jwt) {
      config.headers.Authorization = `Bearer ${jwt}`;
    } 
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (res) => res,
  (err) => {
    if (err?.response?.status === 401 || err?.response?.status === 403) {
      console.log("unauthorized, redirecting...")
      cleanLocalStorage()
      window.location.href = "welcome/redirect"
    }
    return Promise.reject(err);
  }
);

const App = () => {

  // set tab title
  useEffect(() => {
    document.title = "TalkAround"
  }, []);

  return (
    <div className="App">    
      <Routes>
        <Route exact path='/' element={<LoadingPage/>} />
        <Route path='/welcome/redirect' element={<WelcomePage/>} />
        <Route path='/welcome/signedup' element={<WelcomePage/>} />
        <Route exact path='/welcome' element={<WelcomePage/>} />
        <Route exact path='/signup' element={<RegisterPage/>} />
        <Route exact path='/discover' element={<DiscoverPage/>} />
        <Route exact path='/admin/chats' element={<AdminChatAnalytics/>} />
        <Route exact path='/admin' element={<AdminDashboard/>} />
        <Route path='/chats/:chatName' element={<ChatsPage  />} />
        <Route path='/chats' element={<ChatsPage  />} />
        <Route exact path='/test' element={<ChatsPage />} />
      </Routes>
    </div>
  );
}


export default App;
