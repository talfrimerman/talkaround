
import "./loading-page.css"
import { useNavigate } from 'react-router-dom' 
import { useEffect } from "react";

/**
 * just a cool fake loading page- completely unnecessary but good for user engagement.
 * this page routes to "discover" page for logeed-in users and to "welcome" page for not logged in users
 */
const LoadingPage = ({displayOnly}) => {
    const navigate = useNavigate();

    useEffect(() => {
        if (!displayOnly) {
            const timer = setTimeout(() => {
                localStorage.getItem('username') ? 
                    navigate('/discover') :
                    navigate('/welcome')
            }, 1200);
            return () => clearTimeout(timer);
        }
    }, [displayOnly, navigate]);

    return(
        <div className="loading-wrapper">
            <span>t</span>
            <span>a</span>
            <span>l</span>
            <span>K</span>
            <span>A</span>
            <span>r</span>
            <span>o</span>
            <span>u</span>
            <span>n</span>
            <span>d</span>
        </div>
    )
}

export default LoadingPage;