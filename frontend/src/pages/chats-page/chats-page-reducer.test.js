import { 
    HANDLE_NEW_MESSAGE_TEXT_CHANGE, 
    HANDLE_REPLACE_MESSAGES,
    HANDLE_CHAT_NAME_CHANGE,
    HANDLE_CHATS_REPLACE,
} from "./reducerEventTypes";
import chatsPageReducer from "./chats-page-reducer";

const initialState = {
    chats: [],
    chatName: "",
    messages: [],
    newMessageValue: "",
};

describe("testing chats page reducer", () =>{
    test("Test initial values", () => {
        const state = chatsPageReducer(initialState, {type: "null"})
        expect(state.chats).toHaveLength(0)
        expect(state.chatName).toEqual("")
        expect(state.messages).toHaveLength(0)
        expect(state.newMessageValue).toEqual("")
    });

    test("Test new message value change", () => {
        const state = chatsPageReducer(initialState, {type: HANDLE_NEW_MESSAGE_TEXT_CHANGE, payload: "test message1"})
        expect(state.newMessageValue).toEqual("test message1")
    });

    test("Test replace messages event", () => {
        const state = chatsPageReducer(initialState, {type: HANDLE_REPLACE_MESSAGES, payload: ["m1", "m2"]})
        expect(state.messages).toHaveLength(2)
        expect(state.messages).toContain("m1")
        expect(state.messages).toContain("m2")
    });

    test("Test chat name change event", () => {
        const state = chatsPageReducer(initialState, {type: HANDLE_CHAT_NAME_CHANGE, payload: "testChat1"})
        expect(state.chatName).toEqual("testChat1")
    });

    test("Test chats replace event", () => {
        const state = chatsPageReducer(initialState, {type: HANDLE_CHATS_REPLACE, payload: ["c1", "c2"]})
        expect(state.chats).toContain("c1")
        expect(state.chats).toContain("c2")
    });
});