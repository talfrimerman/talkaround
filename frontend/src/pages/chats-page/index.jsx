import ChatsPageComponent from "./chats-page-component";
import { useReducer, useEffect, useCallback } from 'react';
import { handleReplaceMessagesAction, newMessageTextChangeAction, receivedMessageAction, loadingMessagesChangeAction, addChatsToAlertsAction, removeChatFromAlertsAction, chatNameChangeAction, chatsReplaceAction } from './chats-page-actions'; 
import chatsPageReducer from "./chats-page-reducer";
import { getChatNamesMap,
    responseStatuses, 
    getChat,
    markChatAsRead }
    from "../../api"
import { useParams, useNavigate } from "react-router-dom";
import { API_WS_CONNECTION_URL, CONNECTION_FAILED_MESSAGE } from "../../constants";
import SockJsClient from 'react-stomp';
import { useRef } from 'react';
import * as dateUtils from "../../utils/dateUtils" 

const SEND_MESSAGE_URL_PREFIX = '/app/' 
const SEND_MESSAGE_URL_POSTFIX = '/sendMessage'
const SUBSCRIBE_URL_PREFIX = '/channel/'

const ChatsPage = () => {
    const [state, dispatch] = useReducer(chatsPageReducer, chatsPageReducer());
    const initialChatName = useParams().chatName
    const navigate = useNavigate();
    const clientRef = useRef(null)
    const subscribeHeaders = {"username": localStorage.getItem("username")}

    /**
     * this function is responsible for asynchronously switching the displayed chat context in the page
     * by:
     * (1) changing the chatName state to the selected chat 
     * (2) fetching messages for selected chat and update messages list in state
     * (3) removing unread badge from current chat
     */
    const chatContextSwitch = useCallback(async (chatName) => {
        const username = localStorage.getItem("username");
        // change chatName state        
        dispatch(chatNameChangeAction(chatName));
        // empty messages while loading new chat
        dispatch(handleReplaceMessagesAction([]));
        dispatch(loadingMessagesChangeAction(true));
        // load new messages
        const chatId = state.chats[chatName].id;
        try {
            const response =  await getChat(chatId, username)
            if (response.status === "OK" && response.payload) {
                dispatch(handleReplaceMessagesAction(response.payload.messages));
                // mark chat as read in server
                const username = localStorage.getItem("username");
                const chatId = state.chats[chatName].id;
                const markReadResponse = await markChatAsRead(username, chatId)
                if (!markReadResponse.status === "OK") {
                    alert("Problem marking chat as read.")
                }
            }
            else if (response.status === responseStatuses["WRONG_CREDENTIALS"]) {
                alert("Problem finding user in the server. Please retry.")
            }
            else if (response.status === responseStatuses["BAD_REQUEST"]) {
                alert("Could not open chat. Please try another one or try again later.")
            }
        } catch(e) {
            alert(CONNECTION_FAILED_MESSAGE)
        }
        // stop loading
        dispatch(loadingMessagesChangeAction(false));
        // remove unread badge from current chat
        dispatch(removeChatFromAlertsAction(chatName));
    }, [dispatch, state.chats])

    /**
     * every time the page renders, we: 
     * (1) validate user is logged in
     * (2) loading user's chats (pairs of chat-name:chat-id)
     */
    useEffect( () => { 
        /**
     * get from server a dictionary of {chat-name: chat-id} pairs
     * the names (keys) will be displayed in the chats list, and
     * the corresponding chat id will be used to send a message
    */
    const loadChatNames = async () => {
        const username = localStorage.getItem("username");
        try {
            const response = await getChatNamesMap(username)
            if (response.status==="OK" && response.payload) {
                const chatsMap = response.payload
                dispatch(chatsReplaceAction(chatsMap));
                // add notification badge to unread messages 
                const unreadChatsNames = [];
                for (const [chatName, chatData] of Object.entries(chatsMap)) {
                    if (chatData.newMessagesForUser) {
                        unreadChatsNames.push(chatName);
                    }
                    dispatch(addChatsToAlertsAction(unreadChatsNames));
                }
            }
            else if (response.status === responseStatuses["WRONG_CREDENTIALS"]) {
                alert("Problem finding user in the server. Please retry.")
            }
        } catch (e) {
            alert(CONNECTION_FAILED_MESSAGE)
        }

    }

        const username = localStorage.getItem("username");
        if (!username) {
            alert("You are logged out. Please login again.")
            navigate('/welcome')
        }
        // load all chat names for user 
        loadChatNames()
    },[navigate])

    /**
     * after loading chats, 
     * load initial chat if provided
     */
    useEffect(() => {
        if (Object.keys(state.chats).length!==0  && initialChatName && initialChatName!== ""){
            chatContextSwitch(initialChatName)
        }
        
    },[state.chats, chatContextSwitch, initialChatName])

    /**
     * handling click on chat name by switching context to selected chat
     * @param {*} selectedChatName 
     */
    const handleChatClick = async (selectedChatName) => {
        await chatContextSwitch(selectedChatName)
    }

    /**
     * handle change in the message input box-
     * fires the event to chande corresponding state
     * @param {} e 
     */
    const handleNewMessageChange = (e) => {
        dispatch(newMessageTextChangeAction(e.target.value));
    }

    /**
     * send new message to the context chat
     */
    const handleSend = () => {
        // assemble relevant data
        const username = localStorage.getItem("username");
        const formattedDateTime = dateUtils.getFormattedNow()
        const content = state.newMessageValue;
        const chatId = state.chats[state.chatName].id;
        if (!chatId || content==="") {
            return 
        }
        // empty input box
        dispatch(newMessageTextChangeAction(""));
        // create message object
        const messageObject = {
            username: username,
            formattedDateTime: formattedDateTime,
            content: content
        }
        // send the message
        const path = SEND_MESSAGE_URL_PREFIX + chatId + SEND_MESSAGE_URL_POSTFIX;
        try {
            clientRef.current.sendMessage(path, JSON.stringify(messageObject));
        } catch (e) {
            alert(CONNECTION_FAILED_MESSAGE)
        }

    }

    /**
     * if the message is for the currently open chat, fire an event to push the new message
     * else, fire an event to add notification badge to the chat 
     */
    const handleReceivedMessage = async (messageToPush) => {
        // switch case to distinguish messages of currently displayed chat from message of other chats.
        console.log(messageToPush)
        switch(messageToPush.chatName) {
            case state.chatName:
                if (messageToPush.content) {
                    dispatch(receivedMessageAction(messageToPush));
                    // mark chat as read in server
                    const username = localStorage.getItem("username");
                    const chatId = state.chats[state.chatName].id;
                    const markReadResponse = await markChatAsRead(username, chatId)
                    if (!markReadResponse.status === "OK") {
                        alert("Problem marking chat as read.")
                    }
                }
                // if it is an update message to an already displayed message, update the displayed message
                else {
                    const displayedMessages = state.messages
                    const updatedMessages = displayedMessages.map((originalMessage) => {
                        if (originalMessage.id === messageToPush.id) {
                            return { ...originalMessage, read:messageToPush.read}    ;
                        } 
                        return originalMessage;
                    });
                    dispatch(handleReplaceMessagesAction(updatedMessages));
                }
                break;
            default:
            // the message is for another chat:
                if (messageToPush.content) {
                    dispatch(addChatsToAlertsAction([messageToPush.chatName]));
                }
                break;
        }
    }

    /**
     * @returns list of topics to subscribe to via socket, based on user's chat
     */
    const generateTopicList = () => {
        let topicList = []
        Object.keys(state.chats).forEach((currentChatName) => {
            let subscribtionDestination = SUBSCRIBE_URL_PREFIX + state.chats[currentChatName].id
            topicList.push(subscribtionDestination);
        })
        return topicList;
    }


    return (
    <>
        {/* subscribing to current chat socket */}
        <SockJsClient 
            url={API_WS_CONNECTION_URL}
            topics={generateTopicList()}
            subscribeHeaders={subscribeHeaders}
            onMessage={ (msg) => { handleReceivedMessage(msg)} }
            ref={ (client) => { clientRef.current = client } }
            onConnect={()=>{}}
            onDisconnect={() => {}}
        />
        <ChatsPageComponent
            chats={Object.keys(state.chats)} // the keys are the chat names
            alertsChatsNames={state.alertsChatsNames}
            chatName={state.chatName} 
            messages={state.messages}
            newMessageValue={state.newMessageValue}
            loadingMessages={state.loadingMessages}
            handleChatClick={handleChatClick}
            handleNewMessageChange={handleNewMessageChange}
            handleSend={handleSend}
        />
    </>
    )
}

export default ChatsPage;