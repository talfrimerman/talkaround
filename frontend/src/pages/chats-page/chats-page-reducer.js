import { 
    HANDLE_NEW_MESSAGE_TEXT_CHANGE, 
    HANDLE_REPLACE_MESSAGES,
    HANDLE_CHAT_NAME_CHANGE,
    HANDLE_CHATS_REPLACE,
    HANDLE_RECEIVED_MESSAGE,
    HANDLE_LOADING_MESSAGES_CHANGE,
    HANDLE_ADD_CHATS_TO_ALERTS,
    HANDLE_REMOVE_CHAT_FROM_ALERTS
} from "./reducerEventTypes";

const initialState = {
    chats: {},
    alertsChatsNames: [],
    chatName: "",
    messages: [],
    newMessageValue: "",
    loadingMessages: false,
};

const chatsPageReducer = (state=initialState, action) => {
    switch(action?.type) {
        case HANDLE_RECEIVED_MESSAGE:
            return { ...state, "messages": [...state.messages, action.payload] }
        case HANDLE_NEW_MESSAGE_TEXT_CHANGE:
            return { ...state, "newMessageValue": action.payload };
        case HANDLE_REPLACE_MESSAGES:
            return { ...state, "messages": action.payload }
        case HANDLE_CHAT_NAME_CHANGE: 
            return { ...state, "chatName": action.payload }
        case HANDLE_CHATS_REPLACE:
            return { ...state, "chats": action.payload}
        case HANDLE_LOADING_MESSAGES_CHANGE:
            return { ...state, "loadingMessages": action.payload}
        case HANDLE_ADD_CHATS_TO_ALERTS: 
            return { ...state, "alertsChatsNames": [...state.alertsChatsNames, ...action.payload]}
        case HANDLE_REMOVE_CHAT_FROM_ALERTS: 
            return { ...state, "alertsChatsNames": state.alertsChatsNames.filter(name => name !== action.payload)}
        default: 
            return state;
    }
};

export default chatsPageReducer;