import { HANDLE_REMOVE_CHAT_FROM_ALERTS, HANDLE_ADD_CHATS_TO_ALERTS, HANDLE_LOADING_MESSAGES_CHANGE, HANDLE_RECEIVED_MESSAGE, HANDLE_NEW_MESSAGE_TEXT_CHANGE, HANDLE_REPLACE_MESSAGES, HANDLE_CHAT_NAME_CHANGE, HANDLE_CHATS_REPLACE } from "./reducerEventTypes"

export const chatNameChangeAction = (newChatName) => {
    return {
        type: HANDLE_CHAT_NAME_CHANGE,
        payload: newChatName,
    }
}

export const chatsReplaceAction = (chatsMap) => {
    return {
        type: HANDLE_CHATS_REPLACE,
        payload: chatsMap
    }
}

export const removeChatFromAlertsAction = (chatName) => {
    return {
        type: HANDLE_REMOVE_CHAT_FROM_ALERTS,
        payload: chatName
    }
}

export const addChatsToAlertsAction = (chatsNames) => {
    return {
        type: HANDLE_ADD_CHATS_TO_ALERTS,
        payload: chatsNames
    }
}

export const loadingMessagesChangeAction = (newValue) => {
    return {
        type: HANDLE_LOADING_MESSAGES_CHANGE,
        payload: newValue
    }
}

export const receivedMessageAction = (message) => {
    return {
        type: HANDLE_RECEIVED_MESSAGE,
        payload: message
    }
}

export const newMessageTextChangeAction = (newText) => {
    return {
        type: HANDLE_NEW_MESSAGE_TEXT_CHANGE,
        payload: newText
    }
}

export const handleReplaceMessagesAction = (messages) => {
    return {
        type: HANDLE_REPLACE_MESSAGES,
        payload: messages
    }
}