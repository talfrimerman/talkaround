import ChatsPage from "./index"
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';
import * as api from "../../api";
import { act } from 'react-dom/test-utils';

describe('Test chats page wrapper', () => {

    test('Test chats names are loaded on mount', async() => {
        const chatList = {"chat1" : {id:3}, "chat2" : {id:4}}
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "username");
        const fetchChatsMapSpy = jest.spyOn(api, 'getChatNamesMap')
            .mockImplementation(() => 
                {return {status: "OK", payload: chatList}
            })
            let mountedWrapper;
            await act(async () => {
                mountedWrapper = mount(<Router><ChatsPage /></Router>);
            })
            mountedWrapper.update();
            expect(mountedWrapper.find('ChatsPageComponent').props().chats).toEqual(Object.keys(chatList));
    })

    test('Test selecting chat changes chatName in state', async() => {
        const chatList = {"chat1" : {id:3}}
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "user1");
        const fetchChatsMapSpy = jest.spyOn(api, 'getChatNamesMap')
            .mockImplementation(() => 
                {return {status: "OK", payload: chatList}
            })
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><ChatsPage /></Router>);
            
        })
        mountedWrapper.update(); 
        await act(async () => {
            mountedWrapper.find('ChatsPageComponent').props().handleChatClick("chat1");
            
        })
        mountedWrapper.update();
        console.log(mountedWrapper.find('ChatsPageComponent').props())
        expect(mountedWrapper.find('ChatsPageComponent').props().chatName).toEqual("chat1");
    })

    test('Test selecting chat changes messages array in state', async() => {
        const chatList = {"chat1" : {id:3}}
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "user1");
        const getChatSpy = jest.spyOn(api, 'getChat')
            .mockImplementation(() => 
                {return {status: "OK", payload: {messages:[1,2]}}
            })
        const fetchChatsMapSpy = jest.spyOn(api, 'getChatNamesMap')
            .mockImplementation(() => 
                {return {status: "OK", payload: chatList}
            })
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><ChatsPage /></Router>);
        })
        mountedWrapper.update(); 
        await act(async () => {
            mountedWrapper.find('ChatsPageComponent').props().handleChatClick("chat1");
            
        })
        mountedWrapper.update();
        console.log(mountedWrapper.find('ChatsPageComponent').props())
        expect(mountedWrapper.find('ChatsPageComponent').props().messages).toHaveLength(2);
    })
})

