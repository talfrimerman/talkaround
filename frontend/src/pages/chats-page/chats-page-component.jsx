import ChatBox from "../../components/chats/chat-box";
import Box from '@mui/material/Box';
import AppBarComponent from '../../components/app-bar';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import ChatList from "../../components/chats/chat-list";




const ChatsPageComponent = ({chats, alertsChatsNames, chatName, handleChatClick, messages, newMessageValue, handleSend, handleNewMessageChange, loadingMessages}) => {
    return (
        <>
        <Box className="chats-page-wrapper" sx={{backgroundColor: "#F6FCFF"}}>
                <Box className="bar-wrapper">
                    <AppBarComponent/>
                </Box>
                <Grid container component="main" alignItems="center" justifyContent="center" style={{ minHeight: '89vh' }}>
                <CssBaseline/>

                <Grid item xs={3} sm={3} lg={2} >
                {/* list of chats to select chat from*/}
                  <ChatList 
                    chats={chats} 
                    alertsChatsNames={alertsChatsNames}
                    handleClick={handleChatClick}
                    selected={chatName}
                  />
                </Grid>

                <Grid item xs={8} sm={6} lg={5}>
                {/* chat box by selected chat */}
                  <ChatBox 
                    chatName={chatName} 
                    messages={messages}
                    newMessageValue={newMessageValue}
                    handleSend={handleSend}
                    handleNewMessageChange={handleNewMessageChange}
                    loadingMessages={loadingMessages}
                  />
                </Grid>

            </Grid>
        </Box>          
        </>
    )    
}

export default ChatsPageComponent;