import DiscoverPageComponent from './discover-page-component'
import { BrowserRouter as Router } from 'react-router-dom';


import { mount, render, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });


describe("testing discover page component", () =>{
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<DiscoverPageComponent/>);

    });
  
    test("render a UserCardList child", () => {
      expect(wrapper.find("UserCardList").exists()).toBeTruthy();
    });

});