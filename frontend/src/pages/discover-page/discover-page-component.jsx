import Grid from '@mui/material/Grid';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import AppBarComponent from '../../components/app-bar';
import CreateChatModal from '../../components/chats/create-chat-modal';
import ChatPreviewList from '../../components/chats/chat-preview-list';
import UserCardList from '../../components/users/user-card-list'

const DiscoverPageComponent = ({loadedUsers, users, handleUserCardClick, handleModalClose, modalOpen, targetUsername, chatPreviews, handleChatPreviewClick}) =>  {

    return(
        <Box className="discover-page-wrapper" sx={{backgroundColor: "#F6FCFF"}}>
            <Box className="bar-wrapper">
                <AppBarComponent/>
            </Box>
            <Grid container component="main" sx={{ maxHeight: '100vh' }}>
                <CssBaseline/>

                <Grid item xs={12} sm={5} lg={4} style={{maxHeight: '89vh', overflow: 'auto' }}>
                    <Box sx={{marginTop: '1%', textAlign: 'center'}}>
                        <Typography variant="h6" color="primary"  >Choose a user to start a new chat </Typography>
                        <Typography variant="subtitle2" > 🛈 We always suggest users who share passions with you </Typography>
                    </Box>
                    <CreateChatModal open={modalOpen} handleClose={handleModalClose} targetUsername={targetUsername}/>
                    <UserCardList loaded={loadedUsers} users={users} onCardClick={handleUserCardClick}/>
                </Grid>

                <Grid item xs={false} sm={7} lg={8}  style={{maxHeight: '89vh', overflow: 'auto' }}>
                    <Box sx={{marginTop: '1%', textAlign: 'center'}}>
                        <Typography variant="h6" color="primary"  > Join an existing chat room </Typography>
                        <Typography variant="subtitle2"> 🛈 The chat rooms are recommended for you based on your passions </Typography>
                    </Box>
                    <ChatPreviewList chatPreviews={chatPreviews} onChatPreviewClick={handleChatPreviewClick}/>
                </Grid>

            </Grid>
        </Box>    
    );
}

export default DiscoverPageComponent;