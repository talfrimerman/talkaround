import discoverPageReducer from "./discover-page-reducer"
import {HANDLE_LOADED_USERS, HANDLE_USERS_CHANGE, HANDLE_MODAL_OPEN_CHANGE, HANDLE_TARGET_USERNAME_CHANGE} from "./reducerEventTypes"

describe("testing discover page reducer", () =>{

    const initialState = {
        loadedUsers: false,
        users: [],
        modalOpen: false,
        targetUsername: ""
    };

    test("Test initial values", () => {
        expect(discoverPageReducer(initialState, {type: "null"}).targetUsername).toEqual("")
        expect(discoverPageReducer(initialState, {type: "null"}).modalOpen).toEqual(false)
        expect(discoverPageReducer(initialState, {type: "null"}).users).toHaveLength(0)
        expect(discoverPageReducer(initialState, {type: "null"}).loadedUsers).toEqual(false)
    });

    test("Test loaded users event", () => {
        expect(discoverPageReducer(initialState, {'type': HANDLE_LOADED_USERS}).loadedUsers).toEqual(true)
    });

    test("Test loaded users event when loaded users already true (should stay true)", () => {
        const modifiedState = { ...initialState, loadedUsers:true}
        expect(discoverPageReducer(initialState, {'type': HANDLE_LOADED_USERS}).loadedUsers).toEqual(true)
    });

    test("Test modal open change event", () => {
        expect(discoverPageReducer(initialState, {'type': HANDLE_MODAL_OPEN_CHANGE, payload: true}).modalOpen).toEqual(true)
    });

    test("Test users change change event", () => {
        const testedState = discoverPageReducer(initialState, {'type': HANDLE_USERS_CHANGE, payload: [1,2,3]}).users
        expect(testedState).toHaveLength(3)
        expect(testedState).toContain(1)
        expect(testedState).toContain(2)
        expect(testedState).toContain(3)
    });

    test("Test targetUsername change event", () => {
        const testedState = discoverPageReducer(initialState, {'type': HANDLE_TARGET_USERNAME_CHANGE, payload: "targetUser1"}).targetUsername
        expect(testedState).toEqual("targetUser1")
    });


    
});
