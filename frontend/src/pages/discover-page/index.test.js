import DiscoverPage from "./index";
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';
import * as api from "../../api";
import { act } from 'react-dom/test-utils';

describe('Test discover page wrapper', () => {

    test('Test users are loaded on component mount', async () => {
        const usersArray = [
            {username: "JustAUser", status: "I like dogs and guns!", country: "Germany", passions: ["netflix", "climbing", "cooking"]},
            {username: "NotMe", status: "What is this website? can someone explain?", country: "Canada", passions: ["netflix", "climbing", "coffee"]},
        ]
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "username");
        const fetchUsersSpy = jest.spyOn(api, 'discoverUsersRequest')
                    .mockImplementation(() => 
                        {return {status: "OK", payload: usersArray}
                    })

        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><DiscoverPage /></Router>);
        })
        mountedWrapper.update();
        expect(mountedWrapper.find('DiscoverPageComponent').props().users).toEqual(usersArray);
    });

    test('Test alert is called when user not logged in', async () => {
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => undefined);
        jest.spyOn(window, 'alert').mockImplementation(() => {});
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><DiscoverPage /></Router>);
        })
        mountedWrapper.update();
        expect(window.alert).toHaveBeenCalled();
    });

    test('Test card click changes target username', async () => {
        const clickedUsername = "user1";
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "username");
        const fetchUsersSpy = jest.spyOn(api, 'discoverUsersRequest')
                    .mockImplementation(() => 
                        {return {status: "OK", payload: usersArray}
                    })
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><DiscoverPage /></Router>);
        })
        mountedWrapper.update();
        await act(async () => {
            mountedWrapper.find('DiscoverPageComponent').props().handleUserCardClick(clickedUsername);
        })
        mountedWrapper.update();
        expect(mountedWrapper.find('DiscoverPageComponent').props().targetUsername).toEqual(clickedUsername);
    });

    test('Test card click changes modal open state', async () => {
        const clickedUsername = "user1";
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "username");
        const fetchUsersSpy = jest.spyOn(api, 'discoverUsersRequest')
                    .mockImplementation(() => 
                        {return {status: "OK", payload: usersArray}
                    })
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><DiscoverPage /></Router>);
        })
        mountedWrapper.update();
        await act(async () => {
            mountedWrapper.find('DiscoverPageComponent').props().handleUserCardClick(clickedUsername);
        })
        mountedWrapper.update();
        expect(mountedWrapper.find('DiscoverPageComponent').props().modalOpen).toEqual(true);
    });

    test('Test chats are loaded on component mount', async () => {
        const chatsArray = [
            {chatName: "chat1", usernames: ["u1", "u2"], topPassions: ["netflix", "climbing", "cooking"]},
        ]
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "username");
        const fetchChatsSpy = jest.spyOn(api, 'discoverChatsRequest')
                    .mockImplementation(() => 
                        {return {status: "OK", payload: chatsArray}
                    })

        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><DiscoverPage /></Router>);
        })
        mountedWrapper.update();
        expect(mountedWrapper.find('DiscoverPageComponent').props().chatPreviews).toEqual(chatsArray);
    });

    test('Test chat click calls api', async () => {
        const clickedChatName = "chat1";
        jest.spyOn(window.localStorage.__proto__, 'getItem').mockImplementation(() => "username");
        const fetchUsersSpy = jest.spyOn(api, 'discoverUsersRequest')
                    .mockImplementation(() => 
                        {return {status: "OK", payload: usersArray}
                    })
        const fetchChatsSpy = jest.spyOn(api, 'linkUserToChatRequest').mockImplementation(() => 
        { return {status: "OK"} })
        
  
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><DiscoverPage /></Router>);
        })
        mountedWrapper.update();
        mountedWrapper.find('DiscoverPageComponent').props().handleChatPreviewClick(clickedChatName);
        mountedWrapper.update();
        expect(fetchChatsSpy).toBeCalledTimes(1);
    });
})

