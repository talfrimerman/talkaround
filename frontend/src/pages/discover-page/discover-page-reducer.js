import { HANDLE_CHATS_CHANGE, HANDLE_LOADED_USERS, HANDLE_USERS_CHANGE, HANDLE_MODAL_OPEN_CHANGE, HANDLE_TARGET_USERNAME_CHANGE } from "./reducerEventTypes";


const initialState = {
    loadedUsers: false,
    users: [],
    modalOpen: false,
    targetUsername: "",
    chatPreviews: []
};

const discoverPageReducer = (state=initialState, action) => {
    switch(action?.type) {
        case HANDLE_LOADED_USERS:
            return { ...state, "loadedUsers": true };
        case HANDLE_USERS_CHANGE:
            return { ...state, "users": action.payload}
        case HANDLE_MODAL_OPEN_CHANGE:
            return { ...state, "modalOpen": action.payload}
        case HANDLE_TARGET_USERNAME_CHANGE:
            return { ...state, "targetUsername": action.payload}
        case HANDLE_CHATS_CHANGE:
            return { ...state, "chatPreviews": action.payload}
        default: 
            return state;
    }
}

export default discoverPageReducer;