import DiscoverPageComponent from "./discover-page-component"
import { useReducer, useEffect  } from 'react';
import {linkUserToChatRequest, discoverUsersRequest, discoverChatsRequest} from "../../api"
import { useNavigate } from 'react-router-dom'
import { CONNECTION_FAILED_MESSAGE } from "../../constants"; 
import discoverPageReducer from "./discover-page-reducer";
import { HANDLE_CHATS_CHANGE, HANDLE_LOADED_USERS, HANDLE_USERS_CHANGE, HANDLE_MODAL_OPEN_CHANGE, HANDLE_TARGET_USERNAME_CHANGE } from "./reducerEventTypes";


export const DiscoverPage = () => {

    const [state, dispatch] = useReducer(discoverPageReducer, discoverPageReducer());
    const navigate = useNavigate();

    useEffect( () => {
        // TODO another function to fetch groups, execute both at the same time async
        const username = localStorage.getItem('username');
        if (username === null) {
            alert("You are logged out. Please login.")
            return navigate("/")
        }
        async function fetchUsers() {
            try {
                const response = await discoverUsersRequest(username);
                if (response.status === "OK") {
                    dispatch({
                        type: HANDLE_USERS_CHANGE,
                        payload: response.payload
                    });
                    dispatch({
                        type: HANDLE_LOADED_USERS,
                    });
                } else {
                    alert ("Problem with getting users. please try to login again.")
                    return navigate("/welcome")
                }
            }
            catch (e) {
                alert(CONNECTION_FAILED_MESSAGE)
            }
        }
        async function fetchChats() {
            try {
                const response = await discoverChatsRequest(username);
                if (response.status === "OK") {
                    dispatch({
                        type: HANDLE_CHATS_CHANGE,
                        payload: response.payload
                    });
                } else {
                    alert ("Problem with getting chats. please try to login again.")
                    return navigate("/welcome")
                }
            }
            catch (e) {
                alert(CONNECTION_FAILED_MESSAGE)
            }
        }
        fetchUsers()
        fetchChats()
    },[navigate])

    const handleUserCardClick = (username) =>  {
        // open modal with username
        dispatch({
            type: HANDLE_TARGET_USERNAME_CHANGE,
            payload: username
        });
        dispatch({
            type: HANDLE_MODAL_OPEN_CHANGE,
            payload: true
        });
    }

    const handleModalClose = () => {
        // close modal
        dispatch({
            type: HANDLE_MODAL_OPEN_CHANGE,
            payload: false
        });
    }

    const handleChatPreviewClick = async (chatId) => {
        // link user to chat and redirect 
        const username = localStorage.getItem('username');
        const response = await linkUserToChatRequest(username, chatId);
        if (response.status==="OK") {
            return navigate("/chats/" + response.payload.chatName)
        } else {
            alert("Problem adding to chat. Please try again later.")
        }
    }

    return(
        <DiscoverPageComponent 
            loadedUsers={state.loadedUsers} 
            users={state.users} 
            handleUserCardClick={handleUserCardClick} 
            handleModalClose={handleModalClose}
            modalOpen={state.modalOpen} 
            targetUsername={state.targetUsername}
            chatPreviews={state.chatPreviews}
            handleChatPreviewClick={handleChatPreviewClick}
        />
    );
}

export default DiscoverPage