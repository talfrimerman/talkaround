import AdminChatAnalyticsComponent from "./admin-chat-analytics-component";
import adminChatAnalyticsReducer from "./admin-chat-analytics-reducer";
import { useReducer, useEffect } from "react";
import { adminChatAnalyticsRequest } from "../../../api"
import { MOST_USERS_CHATS_CHANGE, MOST_MESSAGES_CHATS_CHANGE } from "./reducerEventTypes";
import { CONNECTION_FAILED_MESSAGE } from "../../../constants";


const AdminChatAnalytics = () => {
    const [state, dispatch] = useReducer(adminChatAnalyticsReducer, adminChatAnalyticsReducer());

    useEffect( () => {
        const fetchChatAnalytics = async () => {
            const response = await adminChatAnalyticsRequest()
            if (response.status==="OK") {
                const responseData = response.payload;
                const { mostUsersChats, mostMessagesChats } = responseData;
                dispatch({
                    type: MOST_USERS_CHATS_CHANGE,
                    payload: mostUsersChats
                });
                dispatch({
                    type: MOST_MESSAGES_CHATS_CHANGE,
                    payload: mostMessagesChats
                });
            }
        }
        try {
            fetchChatAnalytics();
        }
        catch (e) {
            alert(CONNECTION_FAILED_MESSAGE)
        }
        
    },[])

    //TODO get data from here
    return(
        <AdminChatAnalyticsComponent
            mostUsersChats = {state.mostUsersChats}
            mostMessagesChats = {state.mostMessagesChats}
        />
    )
}

export default AdminChatAnalytics;