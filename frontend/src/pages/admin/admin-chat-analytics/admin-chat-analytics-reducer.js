import {MOST_USERS_CHATS_CHANGE, MOST_MESSAGES_CHATS_CHANGE} from './reducerEventTypes'

const initialValues = {
    mostUsersChats : [],
    mostMessagesChats: [],
}

const adminChatAnalyticsReducer = (state=initialValues, action) => {
    switch(action?.type) {
        case MOST_USERS_CHATS_CHANGE:
            return { ...state, mostUsersChats: action.payload };
        case MOST_MESSAGES_CHATS_CHANGE:
            return { ...state, mostMessagesChats: action.payload };
        default: 
            return state;
    }
}

export default adminChatAnalyticsReducer;