import AdminChatAnalytics from './index'
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';
import * as api from "../../../api";
import { act } from 'react-dom/test-utils';

describe('Test admin chat analytics page wrapper', () => {

    test('Test chat analytics data is loaded on mount', async() => {
        const mockedData = {mostUsersChats:[{id: 3}], mostMessagesChats:[{id:1}]}
        const fetchsSpy = jest.spyOn(api, 'adminChatAnalyticsRequest')
            .mockImplementation(() => 
                {return {status: "OK", payload: mockedData}
            })
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><AdminChatAnalytics/></Router>);
        })
        mountedWrapper.update();
        expect(mountedWrapper.find('AdminChatAnalyticsComponent').props().mostUsersChats).toEqual(mockedData.mostUsersChats);
        expect(mountedWrapper.find('AdminChatAnalyticsComponent').props().mostMessagesChats).toEqual(mockedData.mostMessagesChats);
    })

})