import Grid from '@mui/material/Grid';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import AdminAppBar from '../../../components/admin-app-bar';
import ChatSummeriesList from '../../../components/chats-details/chat-summaries-list'

const AdminChatAnalyticsComponent = ({mostUsersChats, mostMessagesChats}) => {
    return (
        <Box  sx={{backgroundColor: "#F6FCFF"}}>
            <Box>
                <AdminAppBar/>
            </Box>
            <Box sx={{
                display:'flex',
                paddingTop: 2,
                paddingBottom: 1,
                paddingLeft: 5
            }}>
                <Typography variant="h5" color="text.primary"> <strong>chats analytics </strong></Typography>
            </Box>
            <Grid container component="main" sx={{ maxHeight: '100vh' }}>
                <CssBaseline/>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                    <Box sx={{marginTop: '1%', textAlign: 'center'}}>
                        <Typography variant="body1" color="text.secondary"> Chats with highest amount of <strong>messages</strong></Typography>
                        <ChatSummeriesList chats={mostMessagesChats}/>
                    </Box>
                </Grid>

                <Grid item xs={false} sm={false} md={6} lg={6}>
                    <Box sx={{marginTop: '1%', textAlign: 'center'}}>
                        <Typography variant="body1" color="text.secondary">  Chats with highest amount of <strong>users</strong> </Typography>
                        <ChatSummeriesList chats={mostUsersChats}/>
                    </Box>
                </Grid>

            </Grid>
        </Box>
        
    )
}

export default AdminChatAnalyticsComponent;