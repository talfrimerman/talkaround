import { MOST_USERS_CHATS_CHANGE, MOST_MESSAGES_CHATS_CHANGE } from "./reducerEventTypes";
import adminChatAnalyticsReducer from "./admin-chat-analytics-reducer";


describe("testing chat analytics reducer", () =>{
    test("Test initial values", () => {
        const state = adminChatAnalyticsReducer()
        expect(state.mostUsersChats).toHaveLength(0)
        expect(state.mostMessagesChats).toHaveLength(0)
       
    });

    test("Test mostUsersChats change", () => {
        const newData = [{id: 1}]
        const state = adminChatAnalyticsReducer(adminChatAnalyticsReducer(), {type: MOST_USERS_CHATS_CHANGE, payload: newData})
        expect(state.mostUsersChats).toEqual(newData)
    });

    test("Test mostMessagesChats change", () => {
        const newData = [{id: 1}]
        const state = adminChatAnalyticsReducer(adminChatAnalyticsReducer(), {type: MOST_MESSAGES_CHATS_CHANGE, payload: newData})
        expect(state.mostMessagesChats).toEqual(newData)
    });

});