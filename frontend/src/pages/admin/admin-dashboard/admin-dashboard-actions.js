import { HANDLE_DATA_CHANGE } from "./reducerEventTypes";

export const passionsCountChangeAction = (newValue) => {
    return {
        type: HANDLE_DATA_CHANGE,
        field: "passionsCounts",
        payload: newValue,
    }
}

export const messagesCountChangeAction = (newValue) => {
    return {
        type: HANDLE_DATA_CHANGE,
        field: "messagesCount",
        payload: newValue,
    }
}

export const usersCountChangeAction = (newValue) => {
    return {
        type: HANDLE_DATA_CHANGE,
        field: "usersCount",
        payload: newValue,
    }
}

export const chatsCountChangeAction = (newValue) => {
    return {
        type: HANDLE_DATA_CHANGE,
        field: "chatsCount",
        payload: newValue,
    }
}

export const newestUsersChangeAction = (newValue) => {
    return {
        type: HANDLE_DATA_CHANGE,
        field: "newestUsers",
        payload: newValue,
    }
}