import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import CssBaseline from '@mui/material/CssBaseline';
import BarChart from '../../../components/charts/bar-chart'
import AdminAppBar from '../../../components/admin-app-bar';
import NumericInfoCard from '../../../components/numeric-info-card';
import Typography from '@mui/material/Typography';
import UserCardList from '../../../components/users/user-card-list'

const AdminDashboardComponent = ({passionsCounts, messagesCount, usersCount, newestUsers, chatsCount, hideChart}) => {

    return (
        <Box sx={{backgroundColor: "#F6FCFF"}}>
            <Box>
                <AdminAppBar/>
            </Box>
            <CssBaseline/>
            <Grid container component="main">
                <Grid item xs={12} sm={5} md={4} sx={{maxHeight: '89vh', overflow: 'auto' }}>
                    <Box sx={{marginTop: '3%', textAlign: 'center'}}>
                        <Typography variant="h5" color="text.secondary" > New users </Typography>
                        <UserCardList loaded={newestUsers.length>0} users={newestUsers} />
                    </Box>
                    <Box>
                    </Box>
                </Grid>
                <Grid item xs={false} sm={7} md={8} sx={{maxHeight: '89vh', overflow: 'auto'}}>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        marginRight: 3,
                        marginLeft: 3
                    }}>
                        <Box>
                            <NumericInfoCard title="Total users" number={usersCount} icon="🕺"/>
                            <NumericInfoCard title="Total messages" number={messagesCount} icon="🗨"/>
                            <NumericInfoCard title="Total chats" number={chatsCount} icon="📃"/>

                        </Box>
                        <Box>
                            <Typography variant="h6" color="text.primary" > Top passions </Typography>
                            {!hideChart && <BarChart data={passionsCounts}/>}
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </Box>
        
    )
}

export default AdminDashboardComponent