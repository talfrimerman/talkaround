import {HANDLE_DATA_CHANGE} from './reducerEventTypes'
import adminDashboardReducer from './admin-dashboard-reducer'

describe("testing admin dashboard reducer", () =>{
    test("Test initial values", () => {
        const state = adminDashboardReducer()
        expect(state.passionsCounts).toHaveLength(0)
        expect(state.newestUsers).toHaveLength(0)
        expect(state.chatsCount).toEqual(0)
        expect(state.usersCount).toEqual(0)
        expect(state.messagesCount).toEqual(0)
    });

    test("Test passions counts change", () => {
        const newData = [{passion: "Coffee", count: 3}]
        const state = adminDashboardReducer(adminDashboardReducer(), {type: HANDLE_DATA_CHANGE, field: "passionsCounts", payload: newData})
        expect(state.passionsCounts).toEqual(newData)
    });

    test("Test users count change", () => {
        const newData = 9
        const state = adminDashboardReducer(adminDashboardReducer(), {type: HANDLE_DATA_CHANGE, field: "usersCount", payload: newData})
        expect(state.usersCount).toEqual(newData)
    });

    test("Test newestUsers change", () => {
        const newData = [{username: "u1"}, {username: "u2"}]
        const state = adminDashboardReducer(adminDashboardReducer(), {type: HANDLE_DATA_CHANGE, field: "newestUsers", payload: newData})
        expect(state.newestUsers).toEqual(newData)
    });

    test("Test messages count change", () => {
        const newData = 3
        const state = adminDashboardReducer(adminDashboardReducer(), {type: HANDLE_DATA_CHANGE, field: "messagesCount", payload: newData})
        expect(state.messagesCount).toEqual(newData)
    });
});