import { baseAdminDashboardRequest } from "../../../api"
import { CONNECTION_FAILED_MESSAGE } from "../../../constants";
import {  useEffect  } from 'react';
import { useReducer } from 'react';
import AdminDashboardComponent from "./admin-dashboard-component";
import adminDashboardReducer from "./admin-dashboard-reducer";
import { passionsCountChangeAction, messagesCountChangeAction, usersCountChangeAction, newestUsersChangeAction, chatsCountChangeAction } from "./admin-dashboard-actions";

const AdminDashboard = ({hideChart}) => {
    const [state, dispatch] = useReducer(adminDashboardReducer, adminDashboardReducer());

    const passionsCountsComparer = (a,b) => {
        return a.count > b.count ? -1 : 1
    } 

    useEffect( () => {
        const fetchBaseDashboard = async () => {
            const response = await baseAdminDashboardRequest()
            if (response.status==="OK") {
                const baseDashboardData = response.payload;
                const {topPassionsCounts, newestUsers, messagesCount, usersCount, chatsCount} = baseDashboardData;
                dispatch(passionsCountChangeAction(topPassionsCounts.sort(passionsCountsComparer).slice(0,8)));
                dispatch(newestUsersChangeAction(newestUsers));
                dispatch(messagesCountChangeAction(messagesCount));
                dispatch(usersCountChangeAction(usersCount));
                dispatch(chatsCountChangeAction(chatsCount));
            }
        }
        try {
            fetchBaseDashboard();
        }
        catch (e) {
            alert(CONNECTION_FAILED_MESSAGE)
        }
        
    },[])

    return (
        <AdminDashboardComponent 
            passionsCounts={state.passionsCounts}
            messagesCount={state.messagesCount}
            usersCount={state.usersCount}
            chatsCount={state.chatsCount}
            newestUsers={state.newestUsers}
            hideChart={hideChart}
        />
    )
}

export default AdminDashboard