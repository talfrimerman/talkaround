import { HANDLE_DATA_CHANGE } from "./reducerEventTypes";

const initialValues = {
    passionsCounts : [],
    messagesCount: 0,
    usersCount: 0,
    chatsCount: 0,
    newestUsers: [],
}

const adminDashboardReducer = (state=initialValues, action) => {
    switch(action?.type) {
        case HANDLE_DATA_CHANGE:
            return { ...state, [action.field]: action.payload };
        default: 
            return state;
    }
};

export default adminDashboardReducer;