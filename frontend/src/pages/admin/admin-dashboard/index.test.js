import AdminDashboard from './index'
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';
import * as api from "../../../api";
import { act } from 'react-dom/test-utils';

describe('Test admin dashboard wrapper', () => {

    test('Test dashboard data is loaded on mount', async() => {
        const mockedData = {topPassionsCounts:[{passion: "Netflix", count: 3}], newestUsers:[{username:"user1"}], messagesCount:3, usersCount:3, chatsCount:3}
        const fetchChatsMapSpy = jest.spyOn(api, 'baseAdminDashboardRequest')
            .mockImplementation(() => 
                {return {status: "OK", payload: mockedData}
            })
        let mountedWrapper;
        await act(async () => {
            mountedWrapper = mount(<Router><AdminDashboard hideChart={true}/></Router>);
        })
        mountedWrapper.update();
        expect(mountedWrapper.find('AdminDashboardComponent').props().passionsCounts).toEqual(mockedData.topPassionsCounts);
    })

})
