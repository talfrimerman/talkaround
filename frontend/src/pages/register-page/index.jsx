import RegisterForm from "../../components/auth/register-form"
import bg from '../../assets/map_gray_bg.jpg'
import full_logo from '../../talkAround_logo_full.png'

import Grid from '@mui/material/Grid';
import CssBaseline from '@mui/material/CssBaseline';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Fade from '@mui/material/Fade';




const RegisterPage = () => {

    return (
    <div className="Welcome-page-wrapper">
        <Grid container component="main" sx={{ height: '100vh' }}>
        <CssBaseline/>
            <Grid item xs={false} sm={4} md={7}
            sx={{
                backgroundImage: 'url('+ bg+')',
                backgroundRepeat: 'no-repeat',
                backgroundColor: (t) =>
                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                backgroundSize: 'cover',
                backgroundPosition: 'center',
            }}
            >
            <div className="fadeTop"></div>
            </Grid>
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} >
                <Box
                    sx={{
                    my: 1,
                    mx: 4,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    }}
                >
                    <Fade appear={true} in={true}><img src={full_logo} className="App-logo" alt="logo" /></Fade>
                    <RegisterForm/>
                </Box>
            </Grid>
        </Grid>
    </div>    
    )
}

export default RegisterPage;