import Grid from '@mui/material/Grid';
import CssBaseline from '@mui/material/CssBaseline';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Fade from '@mui/material/Fade';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import { useLocation } from "react-router-dom"

import LoginForm from '../../components/auth/login-form'


import bg from '../../assets/map_gray_bg.jpg'
import full_logo from '../../talkAround_logo_full.png'



export const WelcomePage = (props) =>  {
    const location = useLocation();

    return(
        <div className="Welcome-page-wrapper">
                <Grid container component="main" sx={{ height: '100vh' }}>
                <CssBaseline/>
                <Grid item xs={false} sm={4} md={7}
                sx={{
                    width : '100vw',
                    backgroundImage: 'url('+ bg+')',
                    backgroundRepeat: 'no-repeat',
                    backgroundColor: (t) =>
                    t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                }}
                >
                    <Box sx={{
                            marginTop: '30%',
                            textAlign: 'center',
                            fontStyle: 'italic',
                            fontSize: 40,
                            textShadow: '2px 2px #d6aaaa'
                        }} >
                            <Typography variant="h2" component="div" color="primary" display="inline" align="left">Meet </Typography>
                            <Typography variant="h3" component="div" display="inline"> inspiring people. <br/> </Typography>
                            <Typography variant="h2" component="div" color="primary" display="inline">Share </Typography>
                            <Typography variant="h3" component="div" display="inline"> your passions.</Typography>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} >
                    <Box
                        sx={{
                            my: 1,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Box sx={{overflow: 'hidden', position: 'relative', margin: '0 auto'}}><Fade appear={true} in={true}><img src={full_logo} className="App-logo" alt="logo" /></Fade></Box>
                        { location.pathname.includes("redirect") &&
                        <Alert severity="warning" sx={{marginBottom:3}}>
                            <AlertTitle>You are logged out</AlertTitle>
                            To continue, <strong>please sign in.</strong>
                        </Alert> }
                        { location.pathname.includes("signedup") &&
                        <Alert severity="success" sx={{marginBottom:3}}>
                            <AlertTitle>Thank you</AlertTitle>
                            To continue, <strong>please sign in.</strong>
                        </Alert> }
                        <LoginForm/>
                    </Box>
                </Grid>
            </Grid>
        </div>    

    );
}

export default WelcomePage;