import ChatPreviewComponent from "./chat-preview-copmonent";

const ChatPreview = ({chatPreview, onClick}) => {
    return(
        <ChatPreviewComponent chatPreview={chatPreview} onClick={onClick}/>
    )
}

export default ChatPreview