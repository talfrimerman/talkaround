import Box from '@mui/material/Box';
import React from "react";
import {
    makeStyles,
} from "@material-ui/core";
import Tooltip from '@mui/material/Tooltip';

import {
  Card,
  CardContent,
  Typography,
} from "@material-ui/core";
import UserPassions from "../../users/user-passions";

const useStyles = makeStyles((theme) => ({
    card: {
      borderRadius: 15,
      minHeight: "20vh",
      maxWidth: "330px",
      minWidth: "330px",
      backgroundColor: theme.palette.background.card,
      margin: theme.spacing(2),
      "&:hover": {
        backgroundColor: "rgba(50, 50, 50, 0.05)"
      }
    },
  }));


const ChatPreviewComponent = ({chatPreview, onClick}) => {
  const classes = useStyles();
  return (
      <Tooltip title={["Click to join", chatPreview.name].join(" ")} placement="top">
      <Card
        variant="outlined"
        className={[classes.card, "pointerCursor"].join(" ")}
        style={{ display: "inline-block" }}
        onClick={()=> onClick(chatPreview.id)}
      >
        <CardContent className="card-content">
            <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
            }}>
                <Box sx={{
                display: 'flex',
                alignItems: 'flex-start',
                }}>
                  <Typography variant="h6">
                      {chatPreview.chatName}
                  </Typography>
                </Box>
                <Box sx={{
                  color: 'text.secondary'
                }}>
                  <Typography
                  variant="subtitle2"
                  align="left"
                  >
                      {chatPreview.usernames[0]}, {chatPreview.usernames[1]}
                      {chatPreview.usernames.length > 2 && 
                      <>
                        {" "} and {chatPreview.usernames.length -2 }
                        {chatPreview.usernames.length > 3 ? 
                          <> {" "} others </> : <> {" "}  other </>}
                      </>}
                  </Typography>
                </Box>
            </Box>
            <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginTop: 3,
                paddingTop: 1,
                color: 'text.secondary',
                borderTop: 1,
                borderColor: 'grey.300',
            }}>
                <Typography variant="subtitle2" sx={{
                }}>
                    🔥 Trending now 
                </Typography>{" "}
                <Box sx={{
                    marginTop: -2,
                    marginBottom: -1.5
                }}>
                    <UserPassions passions={chatPreview.topPassions} />
                </Box>
            </Box>
        </CardContent>
      </Card>
      </Tooltip>
  );
}

export default ChatPreviewComponent