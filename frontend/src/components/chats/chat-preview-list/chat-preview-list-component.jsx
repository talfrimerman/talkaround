import ChatPreview from "../chat-preview";
import ChatPreviewSkeleton from "../chat-preview-skeleton";
import Box from '@mui/material/Box';

const ChatPreviewListComponent = ( ({chatPreviews, onChatPreviewClick}) => {

    return (
        <>
        {chatPreviews.length > 0 ?
            <Box>
                {chatPreviews.map((chatPreview, index) => 
                        <ChatPreview key={index} chatPreview={chatPreview} onClick={onChatPreviewClick}/>
                )}
            </Box>
        :
            <Box sx={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent:'center'
            }}>
                    {[0,1,2,3,4,5].map((chatPreview, index) => 
                            <Box key={index}>
                                <ChatPreviewSkeleton key={index} />
                            </Box>
                    )}
            </Box>
            
        }
        
        </>
    )
})

export default ChatPreviewListComponent