import ChatPreviewListComponent from "./chat-preview-list-component";

const ChatPreviewList = ( ({chatPreviews, onChatPreviewClick}) => {
    return (
        <ChatPreviewListComponent chatPreviews={chatPreviews} onChatPreviewClick={onChatPreviewClick} />
    )
})

export default ChatPreviewList