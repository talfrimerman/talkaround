import Box from '@mui/material/Box';
import Badge from '@mui/material/Badge';


const ChatListComponent = ({chats, alertsChatsNames, handleClick, selected}) => {

    return (
        <Box sx={{
            backgroundColor: "rgba(50, 50, 50, 0.1)",
            minHeight: "90vh",
            maxHeight: "90vh",
            overflow: 'auto',
            marginTop: -1,
            padding:1,
        }}>
            <Box sx={{color:'text.secondary', textAlign:'left', padding:"10px"}}> Chats </Box>
            {chats.map((chat, index) => 
                <Box key={index} className={"pointerCursor"}
                    sx={{
                        display:'flex',
                        padding: "12px",
                        fontSize: 13,
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        "&:hover": {
                            backgroundColor: "rgba(50, 50, 50, 0.08)"
                        },
                        backgroundColor: chat===selected ? "rgba(50, 50, 50, 0.08)" : '',
                        borderRadius: 5
                    }}
                    onClick={() => handleClick(chat)}
                >   
                    <Badge color="secondary" variant="dot" invisible={!alertsChatsNames.includes(chat)}>
                        <Box sx={{textAlign: 'left'}}>{chat}</Box>      
                    </Badge>
                </Box>
            )}
        </Box>
    )
}

export default ChatListComponent;