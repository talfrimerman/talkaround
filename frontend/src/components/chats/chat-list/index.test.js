import ChatList from "./index"
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});

describe('Test chats list wrapper', () => {
    let mockedChats;
    beforeAll(() => {
        mockedChats = ["chat1", "chat2"]
    });

    test('Test chat click event triggers correct function', () => {
        const mockedOnClickHandler = jest.fn()
        let mountedWrapper = mount(<ChatList alertsChatsNames={[]} chats={mockedChats} handleClick={mockedOnClickHandler} />); 
        let innerComponent = mountedWrapper.find('ChatListComponent')
        let clickableChatName = innerComponent.find(".pointerCursor").at(0) // find the first chat in the list
        clickableChatName.simulate('click', {})
        expect(mockedOnClickHandler).toBeCalledTimes(1); 
    });

    

})
