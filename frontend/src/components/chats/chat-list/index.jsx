import ChatListComponent from "./ChatListComponent";


const ChatList = ({chats, alertsChatsNames, handleClick, selected}) => {

    return (
        <ChatListComponent handleClick={handleClick} selected={selected} chats={chats} alertsChatsNames={alertsChatsNames}/>
    )
}

export default ChatList;