import { createStyles, makeStyles } from "@material-ui/core/styles";
import OtherUserMessage from "../messages/other-user-message";
import SelfMessage from "../messages/self-message";
import bg from '../../../assets/map_gray_bg.jpg'
import TextField from '@mui/material/TextField';
import SendIcon from '@mui/icons-material/Send';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingPage from "../../../pages/loading-page"


const useStyles = makeStyles((theme) =>
  createStyles({
    innerBox: {
      width: "80vw",
      height: "89vh",
      maxHeight: "100vh",
      display: "flex",
      alignItems: "center",
      flexDirection: "column",
      position: "relative",
    },
    container: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "rgba(50, 50, 50, 0.05)",
    },
    messagesBody: {
      width: "calc( 100% - 20px )",
      margin: 10,
      overflowY: 'scroll',
      display: 'flex',
      flexDirection: 'column-reverse',
      height: "calc( 100% - 80px )",
      backgroundImage: 'url('+ bg+')',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      borderRadius: '10px',
      padding: 5
    }
  })
);


const ChatBoxComponent = ({messages, chatName, newMessageValue, handleNewMessageChange, handleSend, loadingMessages}) => {
    const classes = useStyles();

    return (
        <Box className={classes.container}>
          <Box className={classes.innerBox}>
              <Box sx={{
                display: 'flex',
                width: "100%",
                height: "8vh",
                backgroundColor: "rgba(50, 50, 50, 0.15)",
                justifyContent: 'flex-start',
                alignItems: 'center'
              }}> 
                <Typography sx={{padding:2, fontWeight: 550 }}>
                  {chatName} 
                </Typography>
              </Box>
              <Box className={classes.messagesBody}>
                <Box> {/** this box needed for inner content not to be revesed */}
                  {messages?.map((message, index) => 
                    message.username===localStorage.getItem("username") ?
                      <SelfMessage key={index}
                        message={message.content}
                        timestamp={message.formattedDateTime}
                        read={message.read}
                      />
                    :
                    <OtherUserMessage key={index}
                      message={message.content}
                      timestamp={message.formattedDateTime}
                      displayName={message.username}
                    />
                  )}
                  {loadingMessages ?
                    <Box>
                      <LoadingPage displayOnly={true}/>
                    </Box>
                  : 
                  <></>}
                </Box>
              </Box>
              <Box fullwidth sx={{
                width:"100%",
                display:"flex",
                alignSelf:"flex-start", 
                paddingLeft: 2,
                paddingRight: 2,
                paddingBottom: 2
              }}>
                <TextField 
                  fullWidth 
                  size="small" 
                  sx={{
                    paddingRight: 1,
                  }}
                  value={newMessageValue}
                  onChange={handleNewMessageChange}
                  disabled={loadingMessages || chatName===""}
                  onKeyPress= {(e) => e.key === "Enter" && handleSend()}
                />
                <Button onClick={handleSend} disabled={!newMessageValue || loadingMessages || chatName===""} variant="contained" endIcon={<SendIcon/>}/>
              </Box>
          </Box>
        </Box>
    )
}

export default ChatBoxComponent;