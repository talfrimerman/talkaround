import ChatBoxComponent from "./chat-box-component";



const ChatBox = ({messages, chatName, newMessageValue, handleNewMessageChange, handleSend, loadingMessages}) => {
    // load messages by chatName, each time chatName changes (messages should be state from here)
    return (
        <ChatBoxComponent 
            messages={messages} 
            chatName={chatName} 
            newMessageValue={newMessageValue} 
            handleSend={handleSend}
            handleNewMessageChange={handleNewMessageChange}
            loadingMessages={loadingMessages}
        />
    )
}

export default ChatBox;