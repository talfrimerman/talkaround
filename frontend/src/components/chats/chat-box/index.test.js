import ChatBox from "./index"
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});

describe('Test chat box wrapper', () => {
    
    test('Test button click event not triggers function when empty input', () => {
        const mockedOnClickHandler = jest.fn()
        let mountedWrapper = mount(<ChatBox handleSend={mockedOnClickHandler} />); 
        let innerComponent = mountedWrapper.find('ChatBoxComponent')
        let button = innerComponent.find("button").at(0)
        button.simulate('click', {});
        expect(mockedOnClickHandler).toHaveBeenCalledTimes(0); 
    });

    test('Test input change event triggers correct function', () => {
        const mockedHandleInputChange = jest.fn()
        let mountedWrapper = mount(<ChatBox handleNewMessageChange={mockedHandleInputChange} />); 
        let innerComponent = mountedWrapper.find('ChatBoxComponent')
        expect(innerComponent.exists()).toBeTruthy()
        let input = innerComponent.find("input").at(0)
        expect(input.exists()).toBeTruthy()
        input.simulate('change', {});
        expect(mockedHandleInputChange).toHaveBeenCalledTimes(1); 
    });
})
