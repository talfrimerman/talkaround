import { useStyles } from "../messagesStyle";
import Box from '@mui/material/Box';
import unread_double_v from '../../../../assets/unread_double_v.png'
import read_double_v from '../../../../assets/read_double_v.png'




const SelfMessageComponent = ({message, timestamp, read}) => {
    const classes = useStyles();

    return (
        <Box className={classes.messageRowRight}>
            <Box className={classes.messageBlue}>
                <p className={classes.messageContent}>{message}</p>
                <Box className={classes.rightMetadata}>
                    <Box>
                        {read ? 
                            <img className={classes.readRecipts} src={read_double_v} alt="" />
                        :
                            <img className={classes.readRecipts} src={unread_double_v} alt="" />
                        }
                    </Box>
                    <Box className={classes.messageTimeStampRight}>
                        {timestamp}
                    </Box>
                </Box>
                
            </Box>
        </Box>
  );
}

export default SelfMessageComponent;