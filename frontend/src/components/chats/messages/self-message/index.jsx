import SelfMessageComponent from "./self-message-component";

const SelfMessage = ({message, timestamp, read}) => {
    return (
        <SelfMessageComponent
            message={message}
            timestamp={timestamp}
            read={read}
        />
  );
}

export default SelfMessage;