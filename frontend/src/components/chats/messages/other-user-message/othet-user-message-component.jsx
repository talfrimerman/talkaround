import Avatar from "@material-ui/core/Avatar";
import Box from '@mui/material/Box';
import { useStyles } from "../messagesStyle";

const OtherUserMessageComponent = ({message, timestamp, displayName, }) => {

    const classes = useStyles();
    return (
      <>
        <Box className={classes.messageRow}>
          <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent:'flex-start',
          }}>
            <Avatar className={classes.color}>
              {displayName?.charAt(0)}
            </Avatar>
          </Box>
          <Box>
            <Box className={classes.messageGrey}>
              <Box className={[classes.displayName, "pointerCursor"].join(" ")}>{displayName}</Box>
              <Box>
                <Box className={classes.messageContent}>{message}</Box>
              </Box>
              <Box className={classes.messageTimeStampRight}>{timestamp}</Box>
            </Box>
          </Box>
        </Box>
      </>
    );
}

export default OtherUserMessageComponent;