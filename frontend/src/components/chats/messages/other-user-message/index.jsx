import OtherUserMessageComponent from "./othet-user-message-component"

const OtherUserMessage = ({message, timestamp, displayName}) => {

    return (
      <OtherUserMessageComponent 
        message = {message}
        timestamp = {timestamp}
        displayName = {displayName}
      />
    )
}

export default OtherUserMessage;