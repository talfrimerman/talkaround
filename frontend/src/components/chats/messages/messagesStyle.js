import { createStyles, makeStyles } from "@material-ui/core/styles";
import { pink } from '@material-ui/core/colors';

export const useStyles = makeStyles((theme) =>
    createStyles({
    messageRow: {
        display: "flex"
    },
    messageRowRight: {
        display: "flex",
        justifyContent: "flex-end"
    },
    messageGrey: {
        position: "relative",
        marginLeft: "5px",
        marginBottom: "10px",
        padding: "10px",
        backgroundColor: "#F2F3F4",
        minWidth: "7rem",
        maxWidth: "19vw",
        //width: "20vw",
        //height: "50px",
        textAlign: "left",
        font: "400 .9em 'Open Sans', sans-serif",
        border: "1px solid #BDC3C7",
        borderRadius: "10px",
        // "&:after": {
        // content: "''",
        // position: "absolute",
        // width: "0",
        // height: "0",
        // borderTop: "15px solid #F2F3F4",
        // borderLeft: "15px solid transparent",
        // borderRight: "15px solid transparent",
        // top: "0",
        // left: "-15px"
        // },
        // "&:before": {
        // content: "''",
        // position: "absolute",
        // width: "0",
        // height: "0",
        // borderTop: "17px solid #BDC3C7",
        // borderLeft: "16px solid transparent",
        // borderRight: "16px solid transparent",
        // top: "-1px",
        // left: "-17px"
        // }
    },
    messageBlue: {
        position: "relative",
        marginRight: "20px",
        marginBottom: "10px",
        padding: "10px",
        backgroundColor: "#AED6F1",
        minWidth: "7rem",
        maxWidth: "19vw",
        //height: "50px",
        textAlign: "left",
        font: "400 .9em 'Open Sans', sans-serif",
        border: "1px solid #85C1E9",
        borderRadius: "10px",
        "&:after": {
            content: "''",
            position: "absolute",
            width: "0",
            height: "0",
            borderTop: "15px solid #AED6F1",
            borderLeft: "15px solid transparent",
            borderRight: "15px solid transparent",
            top: "0",
            right: "-15px"
        },
        "&:before": {
            content: "''",
            position: "absolute",
            width: "0",
            height: "0",
            borderTop: "17px solid #85C1E9",
            borderLeft: "16px solid transparent",
            borderRight: "16px solid transparent",
            top: "-1px",
            right: "-17px"
        }
    },
    messageContent: {
        padding: 1,
        paddingBottom: 4,
        margin: 0,
        minWidth:1,
        overflowWrap:'break-word',
        fontSize:13,
    },
    rightMetadata : {
        display: 'flex',
        flexDirection: 'row-reverse'
    },
    readRecipts: {
        position: "absolute",
        height: '2.7vh',
        right: '65px',
        bottom: '0.1px'
    },
    messageTimeStampRight: {
        position: "absolute",
        fontSize: ".75em",
        padding: 3,
        fontWeight: "300",
        bottom: "-3px",
        right: "2px"

    },
    displayName: {
        color: '#FF3364',
        fontSize: 13,
        "&:hover": {
            textDecoration: "underline",
        }
    },
    color: {
        backgroundColor: pink[500]
    },
   
    })
);