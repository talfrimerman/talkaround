import CreateChatModal from "./index"
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';
import { act } from 'react-dom/test-utils';


describe('Test create chat modal wrapper', () => {

    test('Test chat name field change event', () => {  
        let mountedWrapper = mount(<Router><CreateChatModal open={true}/></Router>); 
        let innerComponent = mountedWrapper.find('CreateChatModalComponent')
        let chatNameInputBox = innerComponent.find("ForwardRef(ModalUnstyled)").find("input")
        chatNameInputBox.simulate('change', { 'target': { 'value': 'chat1' }});
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('CreateChatModalComponent');
        expect(innerComponent.props().chatName).toStrictEqual("chat1");
    });

    test('Test button not clicking when chatname is empty', () => { 
        let mountedWrapper = mount(<Router><CreateChatModal open={true}/></Router>); 
        let innerComponent = mountedWrapper.find('CreateChatModalComponent')
        const mockedHandleClick = jest.fn();
        // inject mocked handle click
        innerComponent.props().handleClick = mockedHandleClick;
        let sendButton = innerComponent.find("ForwardRef(ModalUnstyled)").find("button");
        sendButton.simulate('click', {});
        expect(mockedHandleClick).toHaveBeenCalledTimes(0)
    });

});