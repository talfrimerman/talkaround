import CreateChatModalComponent from "./create-chat-modal-component";
import { useReducer } from 'react';
import createChatReducer from "./create-chat-reducer";
import { HANDLE_CHATNAME_CHANGE, HANDLE_DISABLE_BUTTON_CHANGE } from "./reducerEventTypes";
import { useNavigate } from 'react-router-dom' 
import { createChat, responseStatuses } from "../../../api";

const initialState = {
    chatName: "",
    disableButton: false
};

const CreateChatModal = ({targetUsername, open, handleClose}) => {
    const [state, dispatch] = useReducer(createChatReducer, initialState);
    const navigate = useNavigate();
    
    const handleChatNameChange = (e) => {
        dispatch({
          type: HANDLE_CHATNAME_CHANGE,
          payload: e.target.value,
        });
    };

    const handleCreateClick = async () => {
        const newChatName = state.chatName;
        if (newChatName !== "") {
            dispatch({
                type: HANDLE_DISABLE_BUTTON_CHANGE,
                payload: true,
            });
            const selfUsername = localStorage.getItem("username")
            const response = await createChat(newChatName, selfUsername, targetUsername)
            console.log(response)
            if (response.status === "OK" && response.payload && response.payload.chatName) {
                const newChatName = response.payload.chatName
                return navigate('/chats/' + newChatName)
            }
            if (response.status === responseStatuses["DUPLICATE_ENTITY"]) {
                alert("Chat already exists. Please choose another name.")
                dispatch({
                    type: HANDLE_DISABLE_BUTTON_CHANGE,
                    payload: false,
                });
                return;
            }
            alert("problem creating chat")
        }
    }

    return (
        <CreateChatModalComponent
            open = {open}
            handleClose = {handleClose}
            disable = {state.disableButton}
            targetUsername = {targetUsername}
            chatName = {state.chatName}
            handleChatNameChange = {handleChatNameChange}
            handleClick = {handleCreateClick}
        />
    )
}

export default CreateChatModal;