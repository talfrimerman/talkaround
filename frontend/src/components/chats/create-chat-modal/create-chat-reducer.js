import { HANDLE_CHATNAME_CHANGE, HANDLE_DISABLE_BUTTON_CHANGE } from "./reducerEventTypes";

const createChatReducer = (state, action) => {
    switch(action.type) {
        case HANDLE_CHATNAME_CHANGE:
            return { ...state, "chatName": action.payload };
        case HANDLE_DISABLE_BUTTON_CHANGE:
            return { ...state, "disableButton": action.payload };
        default: 
            return state;
    }
};

export default createChatReducer;