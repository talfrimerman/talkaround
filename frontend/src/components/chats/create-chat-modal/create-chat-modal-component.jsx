import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import CssBaseline from '@mui/material/CssBaseline';
import Button from '@mui/material/Button';
import { Avatar } from "@material-ui/core";
import { LinearProgress } from '@mui/material';

const CreateChatModalComponent = ({open, handleClose, disable, targetUsername, chatName, handleChatNameChange, handleClick}) => {
    return (
        <Box>
            <CssBaseline />
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    width: 250,
                    bgcolor: 'background.paper',
                    border: '2px solid #000',
                    borderRadius: 2,
                    boxShadow: 24,
                    p: 3,
                }}>
                <Box sx={{
                    display:'flex',
                    flexDirection: 'column',
                    backgroundColor: "rgba(50, 50, 50, 0.15)",
                    alignItems: 'center',
                    justifyContent: 'center',
                    mb: 3.5,
                    mr: -3,
                    ml: -3,
                    mt: -3,
                    p: 1,
                }}>
                    <Avatar> 
                        {targetUsername?.charAt(0)}
                    </Avatar>
                    <Typography
                        className="username"
                        variant="body2"
                        align="center"
                    >
                        {targetUsername}
                    </Typography>
                </Box>
                {/* <Typography id="modal-modal-title" variant="body1" component="h2">
                    Start a chat
                </Typography> */}
                { disable ? <LinearProgress/> : <></> }
                <TextField
                    margin="normal"
                    fullWidth
                    id="chatname"
                    label="Type chat name..."
                    name="chatname"
                    value={chatName}
                    onChange={handleChatNameChange}
                    autoFocus
                    disabled={disable}
                />
                
                <Button
                    fullWidth
                    variant="contained"
                    sx={{ mt: 2, mb: 2 }}
                    onClick={handleClick}
                    disabled={disable}
                >
                    create
                </Button>
                <Typography id="modal-modal-description" variant="body2" sx={{ mt: 1 }}>
                    🛈 This chat will be public 
                </Typography>

                </Box>
            </Modal>
        </Box>
    )
}

export default CreateChatModalComponent;