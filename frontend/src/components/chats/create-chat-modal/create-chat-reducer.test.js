import createChatReducer from "./create-chat-reducer";
import {HANDLE_CHATNAME_CHANGE, HANDLE_DISABLE_BUTTON_CHANGE} from "./reducerEventTypes"

describe("testing chat creating modal reducer", () =>{

    test("Test initial values", () => {
        expect(createChatReducer({'chatName': ""}, {type: "null"}).chatName).toEqual("")
        expect(createChatReducer({'disableButton': false}, {type: "null"}).disableButton).toEqual(false)
    });

    test("Test chatName edit event", () => {
        expect(createChatReducer({'chatName': ""}, {'type': HANDLE_CHATNAME_CHANGE, 'field': "chatName", 'payload': "chat1"}).chatName).toEqual('chat1')
    });

    test("Test disableButton edit event", () => {
        expect(createChatReducer({'disableButton': false}, {'type': HANDLE_DISABLE_BUTTON_CHANGE, 'field': "disableButton", 'payload': true}).disableButton).toEqual(true)
    });

});