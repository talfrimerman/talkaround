import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';

export const ChatPreviewSkeleton = () => {
    return (
      <Stack sx={{ display: "flex", marginTop:4 }} spacing={1} alignItems="left" margin={8}   justify="center">
        <Skeleton variant="rectangular" width={120} height={40} />
        <Skeleton variant="rectangular" width={160} height={25}/>
        <Skeleton variant="rectangular" width={0} height={18}/>
        <Skeleton variant="rectangular" width={240} height={45} sx={{alignSelf: 'center'}}/>
      </Stack>
    );
  }

export default ChatPreviewSkeleton;