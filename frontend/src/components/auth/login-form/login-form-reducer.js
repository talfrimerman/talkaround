import { HANDLE_INPUT_TEXT } from "./reducerEventTypes";

const loginFormReducer = (state, action) => {
    switch(action.type) {
        case HANDLE_INPUT_TEXT:
            return { ...state, [action.field]: action.payload };
        default: 
            return state;
    }
};

export default loginFormReducer;