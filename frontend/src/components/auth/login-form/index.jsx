import loginFormReducer from './login-form-reducer';
import LoginFormComponent from './login-form-component'
import { useReducer } from 'react';
import { CONNECTION_FAILED_MESSAGE } from '../../../constants';
import { loginRequest, responseStatuses } from '../../../api';
import { HANDLE_INPUT_TEXT } from "./reducerEventTypes";
import { useNavigate } from 'react-router-dom' 
import cleanLocalStorage from '../../../utils/localStorageCleaner';
const initialFormState = {
  username: "",
  password: "",
};


const LoginForm = () => {
    const [formState, dispatch] = useReducer(loginFormReducer, initialFormState);
    const navigate = useNavigate();

    /**
       * This function is triggered when user tries to login.
       * if login successful, add details to local storage and redirect to homepage
       * @param {*} event 
    */
    const handleSubmitClick = async (e) => {
      cleanLocalStorage();
      const { username, password } = formState
      console.log(username)
      if (username !== '' && password !== '') {
        try {
          const response = await loginRequest(username, password)
          if (response.status === "OK") {
            localStorage.setItem('jwt', response.payload.jwt)
            if (response.payload.admin) {
              localStorage.setItem('admin', response.payload.admin)
              navigate("/admin")
            }
            else {
              localStorage.setItem('username', response.payload.username);
              localStorage.setItem('language', response.payload.language);
              navigate("/discover")
            }
          }
          if (response.status === responseStatuses["WRONG_CREDENTIALS"]) {
            alert("Wrong details. Please try again.")
          }
        }
        catch (e) {
          alert(CONNECTION_FAILED_MESSAGE)
        }
      }
    };

    const handleTextChange = (e) => {
      dispatch({
        type: HANDLE_INPUT_TEXT,
        field: e.target.name,
        payload: e.target.value,
      });
    };

    return (<LoginFormComponent
      handleSubmit={ handleSubmitClick }
      handleTextChange={ handleTextChange }
      navigate={ navigate }
      username = { formState.username }
      password = { formState.password }
    />);
};

export default LoginForm;
