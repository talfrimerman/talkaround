import Adapter from 'enzyme-adapter-react-16';
import { mount, render, shallow, configure } from 'enzyme';
configure({ adapter: new Adapter() });

import LoginFormComponent from "./login-form-component"


describe("testing login form component", () =>{
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<LoginFormComponent/>).dive();
  });

  test("render a username input", () => {
    expect(wrapper.find("#username").exists()).toBeTruthy();
  });

  test("render a password input", () => {
    expect(wrapper.find("#password").exists()).toBeTruthy();
  });


});