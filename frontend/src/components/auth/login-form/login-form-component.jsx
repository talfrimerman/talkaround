import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

const LoginFormComponent = ({handleTextChange, handleSubmit, username, password, navigate}) =>  {

    return(
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 0,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >   
          {/* <Fade in={true} timeout={1500}>
            <Avatar src={OFromLogo} sx={{ width: 65, height: 65 }}/>
          </Fade> */}
          <Typography component="h1" variant="h4">
            Sign in
          </Typography>
          <Box sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              value={username}
              autoFocus
              onChange={handleTextChange}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={handleTextChange}
            />
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link onClick={() => navigate("/signup")} variant="body2">
                  {"Don't have an account yet? Sign up now!"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    );
    
  
}
   
export default LoginFormComponent;