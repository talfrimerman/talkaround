import loginFormReducer from "./login-form-reducer"
import { HANDLE_INPUT_TEXT } from "./reducerEventTypes";



describe("testing register form reducer", () =>{

    test("Test initial values", () => {
        expect(loginFormReducer({'username': ""}, {type: "null"}).username).toEqual("")
    });

    test("Test username edit event from empty username", () => {
        expect(loginFormReducer({'username': ""}, {'type': HANDLE_INPUT_TEXT, 'field': "username", 'payload': "user1"}).username).toEqual('user1')
    });
    test("Test password edit event from existing password", () => {
        expect(loginFormReducer({'username': "", 'password': "123"}, {'type': HANDLE_INPUT_TEXT, 'field': "password", 'payload': "1234"}).password).toEqual('1234')
    });
});