import LoginForm from "./index"
import { mount, configure  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';



describe('Test login form wrapper', () => {
    test('Test username field change event', () => {  
        let mountedWrapper = mount(<Router><LoginForm /></Router>); 
        let innerComponent = mountedWrapper.find('LoginFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        let userNameInputBox = innerComponent.find("input").at(0)
        expect(userNameInputBox.exists()).toBeTruthy()

        userNameInputBox.simulate('change', { 'target': { 'name': 'username', 'value': 'user1' }});
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('LoginFormComponent')
        expect(innerComponent.props().username).toStrictEqual("user1");
    });

    test('Test password field change event', () => {  
        let mountedWrapper = mount(<Router> <LoginForm /> </Router>);      
        let innerComponent = mountedWrapper.find('LoginFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        let passwordInputBox = innerComponent.find("input").at(1)
        expect(passwordInputBox.exists()).toBeTruthy()

        passwordInputBox.simulate('change', { 'target': { 'name': 'password', 'value': 'password1' }});
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('LoginFormComponent')
        expect(innerComponent.props().password).toStrictEqual("password1");
    });


});
