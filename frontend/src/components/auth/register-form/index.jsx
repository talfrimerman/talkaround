import RegisterFormComponent from "./register-form-component"
import registerFormReducer from "./register-form-reducer"
import {languageCodeDictionary, languages} from "../../../utils/languageUtils"
import { useReducer } from 'react';
import { CONNECTION_FAILED_MESSAGE } from '../../../constants';
import { registerRequest, responseStatuses } from '../../../api';
import { HANDLE_INPUT_TEXT, HANDLE_SELCTED_PASSIONS_CHANGE } from "./reducerEventTypes";
import { useNavigate } from 'react-router-dom' 



const initialFormState = {
    username: "",
    password: "",
    language: "English",
    country: "",
    status: "",
    passions:[],

};

const RegisterForm = () => {
    const [formState, dispatch] = useReducer(registerFormReducer, initialFormState);
    const navigate = useNavigate();


    /**
       * This function is triggered when user tries to register.
       * if register successful, redirect to homepage
       * @param {*} event 
    */
    const handleSubmitClick = async (e) => {
        const {username, password, country, language, status, passions} = formState
        if (validDeatils(username, password, country, language, status, passions)) {
            const languageCode = languageCodeDictionary[language] // find langage code for user's language
            try {
                const response = await registerRequest(username, password, country, languageCode, status, passions)
                if (response.status === "OK") {
                    navigate("/welcome/signedup")
                }
                if (response.status === responseStatuses["DUPLICATE_ENTITY"]) {
                    alert("Username already exists. Please try again.")
                }
            }
            catch (e) {
                alert(CONNECTION_FAILED_MESSAGE)
            }
        }
    };

    const handleTextChange = (e, values) => {
        console.log("text changed event!!!")
        let field = values ? "language" : e.target.name // values means its language input, we consider language as a text input
        let payload = values || e.target.value
        dispatch({
            type: HANDLE_INPUT_TEXT,
            field: field,
            payload: payload,
        });
    };

    const handlePassionsChanged = (e, values) => {
        console.log("passions changed event!!!")
        console.log(values)
        dispatch({
            type: HANDLE_SELCTED_PASSIONS_CHANGE,
            passions: values.slice(0,3)
        });
    }

    return (<RegisterFormComponent
        handleSubmit={ handleSubmitClick }
        handleTextChange={ handleTextChange }
        handlePassionsChanged={ handlePassionsChanged }
        navigate={navigate}
        username = { formState.username }
        password = { formState.password }
        country = { formState.country }
        language = { formState.language }
        status = { formState.status }
        passions  = { formState.passions }

    />);

};

const validDeatils = (username, password, country, language, status, passions) => {
    if (!(username.length > 3 && username.length < 20)) {
        alert("Username must be 4 to 19 charachters.")
        return false;
    }
    if (!(password.length > 7 && password.length < 20)) {
        alert("Password must be 8 to 19 chrachters.")
        return false;
    }
    if (!(country.length > 3 && country.length < 40)) {
        alert("Country must be 3 to 39 charachters.")
        return false;
    }
    if (!(status.length > 2 && status.length< 400)) {
        alert("Status must be 3 to 399 charachters.")
        return false;
    }
    if (!(passions.length === 3)) {
        alert("Please choose 3 passions.")
        return false;
    }
    if (!(languages.includes(language))) {
        alert("Please choose a language form the list.")
        return false;
    }
    return true;
}


export default RegisterForm;


