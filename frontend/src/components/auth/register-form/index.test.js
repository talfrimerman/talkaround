import RegisterForm from "./index"
import { mount, configure, shallow  } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({adapter: new Adapter()});
import { BrowserRouter as Router } from 'react-router-dom';
import { act } from 'react-dom/test-utils';



describe('Test register form wrapper', () => {
    test('Test username field change event', () => {  
        let mountedWrapper = mount(<Router> <RegisterForm/> </Router>); 
        let innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        let userNameInputBox = innerComponent.find("input").at(0)
        expect(userNameInputBox.exists()).toBeTruthy()

        userNameInputBox.simulate('change', { 'target': { 'name': 'username', 'value': 'user1' }});
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.props().username).toStrictEqual("user1");
    });

    test('Test password field change event', () => {  
        let mountedWrapper = mount(<Router> <RegisterForm/> </Router>); 
        let innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        let passwordInputBox = innerComponent.find("input").at(1)
        expect(passwordInputBox.exists()).toBeTruthy()

        passwordInputBox.simulate('change', { 'target': { 'name': 'password', 'value': 'pass1' }});
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.props().password).toStrictEqual("pass1");
    });

    test('Test country field change event', () => {  
        let mountedWrapper = mount(<Router> <RegisterForm/> </Router>); 
        let innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        let countryInputBox = innerComponent.find("input").at(2)
        expect(countryInputBox.exists()).toBeTruthy()

        countryInputBox.simulate('change', { 'target': { 'name': 'country', 'value': 'Germany' }});
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.props().country).toStrictEqual('Germany');
    });

    test('Test passions field change event', async() => {  
        let mountedWrapper = mount(<Router> <RegisterForm/> </Router>); 
        let innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        await act(async () => {
            console.log(innerComponent.props())
            innerComponent.props().handlePassionsChanged({"target": {"value": ""}}, ['Netflix']); 
        })
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.props().passions).toEqual(['Netflix']);
    });

    test('Test passions field: trying to add more then 3 passions', () => {  
        let mountedWrapper = mount(<Router> <RegisterForm/> </Router>); 
        let innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.exists()).toBeTruthy()
        innerComponent.props().handlePassionsChanged({"target": {"value": ""}}, ["Netflix", "Coffee", "Cooking", "Golf"]); 
        mountedWrapper.update()
        innerComponent = mountedWrapper.find('RegisterFormComponent')
        expect(innerComponent.props().passions).toEqual(["Netflix", "Coffee", "Cooking"]);
    });


});
