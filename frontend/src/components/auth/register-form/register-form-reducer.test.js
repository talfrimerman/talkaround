import registerFormReducer from "./register-form-reducer"
import { useReducer } from 'react';
import { renderHook, act } from '@testing-library/react-hooks'
import { HANDLE_INPUT_TEXT, HANDLE_SELCTED_PASSIONS_CHANGE } from "./reducerEventTypes";



describe("testing register form reducer", () =>{

    test("Test initial values", () => {
        expect(registerFormReducer({'username': ""}, {type: "null"}).username).toEqual("")
    });

    test("Test username edit event", () => {
        expect(registerFormReducer({'username': ""}, {'type': HANDLE_INPUT_TEXT, 'field': "username", 'payload': "user1"}).username).toEqual('user1')
    });
    test("Test password edit event", () => {
        expect(registerFormReducer({'username': "", 'password': "123", "language": "en", "country": "Israel"}, 
        {'type': HANDLE_INPUT_TEXT, 'field': "password", 'payload': "1234"}).password).toEqual('1234')
    });

    test("Test first passion add", () => {
        expect(registerFormReducer({'username': "", 'password': "123", "language": "en", "country": "Israel", passions: []}, 
        {'type': HANDLE_SELCTED_PASSIONS_CHANGE, 'passions': ["Netflix"]}).passions[0]).toEqual('Netflix')
    });

    test("Test full passions list change passion", () => {
        expect(registerFormReducer({'username': "", 'password': "123", "language": "en", "country": "Israel", passions: ["Netflix","Golf","Running"]}, 
        {'type': HANDLE_SELCTED_PASSIONS_CHANGE, 'passions': ["Netflix", "Golf", "Climbing"]}).passions[2]).toEqual('Climbing')
    });
});