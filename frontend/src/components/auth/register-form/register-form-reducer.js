import { HANDLE_INPUT_TEXT, HANDLE_SELCTED_PASSIONS_CHANGE } from "./reducerEventTypes";

const registerFormReducer = (state, action) => {
    switch(action.type) {
        case HANDLE_INPUT_TEXT:
            return { ...state, [action.field]: action.payload };
        case HANDLE_SELCTED_PASSIONS_CHANGE:
            return { ...state, passions: action.passions }
        default: 
            return state;
    }
};

export default registerFormReducer;