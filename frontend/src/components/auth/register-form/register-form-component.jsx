import {languages} from "../../../utils/languageUtils"
import {passions as passionsList} from "../../../utils/passionsUtils"
import Autocomplete from '@mui/material/Autocomplete';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';


const RegisterFormComponent = ({handleTextChange, handlePassionsChanged, handleSubmit, 
  username, password, country, language, status, passions, navigate}) =>  {
    
    
    return(
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 0,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >   

            <Typography component="h1" variant="h6">
              Just a quick signup and you'll be ready!
            </Typography>
            <Box sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                value={username}
                autoFocus
                onChange={handleTextChange}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                value={password}
                autoComplete="current-password"
                onChange={handleTextChange}
                helperText="Password should contain at least 8 charachters."
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="country"
                label="country"
                name="country"
                value={country}
                onChange={handleTextChange}
              />
              <Autocomplete
                disablePortal
                required
                fullWidth
                id="language"
                name="language"
                value={language || "English"}
                options={languages}
                onChange={handleTextChange}
                
                renderInput={(params) => <TextField {...params} helperText="Please choose your language." label="language" />}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="status"
                label="status"
                name="status"
                value={status}
                helperText="Choose a status that people will see."
                onChange={handleTextChange}
              />
              <Autocomplete
                multiple
                id="passions-AC"
                options={passionsList}
                getOptionLabel={(option) => option}
                onChange={handlePassionsChanged}
                value={passions}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    label="Passions"
                    placeholder="Choose 3 passions"
                    helperText="Tell the world what you're intersted in."
                    // onChange={handlePassionsChanged}

                  />
                )}
              />
              <Button
                onClick={handleSubmit}
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign up
              </Button>
              <Grid container>
                <Grid item>
                  <Link onClick={() => navigate("/welcome")} variant="body2">
                    {"I already have an account"}
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      );
}
   
export default RegisterFormComponent;