import Adapter from 'enzyme-adapter-react-16';
import { mount, render, shallow, configure } from 'enzyme';
configure({ adapter: new Adapter() });

import RegisterFormComponent from "./register-form-component"



describe("testing register form component", () =>{
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<RegisterFormComponent/>).dive();
  });

  test("render a username input", () => {
    expect(wrapper.find("#username").exists()).toBeTruthy();
  });

  test("render a password input", () => {
    expect(wrapper.find("#password").exists()).toBeTruthy();
  });

  test("render a password input", () => {
    expect(wrapper.find("#country").exists()).toBeTruthy();
  });

  test("render a password input", () => {
    expect(wrapper.find("#language").exists()).toBeTruthy();
  });

});