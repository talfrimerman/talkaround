import {useState} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Avatar } from '@mui/material';
import o_logo from "../../assets/o_from_logo.png"
import { useNavigate } from 'react-router-dom' 
import cleanLocalStorage from '../../utils/localStorageCleaner';
import MenuIcon from '@mui/icons-material/Menu';
import AdminAppbarMenu from '../admin-appbar-menu';

const AdminAppBar = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const menuOpen = Boolean(anchorEl);
    const handleMenuClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    const handleMenuClose = () => {
      setAnchorEl(null);
    };

    const navigate = useNavigate();
    const handleChatsAnalyticsClick = () => {
      navigate("/admin/chats")
    }
    const handleHomeClick = () => {
      navigate("/admin")
    }
    const logout = () => {
      cleanLocalStorage()
      navigate("/")
    }
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar elevation={1} position="fixed" style={{ background: '#2E3B55', color:'white' }}>
          <Toolbar sx={{justifyContent: "space-between"}}>
            <Box sx={{display: "flex", alignItems: "center"}}>
              <Avatar src={o_logo}  sx={{ mr: 2 }}  />
              <Typography variant="h6" component="div" className={"disabledTextSelect"}>
                ADMIN'S DASHBOARD
              </Typography>
            </Box>  
            <Box>  
              <Button 
                color="inherit" 
                onClick={handleMenuClick}
              >
                <MenuIcon />
              </Button>
              <Button color="inherit" onClick={logout}>Logout</Button>
              <AdminAppbarMenu 
                anchorEl={anchorEl}
                menuOpen = {menuOpen}
                handleMenuClose = {handleMenuClose}
                handleChatsAnalyticsClick = {handleChatsAnalyticsClick}
                handleHomeClick = {handleHomeClick}
              />
            </Box>
          </Toolbar>
        </AppBar>
        <Toolbar /> {/* important to keep content under appbar! */}
      </Box>
    );
}

export default AdminAppBar;