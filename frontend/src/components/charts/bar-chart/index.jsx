import BarChartComponent from "./bar-chart-component";

const BarChart = ({data}) => {
    return (
        <BarChartComponent data={data} 
        valueField={data.length > 0 ? Object.keys(data[0])[1] : ""}
        argumentField={data.length > 0 ? Object.keys(data[0])[0] : ""}/>
    )
}

export default BarChart