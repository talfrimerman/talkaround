import Grid from '@mui/material/Grid';
import {
    ArgumentAxis,
    ValueAxis,
    Chart,
    BarSeries,
  } from '@devexpress/dx-react-chart-material-ui';
import { Animation } from '@devexpress/dx-react-chart';

const BarChartComponent = ({data, valueField, argumentField}) => {
    return (
        <Grid>
            <Chart height={320} data={data}>
                <ArgumentAxis />
                <ValueAxis />
                <BarSeries valueField={valueField} argumentField={argumentField}/>
                <Animation/>
            </Chart>
        </Grid>
    )
}

export default BarChartComponent