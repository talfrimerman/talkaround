import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Avatar } from '@mui/material';
import o_logo from "../../assets/o_from_logo.png"
import { useNavigate } from 'react-router-dom' 
import TravelExploreIcon from '@mui/icons-material/TravelExplore';
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import cleanLocalStorage from '../../utils/localStorageCleaner';

const AppBarComponent = () => {
    const navigate = useNavigate();
    const logout = () => {
      cleanLocalStorage()
      navigate("/")
    }
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar elevation={1} position="fixed" style={{ background: '#e6f3ff', color:'black' }}>
          <Toolbar sx={{justifyContent: "space-between"}}>
            <Box sx={{display: "flex", alignItems: "center"}}>
              <Avatar src={o_logo}  sx={{ mr: 2 }}  />
              <Typography variant="h6" component="div"  >
                {localStorage.getItem('username')}
              </Typography>
            </Box>  
            <Box>  
              <Button color="inherit" onClick={() => navigate("/discover")}><TravelExploreIcon/></Button>
              <Button color="inherit" onClick={() => navigate("/chats")}><ChatOutlinedIcon/></Button>
              <Button color="inherit" onClick={logout}>Logout</Button>
            </Box>
          </Toolbar>
        </AppBar>
        <Toolbar /> {/* important to keep content under appbar! */}
      </Box>
    );
}

export default AppBarComponent;