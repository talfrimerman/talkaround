import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

const AdminAppbarMenuComponent = ({anchorEl, menuOpen, handleMenuClose, handleChatsAnalyticsClick, handleHomeClick}) => {

    return(
        <Menu
            anchorEl={anchorEl}
            open={menuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={handleHomeClick}>Home</MenuItem>
            <MenuItem onClick={handleChatsAnalyticsClick}>Chats analytics</MenuItem>

        </Menu>
    )
}

export default AdminAppbarMenuComponent;