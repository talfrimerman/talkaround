import AdminAppbarMenuComponent from "./admin-appbar-menu-component";


const AdminAppbarMenu = ({anchorEl, menuOpen, handleMenuClose, handleChatsAnalyticsClick, handleHomeClick}) => {
    return(
        <AdminAppbarMenuComponent
            anchorEl = {anchorEl}
            menuOpen =  {menuOpen}
            handleMenuClose = {handleMenuClose}
            handleChatsAnalyticsClick = {handleChatsAnalyticsClick}
            handleHomeClick = {handleHomeClick}
        />
    )
}

export default AdminAppbarMenu;