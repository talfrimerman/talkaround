import NumericInfoCardComponent from "./numeric-info-card-component";

const NumericInfoCard = ({title, number, icon}) => {
    return (
        <NumericInfoCardComponent title={title} number={number} icon={icon}/>
    )
}

export default NumericInfoCard