import React from "react";
import {
    makeStyles,
} from "@material-ui/core";
import CountUp from 'react-countup';
import {
  Card,
  CardContent,
  Typography,
} from "@material-ui/core";
import Box from '@mui/material/Box';

const useStyles = makeStyles((theme) => ({
    card: {
        paddingTop:10,
        borderRadius: 15,
        minHeight: "25vh",
        maxWidth: "20vw",
        minWidth: "17vw",
        margin: theme.spacing(2),
        display: "inline-block",  
        "&:hover": {
        backgroundColor: "rgba(50, 50, 50, 0.05)"
        }
    },
}));

const NumericInfoCardComponent = ({title, number, icon}) => {
    const classes = useStyles();

    return (
        <Card
        variant="outlined"
        className={[classes.card, "disabledTextSelect"].join(" ")}
        >
            <CardContent className="card-content">
                <Box>
                    <Box sx={{
                        paddingBottom:1
                    }}>
                        <Typography variant="h5" align="center">
                            {title}
                        </Typography>
                    </Box>
                    <Box sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Typography
                            color="textSecondary"
                            variant="h3"
                            align="center"
                        >
                            <CountUp end={number} duration={Math.log(number)/2.5} /> 
                        </Typography>   
                        <Typography
                            variant="h4"
                            align="center"
                        >
                            {icon}
                        </Typography>
                    </Box>
                </Box>
                
            </CardContent>
        </Card>
    )
}

export default NumericInfoCardComponent