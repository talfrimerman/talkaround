import UserCardListComponent from "./user-card-list-component";

import { mount, render, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe("testing card list component", () =>{
    let wrapper;
  
    test("render a UserCardList child and check length 3", () => {
      wrapper = shallow(<UserCardListComponent loaded={true} users={[1, 2, 3]}/>);
      expect(wrapper.find("UserCard")).toHaveLength(3);
    });

    test("render a UserCardList child and check length 0", () => {
      wrapper = shallow(<UserCardListComponent loaded={true} users={[]}/>);
      expect(wrapper.find("UserCard")).toHaveLength(0);
    });

    test("render a unloaded UserCardList child", () => {
      wrapper = shallow(<UserCardListComponent loaded={false} users={[]}/>);
      expect(wrapper.find("UserCard")).toHaveLength(0);
    });

});