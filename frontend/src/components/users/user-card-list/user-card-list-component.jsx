import UserCard from "../user-card" 
import Box from '@mui/material/Box';
import UserCardSkeleton from "../user-card-skeleton";

const UserCardListComponent = ({loaded, users, onCardClick}) => {

    return(
        <> 
        {loaded ? 
            <Box>
                {users && users.map((user, index)=>  
                    <UserCard key={index} user={user} onClick={onCardClick}></UserCard>
                )}
            </Box>
        :
            <Box>
                {[0,1,2,3,4].map((user, index)=>  
                    <UserCardSkeleton key={index}></UserCardSkeleton>
                )}
            </Box>
        }
        </>
    )
}

export default UserCardListComponent