import UserCardListComponent from "./user-card-list-component";

const UserCardList = ({loaded, users, onCardClick}) => {

    return(
        <UserCardListComponent loaded={loaded} users={users} onCardClick={onCardClick} />
    )
}

export default UserCardList;