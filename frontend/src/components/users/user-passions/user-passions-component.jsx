import Box from '@mui/material/Box';
import './user-passions-component.css'

const UserPassionsComponent = ({passions}) => {

    return (
        <Box sx={{ 
            paddingTop: '5%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
            }}>

            {passions && passions.map((passion, index) => 
                <Box className="passion" sx={{
                    borderRadius: 20,
                    fontFamily: ['-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Open Sans', 'Helvetica Neue', 'sans-serif'],
                    fontWeight: 500,
                    alignItems: 'center',
                    justifyContent: 'center',
                    letterSpacing: 1/2,
                    color: "white",
                    width: 78,
                    height: 35,
                    p: 0.5,
                    m: 1,   
                }} key={index}>
                    {passion}
                </Box>
            )}
        </Box>

    )
}

export default UserPassionsComponent;