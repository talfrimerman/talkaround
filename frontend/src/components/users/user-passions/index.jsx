import UserPassionsComponent from "./user-passions-component";

const UserPassions = ({passions}) => {
    return (
            <UserPassionsComponent passions={passions} />
    )
}

export default UserPassions;