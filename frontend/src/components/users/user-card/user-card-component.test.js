import { mount, render, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });


import UserCardComponent from './user-card-component'

describe("testing user card component", () =>{
    let wrapper;
    beforeEach(() => {
      const user= {username: "testUser", status:"Hello", country:"Germany", passions:["Netflix", "Golf", "Cooking"]}
      wrapper = shallow(<UserCardComponent user={user} onClick={()=> {}}/>);
    });
  
    test("render a child of class username", () => {
      expect(wrapper.find(".username").exists()).toBeTruthy();
    });

    test("render a child of class status", () => {
      expect(wrapper.find(".status").exists()).toBeTruthy();
    });

    test("render a child of class country", () => {
      expect(wrapper.find(".Country").exists()).toBeTruthy();
    });

    test("render a child of class passions-wrapper", () => {
      expect(wrapper.find("UserPassions").exists()).toBeTruthy();
    });
  
   
  
  });