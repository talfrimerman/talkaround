import UserCardComponent from "./user-card-component";

const UserCard = ({user, onClick}) => {
    
    return(
        <UserCardComponent user={user} onClick={onClick} />
    )
}

export default UserCard