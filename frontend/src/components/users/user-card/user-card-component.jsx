import React from "react";
import { pink } from '@material-ui/core/colors';
import {
    makeStyles,
} from "@material-ui/core";

import {
  Card,
  CardContent,
  CardMedia,
  Avatar,
  Typography,
} from "@material-ui/core";
import UserPassions from "../user-passions";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import Box from '@mui/material/Box';



const useStyles = makeStyles((theme) => ({
  text: {
    margin: theme.spacing(0, 0, 0.5),
    //color: theme.palette.secondary.contrastText,
  },
  avatar: {
    verticalAlign: "middle",
    marginRight: theme.spacing(0.5),
  },  
  large: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    margin: theme.spacing(1, 1, 0),
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
  },
  card: {
    borderRadius: 15,
    maxWidth: "330px",
    minWidth: "330px",
    backgroundColor: theme.palette.background.card,
    margin: theme.spacing(2),
    "&:hover": {
      backgroundColor: "rgba(50, 50, 50, 0.05)"
    }
  },
  cardContent: {
    padding: theme.spacing(0, 0, 0, 0),
  },
}));

const UserCardComponent = ({user, onClick}) => {
  const classes = useStyles();
  return (
      <Card
        variant="outlined"
        className={[classes.card, onClick && "pointerCursor" ].join(" ")}
        style={{ display: "inline-block" }}
        onClick={onClick ? ()=>onClick(user.username) : ()=>{}}
      >
        <CardMedia align="center">
          <Avatar
            className={classes.large} 
          > 
            {user.username.charAt(0)}

          </Avatar>
          <Typography
            className="username"
            variant="h6"
            align="center"
          >
            {user.username}
          </Typography>
        </CardMedia>
        <CardContent className="card-content">
          <Box sx={{
            margin: 'auto',
            width: '85%',
            borderBottom: 1,
            borderTop: 1,
            borderColor: 'grey.500'
          }}>
            <Typography
              className="status"
              variant="subtitle2"
              align="left"
            >
            {user.status} 
            </Typography>{" "}
          </Box>
          <Typography
            className="Country"
            color="textSecondary"
            variant="subtitle1"
            align="center"
          >
            <LocationOnIcon className={classes.avatar} fontSize="small" />
            {user.country}
          </Typography>{" "}
          <UserPassions passions={user.passions} />
        </CardContent>
      </Card>
  );
}

export default UserCardComponent;