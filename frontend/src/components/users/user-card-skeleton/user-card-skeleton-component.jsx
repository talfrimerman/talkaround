import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';

export const UserCardSkeletonComponent = () => {
    return (
      <Stack spacing={2} alignItems="center" paddingBottom={8} paddingTop={4} justify="center">
        <Skeleton variant="circular" width={40} height={40} />
        <Skeleton variant="rectangular" width={80} height={25} />
        <Skeleton variant="rectangular" width={210} height={48} />
        <Skeleton variant="rectangular" width={180} height={35} />
      </Stack>
    );
  }

export default UserCardSkeletonComponent;