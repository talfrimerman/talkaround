import ChatSummary from '../chat-summary'
import Box from '@mui/material/Box';

const ChatSummeriesListComponent = ({chats}) => {
    return (
        <Box>
            {chats && chats.map((chat, index) => 
                <ChatSummary chatDetails={chat} key={index}/>
            )}
        </Box>
        
    )
}

export default ChatSummeriesListComponent;