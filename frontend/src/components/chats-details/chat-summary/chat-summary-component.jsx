import {
    makeStyles,
} from "@material-ui/core";
import {
  Card,
  CardContent,
  Typography,
} from "@material-ui/core";
import Box from '@mui/material/Box';

const useStyles = makeStyles((theme) => ({
    card: {
        borderRadius: 15,
        minHeight: "13vh",
        minWidth: "80%",
        margin: theme.spacing(1),
        display: "inline-block",  
        "&:hover": {
            backgroundColor: "rgba(50, 50, 50, 0.05)"
        }
    },
}));

const ChatSummaryCopmonent = ({chatDetails}) => {
    const classes = useStyles();

    return (
        <Card
            variant="outlined"
            className={classes.card}
        >
            <CardContent>
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',

                }}>
                    <Box>
                        <Typography variant="h6">
                            {chatDetails.chatName}
                        </Typography>
                    </Box>
                    <Box>
                        <Box>
                            <Typography
                                variant="body1"
                            >
                                {chatDetails.usersCount}
                            </Typography>
                        </Box>
                        <Box>
                            <Typography
                                color="textSecondary"
                                variant="body1"
                            >
                                users
                            </Typography>
                        </Box>
                    </Box>
                    <Box>
                        <Box>
                            <Typography
                                variant="body1"
                            >
                                {chatDetails.messagesCount}
                            </Typography>
                        </Box>
                        <Box>
                            <Typography
                                color="textSecondary"
                                variant="body1"
                            >
                                messages
                            </Typography>
                        </Box>  
                    </Box>
                    <Box>
                        <Box>
                            <Typography
                                variant="body1"
                            >
                                {chatDetails.lastMessageTimestamp || "-"}
                            </Typography>
                        </Box>   
                        <Box>
                            <Typography
                                color="textSecondary"
                                variant="body1"
                            >
                                last message
                            </Typography>
                        </Box>
                    </Box>
                </Box>
            </CardContent>
        </Card>
    )
}

export default ChatSummaryCopmonent
